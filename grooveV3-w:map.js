/*
TwatacGrooveV3 1.1
by marco fabre

BB
*/

//config
/*instance = ["kk","kk","snr","snr","snr","hh open",
"hh close","ride","ride","tomL","tomR",
"timp","crash","crash","crash"];*/

var NOTES = [
'C-2', 'C#-2', 'D-2', 'D#-2', 'E-2', 'F-2', 'F#-2', 'G-2', 'G#-2', 'A-2', 'A#-2', 'B-2',
'C-1', 'C#-1', 'D-1', 'D#-1', 'E-1', 'F-1', 'F#-1', 'G-1', 'G#-1', 'A-1', 'A#-1', 'B-1',
'C0', 'C#0', 'D0', 'D#0', 'E0', 'F0', 'F#0', 'G0', 'G#0', 'A0', 'A#0', 'B0',
'C1', 'C#1', 'D1', 'D#1', 'E1', 'F1', 'F#1', 'G1', 'G#1', 'A1', 'A#1', 'B1',
'C2', 'C#2', 'D2', 'D#2', 'E2', 'F2', 'F#2', 'G2', 'G#2', 'A2', 'A#2', 'B2',
'C3', 'C#3', 'D3', 'D#3', 'E3', 'F3', 'F#3', 'G3', 'G#3', 'A3', 'A#3', 'B3',
'C4', 'C#4', 'D4', 'D#4', 'E4', 'F4', 'F#4', 'G4', 'G#4', 'A4', 'A#4', 'B4',
'C5', 'C#5', 'D5', 'D#5', 'E5', 'F5', 'F#5', 'G5', 'G#5', 'A5', 'A#5', 'B5',
'C6', 'C#6', 'D6', 'D#6', 'E6', 'F6', 'F#6', 'G6', 'G#6', 'A6', 'A#6', 'B6',
'C7', 'C#7', 'D7', 'D#7', 'E7', 'F7', 'F#7', 'G7', 'G#7', 'A7', 'A#7', 'B7',
'C8', 'C#8', 'D8', 'D#8', 'E8', 'F8', 'F#8', 'G8', '---'];

instance = {
	'kick':['b0','c1'],
	'snare':['c#1','d1','a0'],
	'tom':['c2','b1','a1','g1'],
	'hh':['d3','c3'],
	'ride':['f2','d#2','b2'],
	'crash L':['g3','g2','c#2'],
	'crash C':['g#2','f#2','e2'],
	'crash R':['a2','c0']
}


//contatore
var roundsA = [];
for (k in instance) {
	roundsA[k] = 0;
}

var roundsT = [];
for (k in instance) {
	roundsT[k] = 2;
	Trace("roundsT: "+k+" "+roundsT[k]);
}

DL = 0;

/* -- gui -- */
/*
var PluginParameters = [
	{
		name: "Config:",
		type: "text",
	}
];*/

/*
for (k in instance) {
	PluginParameters.push({
		name: instance[k].toUpperCase() + " " + k,
		type: "menu",
		valueStrings: NOTES,
		numberOfSteps: 127,
		defaultValue: 128
	});
}*/

var PluginParameters = [
	{
		name: "Velocity Mixer:",
		type: "text"
	}
];

for (k in instance) {
	var label = "[" + k.toUpperCase() + "]";
	PluginParameters.push(
		/*{
			name: "Solo " + label,
			type: "menu",
			valueStrings: false,
			defaultValue: 4
		},*/
		{
			name: "Velocity Offset " + label,
			type: "lin",
			defaultValue: 0,
			numberOfSteps: 127,
			minValue: -64,
			maxValue: 63
		}
	);
}


for (k in instance) {
	var label = "[" + k.toUpperCase() + "]";
	PluginParameters.push({
		name: "Paramaters " + label,
		type: "text"
	},/* {
		name: "Low Range " + label,
		type: "menu",
		valueStrings: NOTES,
		numberOfSteps: 127,
		defaultValue: 128
	}, {
		name: "Hi Range " + label,
		type: "menu",
		valueStrings: NOTES,
		numberOfSteps: 127,
		defaultValue: 128
	},*/ {
		name: "Solo " + label,
		type: "menu",
		valueStrings: false,
		defaultValue: 4
	}, {
		name: "Accent Every " + label,
		type: "lin",
		defaultValue: 4,
		numberOfSteps: 32,
		minValue: 0,
		maxValue: 32
	}, {
		name: "Accent Vel " + label,
		type: "lin",
		numberOfSteps: 127,
		minValue: 0,
		maxValue: 127,
		defaultValue: 11
	}, {
		name: "Random Vel " + label,
		type: "lin",
		numberOfSteps: 127,
		minValue: 0,
		maxValue: 127,
		defaultValue: 0
	}, {
		name: "Delay Every " + label,
		type: "lin",
		numberOfSteps: 32,
		minValue: 0,
		maxValue: 32,
		defaultValue: 3
	}, {
		name: "Delay Ammount " + label,
		type: "lin",
		numberOfSteps: 1000,
		minValue: 0,
		maxValue: 1000,
		defaultValue: 0
	}, {
		name: "Random Delay " + label,
		type: "lin",
		numberOfSteps: 1000,
		minValue: 0,
		maxValue: 1000,
		defaultValue: 0
	}, {
		name: "Humanize Vel " + label,
		type: "lin",
		numberOfSteps: 127,
		minValue: 0,
		maxValue: 127,
		defaultValue: 0
	}, {
		name: "Humanize Time " + label,
		type: "lin",
		numberOfSteps: 1000,
		minValue: 0,
		maxValue: 1000,
		defaultValue: 0
	});
}

/* -- gui -- */

/* tools */

function rand(min, max) {
	var out = Math.round(Math.random() * (max - min) + min);
	return out;
}

function allSolo(ar) {

	var s = 0;

	for (k in ar) {
		if (ar[k] != 0) {
			s++;
		}
	}

	if (s != 0) {
		return true;
	}

	return false;
}

function testRange(loR, hiR, p) {
	if (loR != 128 && hiR != 128) {
		var loBool = loR < p;
		var hiBool = p < hiR;

		var range = loBool == hiBool;
		return range;
	}
}

function testNote(event) {
	var t = 0;
	for (id in config) {
		if (config[id].indexOf(event.pitch) != -1) {
			//Trace("## "+event);
			//return true;
			t++
		} else {
			//Trace(":: "+event);
			//return false
		}
	}
	
	return t;
}

function getSolo(dtNote,dtSolo,event)
{	
	var t = 0;
	
	for( id in dtSolo )
	{
		if(dtSolo[id] == 1)
		{
			//Trace("id:0"+id+" s: "+dtSolo[id]+" n:"+dtNote[id]+" e:"+event.pitch);
			
			if(dtNote[id].indexOf(event.pitch) != -1)
			{
				//Trace("id:"+id+" s: "+dtSolo[id]+" n:"+dtNote[id]+" e:"+event.pitch+" true");
				t++;

			}
			else
			{
				//Trace("id:"+id+" s: "+dtSolo[id]+" n:"+dtNote[id]+" e:"+event.pitch+" false");
			}
		}
	}
	
	//Trace(t);
	
	if( t != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function testParToNote(dtNote,dtPara,event)
{	
	var t = 0;
	var i = 0;
	
	for( id in dtPara )
	{
		if( dtPara[id] )
		{
			if( dtNote[id].indexOf(event.pitch) != -1 )
			{
				//Trace(id+" getPara "+dtPara[id]+" n:"+dtNote[id]+" e:"+dtNote[id].indexOf(event.pitch)+" p:"+event.pitch);
				t++
				i = id;
			}
			else
			{
				//Trace(id+" getPara "+dtPara[id]+" n:"+dtNote[id]+" e:"+dtNote[id].indexOf(event.pitch)+" p:"+event.pitch);
				
			}
		}
	}
	
	var out = [t,i];
	//Trace(out);
	return out;
}

function safeVel(midi)
{
	if( midi > 127)
	{
		midi = 127;
	}
	
	if(midi < 1)
	{
		midi = 1;
	}
	
	////Trace(midi);
	
	return midi;
}

/* tools */


/* midi */
function HandleMIDI(event) {
	//HandleMIDI
	
	

	config = [];
	var hiR = [];
	var loR = [];

	var configCln = [];
	var solo = [];
	
	var velOffset = [];

	var accEvery = [];
	var accVel = [];
	var accRandVel = [];

	var deleyEvery = [];
	var deleyAmm = [];
	var deleyRand = [];

	var huVel = [];
	var huTime = [];

	// parametri ------------------------------------------------ |
	for (s in instance) {
		var label = "[" + s.toUpperCase() + "]";
		solo[s] = GetParameter("Solo " + label);

		huVel[s] = GetParameter("Humanize Vel " + label);
		huTime[s] = GetParameter("Humanize Time " + label);

		velOffset[s] = GetParameter("Velocity Offset " + label);
		huVel[s] = GetParameter("Humanize Vel " + label);

		accEvery[s] = GetParameter("Accent Every " + label);
		accVel[s] = GetParameter("Accent Vel " + label);
		accRandVel[s] = GetParameter("Random Vel " + label);
		
		deleyEvery[s] = GetParameter("Delay Every " + label);
		deleyAmm[s] = GetParameter("Delay Ammount " + label);
		deleyRand[s] = GetParameter("Random Delay " + label);

		/*if (GetParameter("Hi Range " + label) != 128) {
			hiR[s] = GetParameter("Hi Range " + label);
		}*/

		/*if (GetParameter("Low Range " + label) != 128) {
			loR[s] = GetParameter("Low Range " + label);
		}*/
			//Trace(s+" "+instance[s]);
		
	}
	// parametri ------------------------------------------------ |
	

	// lista note valide ----------------- |
	for (id in instance) {
		var note = "";
		
		for(nt in instance[id])
		{

			ntName = instance[id][nt].toUpperCase();

			note += NOTES.indexOf(ntName) + ",";
		}
		
		config[id] = note;
	}
	
	// lista note valide ----------------- |
	
	// velocity mixer ------------------------------- |	
	var getVelOffset = testParToNote(config,velOffset,event);
	var getVelOffsetItem = getVelOffset[1];
	var getOffsetVal = velOffset[ getVelOffsetItem ];
	
	if( getVelOffset[0] != 0)
	{	
		event.velocity = safeVel(event.velocity+getOffsetVal);
		//Trace("mix "+getOffsetVal+" vel"+event);
	}
	// velocity mixer ------------------------------- |
	
	// humanize vel ------------------------------- |
	var tsHuVel = testParToNote(config,huVel,event);
	var randVel = huVel[ tsHuVel[1] ];
	
	if( tsHuVel[0] != 0  )
	{
		////Trace("huVel!! "+randVel);
		randVel = rand(0,randVel);
		event.velocity = safeVel(event.velocity+randVel);
	}
	else
	{
		////Trace("...");
	}
	
	// humanize vel ------------------------------- |
	
	// humanize time ------------------------------- |
	var tsHuTime = testParToNote(config,huTime,event);
	var randTime = huTime[ tsHuTime[1] ];
	
	var timeOut =0;
	
	if( tsHuTime[0] != 0  )
	{
		////Trace("huTime!! "+randTime);
		timeOut = rand(0,randTime);
		
	}
	else
	{
		
	}
	
	// humanize time ------------------------------- |
	
	// accent --------------------------------- |
	var getAcc = testParToNote(config,accEvery,event);
	var getAccItem = getAcc[1];
	var getAccInter = accEvery[getAccItem];
	
	//Trace(getAcc+getAccItem+getAccInter);

	if( getAcc[0] != 0 )
	{
		//Trace(getAccItem+" - "+getAccInter);
		if(event instanceof NoteOn)
		{
			if( roundsA[getAccItem] % getAccInter == 0 )
			{
				event.velocity = safeVel( event.velocity + accVel[ getAccItem ] );
				//event.velocity = safeVel( event.velocity + rand( event.velocity,accRandVel[getAccItem] ) );
				var rVel = event.velocity+accRandVel[getAccItem];
				event.velocity = safeVel( rand( event.velocity,rVel ) );
				//Trace("## "+event.velocity);
			}
			else
			{
				//Trace(":: "+event.velocity);
			}
			
			roundsA[getAccItem]++;
		}
		
	}
	// accent --------------------------------- |
	
	// swing ---------------------------------- |
	
	var getSw = testParToNote(config,deleyEvery,event);
	var getSwItem = getSw[1];
	var getSwInter = deleyEvery[getSwItem];
	
	//Trace(getSw[1]+" "+getSwItem+" "+getSwInter+" "+getSwInter+" "+roundsT[getSwItem]);
	
	var swTime = 0;
	
	if( getSw[0] != 0)
	{
		if(event instanceof NoteOn)
		{
				if( roundsT[getSwItem] % getSwInter == 0 )
				{
					swTime = deleyAmm[getSwItem];

					//Trace("sw# "+swTime);
				}
				else
				{
					//Trace("sw: "+swTime);
				}
				
				roundsT[getSwItem]++;
				
		}
	}
	
	
	// swing ---------------------------------- |
	
	
	// solo ---------------------------- |
	if (!allSolo(solo)) {
		//Trace("solo OFF")
		
		if( timeOut+swTime > DL)
		{
			Trace("max deley: /"+DL+"ms");
			DL = timeOut+swTime;
		}
		
		if (event instanceof NoteOn) {
			var on = new NoteOn(event);
			on.sendAfterMilliseconds(timeOut+swTime);
		}

		if (event instanceof NoteOff) {
			var off = new NoteOff(event);
			off.sendAfterMilliseconds(timeOut+swTime);
		}

	} else {
		//Trace("solo ON");		
		if( getSolo(config,solo,event) )
		{
			//Trace("true "+event);
			if (event instanceof NoteOn) {
				var on = new NoteOn(event);
				on.sendAfterMilliseconds(timeOut+swTime);
			}

			if (event instanceof NoteOff) {
				var off = new NoteOff(event);
				off.sendAfterMilliseconds(timeOut+swTime);
			}
		}

	}
	

	// solo ---------------------------- |
}
