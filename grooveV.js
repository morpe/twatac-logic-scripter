/*
TwatacGrooveV 0.1
by marco fabre
*/

// example: simple pass through and MIDI monitor

//config
instance = 10;
instance++;

function rand(min, max) {
	var out = Math.round(Math.random() * (max - min) + min);
	return out;
}

//var NOTES = MIDI._noteNames;

var NOTES = [
'C-2', 'C#-2', 'D-2', 'D#-2', 'E-2', 'F-2', 'F#-2', 'G-2', 'G#-2', 'A-2', 'A#-2', 'B-2',
'C-1', 'C#-1', 'D-1', 'D#-1', 'E-1', 'F-1', 'F#-1', 'G-1', 'G#-1', 'A-1', 'A#-1', 'B-1',
'C0', 'C#0', 'D0', 'D#0', 'E0', 'F0', 'F#0', 'G0', 'G#0', 'A0', 'A#0', 'B0',
'C1', 'C#1', 'D1', 'D#1', 'E1', 'F1', 'F#1', 'G1', 'G#1', 'A1', 'A#1', 'B1',
'C2', 'C#2', 'D2', 'D#2', 'E2', 'F2', 'F#2', 'G2', 'G#2', 'A2', 'A#2', 'B2',
'C3', 'C#3', 'D3', 'D#3', 'E3', 'F3', 'F#3', 'G3', 'G#3', 'A3', 'A#3', 'B3',
'C4', 'C#4', 'D4', 'D#4', 'E4', 'F4', 'F#4', 'G4', 'G#4', 'A4', 'A#4', 'B4',
'C5', 'C#5', 'D5', 'D#5', 'E5', 'F5', 'F#5', 'G5', 'G#5', 'A5', 'A#5', 'B5',
'C6', 'C#6', 'D6', 'D#6', 'E6', 'F6', 'F#6', 'G6', 'G#6', 'A6', 'A#6', 'B6',
'C7', 'C#7', 'D7', 'D#7', 'E7', 'F7', 'F#7', 'G7', 'G#7', 'A7', 'A#7', 'B7',
'C8', 'C#8', 'D8', 'D#8', 'E8', 'F8', 'F#8', 'G8', '---'];

/* -- gui -- */

var PluginParameters = [
	{
		name: "Config:",
		type: "text"
	}

];

velC = [];
delC = [];
var solo = [];
var oneSolo = [];



for (i = 1; i < instance; i++) {

	velC[i] = 0;
	delC[i] = 0;

	PluginParameters.push({
		name: "Target" + i,
		type: "menu",
		valueStrings: NOTES,
		numberOfSteps: 127,
		defaultValue: 128,
	});
}


for (i = 1; i < instance; i++) {
	PluginParameters.push({
			name: "Target Paramaters: " + i,
			type: "text"
		},

		{
			name: 'Target' + i + ' Solo',
			type: "menu"
		},
		{
			name: "Velocity Interval" + i,
			type: "lin",
			numberOfSteps: 32,
			minValue: 0,
			maxValue: 32,
			defaultValue: 4
		}, {
			name: "Velocity Add" + i,
			type: "lin",
			numberOfSteps: 127,
			minValue: 0,
			maxValue: 127,
			defaultValue: 11
		}, {
			name: "Velocity Chaos" + i,
			type: "lin",
			numberOfSteps: 127,
			minValue: 0,
			maxValue: 127,
			defaultValue: 0
		}, {
			name: "Swing Interval" + i,
			type: "lin",
			numberOfSteps: 32,
			minValue: 0,
			maxValue: 32,
			defaultValue: 3
		}, {
			name: "Delay" + i,
			type: "lin",
			numberOfSteps: 1000,
			minValue: 0,
			maxValue: 1000,
			defaultValue: 0
		}, {
			name: "Delay Chaos" + i,
			type: "lin",
			numberOfSteps: 1000,
			minValue: 0,
			maxValue: 1000,
			defaultValue: 0
		}, {
			name: "Humanize Vel" + i,
			type: "lin",
			numberOfSteps: 127,
			minValue: 0,
			maxValue: 127,
			defaultValue: 0
		}, {
			name: "Humanize Time" + i,
			type: "lin",
			numberOfSteps: 1000,
			minValue: 0,
			maxValue: 1000,
			defaultValue: 0
		}
	);
}

/* -- gui -- */

targets = [];

var activeNotes = [];

function HandleMIDI(event) {

	if (event instanceof NoteOn) {
		//noteON		
		for (i = 1; i < instance; i++) {
			target = GetParameter("Target" + i);

			velInterval = GetParameter("Velocity Interval" + i);
			velAdd = GetParameter("Velocity Add" + i);
			velChaos = GetParameter("Velocity Chaos" + i);

			swingInterval = GetParameter("Swing Interval" + i);
			delay = GetParameter("Delay" + i);
			delayChaos = GetParameter("Delay Chaos" + i);

			huVel = GetParameter("Humanize Vel" + i);
			huTime = GetParameter("Humanize Time" + i);
			oneSolo = GetParameter("Target" + i+" Solo");

			if (target == event.pitch) {
				//target true
				
				event.velocity = MIDI.normalizeData(event.velocity + rand(0, huVel));

				
				if (velC[i] % velInterval == 0) {
					//vel interval True
					//Trace(velC[i] + " ## tg " + target);
					
					event.velocity = MIDI.normalizeData(event.velocity + rand(velAdd, velAdd + velChaos));

					
				} else {
					//Trace(velC[i]+" tg " + target);
					if (velInterval != 0) {
						//vel interval False
					}
				}
				
				velC[i] ++;

				timeToOut = rand(0, huTime);
				//Trace("huTime1" + i + " " + timeToOut);

				if (delC[i] % swingInterval == 0) {
					timeToOut = rand(delay, delay + delayChaos);
					//Trace("swing" + i + " " + timeToOut);
				} else {
					timeToOut = rand(timeToOut, huTime);
					//Trace("norm" + i + " " + timeToOut);
				}

				delC[i] ++;

			} else {
				//target false1
				timeToOut = 0;

			}
			//end FOR
		}
		
		//note ON
		var noteOn = new NoteOn(event);
		noteOn.send();	
	} else {
		//NoteOFF
		var noteOff = new NoteOff(event);
		noteOff.send();
		
	}
	//event end
	
	//event.sendAfterMilliseconds(timeToOut);
	

		
}


/*


function HandleMIDI(event)
{
	//event.trace();
	//event.send();
	
		if (event instanceof NoteOn)
		{
			Trace("ON");
			//event.send();
			var noteOn = new NoteOn(event);
			
			noteOn.send();
			
		}
		else
		{
			Trace("OFF");
			var noteOff = new NoteOff(event);
			//event.send();
			
			noteOff.send();
		}
	
}








*/




