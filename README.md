*Studio e prove con Scripter di Logic!*

# Twatac-Logic-Scripter
---------------------

### Intro

Nella programmazione midi per parti di batteria ho sempre fatto gran parte lelle operazioni a mano tipo dare accenti (velocity) e ritardi alle note a intervalli prestabiliti (ogni 2, 3 4 ecc..)
il primo script per scripter automatizza appunto questa tediosa lavorazione sull midi.


# grooveV2.js - 1.0
--------------------

## Installazione

basta copiare e incollare *grooveV2.js* dentro il plugin **Scripter** oppure caricare *TwatacGrooveV2.pst* con **Scripter**

* **requisiti:** grooveV2.js richiede Logic Pro X e il plugin Scripter


## Utilizzo

Nella sezione **config** segli le note che si desideri alterare in realtime con i vari parametri

* è posibile modificare le etichette della GUI e il numero di elementi modificando l'array `instance`


#### Parametri

* **Accent Every:** definisce l'intervallo tra nota accentata e normale, non fa riremimento al numero della battuta, ma conta singolarmente la nota ogni volta che viene suonata.

* **Accent Vel:** definise l'offset della velocity per le note accentate

* **Random Vel:** definise un offset casuale per le note accentate

* **Deley Every:** definisce l'intervallo tra nota accentata e normale, non fa riremimento al numero della battuta, ma conta singolarmente la nota ogni volta che viene suonata.

* **Delay Ammount:** definise ritardo in ms per le note accentate


* **Random Delay:** definise ritardo casuale in ms per le note accentate

* **Humanize Vel:** definisce un offset casuale per la velocity, per tutte le note, accentate e non

* **Humanize Time:** definisce un ritardo casuale per tutte le note, accentate e non

#### Screenshot
![image](https://bytebucket.org/morpe/twatac-logic-scripter/raw/a3e69644d7d36a4b12aaf3593052552527bb2b8c/screenshot.png)

#### altro
----------

**allScript.js**
qui ho incollato tutti gli script (preset) creati dagli sviluppatori di Logic, sono proprietà intelletuale di Apple
il file è solo di consultazione.

*Morpe*
