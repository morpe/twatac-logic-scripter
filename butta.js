/*
TwatacGrooveV 0.1
by marco fabre0
*/

// example: simple pass through and MIDI monitor

a = 0;
b = 0;

function rand(min, max) {
	var out = Math.round(Math.random() * (max - min) + min);
	
		//Trace(":: Rand "+out);
		
			if( out > 127 )
			{
				//Trace("in > 127 "+out+" DHO");
				
				//out = 127;
			}
			else
			{
				//Trace("in < 127 "+out);
				
				//out = 55;
			}

	
	return out;
}

var NOTES = MIDI._noteNames;	
/* -- gui -- */
var PluginParameters = [{
		name:"Target1",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:36,
		},
		{
		name:"Interval1",
		type:"lin",
		numberOfSteps:32,
		minValue:0,
		maxValue:32,
		defaultValue: 4
		},
		{
		name:"Interval1 Chaos",
		type:"lin",
		numberOfSteps:127,
		minValue:0,
		maxValue:127,
		defaultValue: 0
		},
		{
		name:"Velocity1 Add",
		type:"lin",
		numberOfSteps:126,
		minValue:1,
		maxValue:127,
		defaultValue: 4
		},
		{
		name:"Velocity1 Chaos",
		type:"lin",
		numberOfSteps:127,
		minValue:0,
		maxValue:127,
		defaultValue: 0
		}

];
/* -- gui -- */

//var info = GetTimingInfo();

		function interval()
		{
			var int = GetParameter("Interval1");
			var chaos = GetParameter("Interval1 Chaos");
			
			return rand(int,int+chaos);
		}
		
		function velocity(vel)
		{
			var vel = GetParameter("Velocity1 Add");
			var chaos = GetParameter("Velocity1 Chaos");
			
			Trace( "rand: "+rand(vel,vel+chaos) );
			Trace( "vel: "+vel );
			
			return rand(vel,vel+chaos);
		}


function HandleMIDI(event) {

//Trace("val:"+target1+" "+int1H+" "+int1L+" "+inc1H+" "+inc1L);



var target1 = GetParameter("Target1");

	if (event instanceof NoteOn) {
		//Trace('if');
		//Trace("target: "+ target1 +" == "+event.pitch );
		if (event.pitch == target1 ) {
			if (a % interval() == 0) {
				//Trace('on vel '+ velocity() );
				event.velocity += velocity();
			} else {
				//Trace('on ##');
			}

			a++;
			Trace('count ## ' + a);
		} else {
			//Trace('off');
		}
	} else {
		//Trace('else');
		//Trace(event);
	}

	event.send();
}