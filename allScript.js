/**
		Channel Filter:

			This script only allows events on the specified channel to be sent. 
			When changing the Pass Only Channel, the MIDI.allNotesOff() message 
			is sent, to prevent stuck notes that were sent on the previously
			selected channel. 
*/

        
var CHANNEL_TO_PASS = 1;
   
function HandleMIDI(event) {
		if (event.channel == CHANNEL_TO_PASS) {
				event.send();
		}
}

function ParameterChanged (param, value) {
		if (param === 0) {
				CHANNEL_TO_PASS = value;
				MIDI.allNotesOff();
		}
}

var PluginParameters = [{
		name:"Pass Only Channel", 
		type:"linear", 
		minValue:1, 
		maxValue:16, 
		numberOfSteps:15, 
		defaultValue:1
}];

/* 
Delay Note events until next beat
Transport must be running
*/

NeedsTimingInfo = true;

function HandleMIDI(e) {
	var info = GetTimingInfo();
	
	if (e instanceof NoteOn)
		e.sendAtBeat(Math.ceil(info.blockStartBeat));
	else if (e instanceof NoteOff)
		e.sendAtBeat(Math.ceil(info.blockStartBeat) + 1);
	else
		e.send();
}

/*
		Drum Kit Designer Mapper.pst
		
		This PST allows you to change the notes that trigger the various Drum Kit
		Designer voices. 

		You can also set which controller changes the articulationID for the HiHat
		Opening and if that controller should be inverted.
		
		For convenience, there is a flag "SHOW_INCOMING_NOTE" that you can set to 
		true so that you can see the note that your controller pad/key triggers. 
		This is set to false by default. 
*/
ResetParameterDefaults = true;			//Global Scripter variable, for reseting 
																	//PluginParameter values to their default
																	//value when re-running the script
var SHOW_INCOMING_NOTE = false;     //set this to true, to print the incoming
																	//note to the console

var INVERT_HIHAT_MOD = true;				//if HiHat controller should be inverted
var HIHAT_CONTROLLER = 1;						//current controller number
var CURRENT_HIHAT_ARTICULATION = 1;	//current articulation ID for Closed Hi-Hat
var NOTES = MIDI._noteNames;				//array of MIDI note names for menu items

const DEFAULT_MAPPINGS = {
		kick:36,
		snareEdge:34,
		snareCenter:38,
		rimshot:40,
		rimshotEdge:32,
		sideStick:37,
		stick:75,
		clap:39,
		lowTom:41,
		lowMidTom:45,
		highMidTom:47,
		highTom:48,
		hhClosedTip:42,
		hhShank:44,
		hhOpenEdge:46,
		hhFootClose:33,
		hhFootSplash:31,
		rideOut:51,
		rideIn:59,
		rideEdge:52,
		rideBell:53,
		crashLeft:49,
		crashRight:57,
		crashLeftStop:28,
		crashRightStop:29,
		tambourine:54,
		shaker:70,
}
var CUSTOM_MAPPINGS = {};

//------------------------------ HandleMIDI () ---------------------------------
function HandleMIDI (event) {
		if(event instanceof NoteOn) {		
				//print the incoming note to the console
				if (SHOW_INCOMING_NOTE) {
						Trace(MIDI.noteName(event.pitch));
				}
				
				//find the original note value, based on the incoming note value
				for (voice in CUSTOM_MAPPINGS) {
						if (CUSTOM_MAPPINGS[voice] === event.pitch) {
								event.pitch = DEFAULT_MAPPINGS[voice]; //get original note
								var noteOff = new NoteOff(event);

								//if HH is being triggered assign the appropriate articulationID
								if (event.pitch === DEFAULT_MAPPINGS.hhOpenEdge 
								|| event.pitch === DEFAULT_MAPPINGS.hhClosedTip
								|| event.pitch === DEFAULT_MAPPINGS.hhShank) {
										event.articulationID = CURRENT_HIHAT_ARTICULATION;
										noteOff.articulationID = CURRENT_HIHAT_ARTICULATION;
								}

								event.send();
								noteOff.sendAfterMilliseconds(100);
								break;
						} 
				}
		}
		else if(event instanceof NoteOff) {
				//do nothing
		}	else if (event instanceof ControlChange) {
				if (event.number === HIHAT_CONTROLLER) {
						updateHiHat(event.value); //update the articulation ID for HiHat
				} else {
						event.send();
				}
		} else {
				event.send();
		}
}

//-------------------------- ParameterChanged () -------------------------------
function ParameterChanged (param, value) {
		switch(param) {
		case 0:
				CUSTOM_MAPPINGS["kick"] = value;
				break;
		case 1:
				CUSTOM_MAPPINGS["snareEdge"] = value;
				break;
		case 2:
				CUSTOM_MAPPINGS["snareCenter"] = value;
				break;
		case 3:
				CUSTOM_MAPPINGS["rimshot"] = value;
				break;
		case 4:
				CUSTOM_MAPPINGS["rimshotEdge"] = value;
				break;
		case 5:
				CUSTOM_MAPPINGS["sideStick"] = value;
				break;
		case 6:
				CUSTOM_MAPPINGS["stick"] = value;
				break;
		case 7:
				CUSTOM_MAPPINGS["clap"] = value;
				break;
		case 8:
				CUSTOM_MAPPINGS["lowTom"] = value;
				break;
		case 9:
				CUSTOM_MAPPINGS["lowMidTom"] = value;
				break;
		case 10:
				CUSTOM_MAPPINGS["highMidTom"] = value;
				break;
		case 11:
				CUSTOM_MAPPINGS["highTom"] = value;
				break;
		case 12:
				CUSTOM_MAPPINGS["hhClosedTip"] = value;
				break;
		case 13:
				CUSTOM_MAPPINGS["hhShank"] = value;
				break;
		case 14:
				CUSTOM_MAPPINGS["hhOpenEdge"] = value;
				break;
		case 15:
				CUSTOM_MAPPINGS["hhFootClose"] = value;
				break;
		case 16:
				CUSTOM_MAPPINGS["hhFootSplash"] = value;
				break;
		case 17:
				CUSTOM_MAPPINGS["rideOut"] = value;
				break;
		case 18:
				CUSTOM_MAPPINGS["rideIn"] = value;
				break;
		case 19:
				CUSTOM_MAPPINGS["rideEdge"] = value;
				break;
		case 20:
				CUSTOM_MAPPINGS["rideBell"] = value;
				break;
		case 21:
				CUSTOM_MAPPINGS["crashLeft"] = value;
				break;
		case 22:
				CUSTOM_MAPPINGS["crashRight"] = value;
				break;
		case 23:
				CUSTOM_MAPPINGS["crashLeftStop"] = value;
				break;
		case 24:
				CUSTOM_MAPPINGS["crashRightStop"] = value;
				break;
		case 25:
				CUSTOM_MAPPINGS["tambourine"] = value;
				break;
		case 26:
				CUSTOM_MAPPINGS["shaker"] = value;
				break;
		//invert hi hat opening
		case 27:                  
				if (value === 0) {
						INVERT_HIHAT_MOD = false;
				} else {
						INVERT_HIHAT_MOD = true;
				}
				break;
		//set controller number for changing HiHat artID
		case 28:
				//subtract 1, to make up for the offset of adding "-- Off --" to the menu options
				HIHAT_CONTROLLER = value - 1;
				//if "Hi-Hat Opening Via" is set to Off, default to closed articulation
				if (HIHAT_CONTROLLER < 0) {
						updateHiHat(1)
				}
				break;
		default:
				Trace("ParameterChanged(): error: unknown parameter index.");
		}
}

//----------------------------- updateHiHat () ---------------------------------
/**
	This function updates the articulation ID for HiHat, based on the controller 
	value. If Invert Hi-Hat Opening is true, use the inverted value.

	@param value is the incoming MIDI CC value
*/
function updateHiHat (value) {
		if (value > 114) {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 1 : 7;
		} else if (value > 94) {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 2 : 6;
		} else if (value > 74) {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 3 : 5;
		} else if (value > 54) {
				CURRENT_HIHAT_ARTICULATION = 4;
		} else if (value > 34) {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 5 : 3;
		} else if (value > 14) {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 6 : 2;
		} else {
				CURRENT_HIHAT_ARTICULATION = (INVERT_HIHAT_MOD) ? 7 : 1;
		};
}

//create numbered MIDI CC names, with added "-- Off --" option
var MIDI_CC_NAMES = ["-- Off --"];
for (var i = 0; i < MIDI._ccNames.length; i++) {
		MIDI_CC_NAMES.push(i + " - " + MIDI._ccNames[i]);
}



//create UI --------------------------------------------------------------------
var PluginParameters = [{
		name:"Kick",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.kick,
}, {
		name:"Snare Edge",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.snareEdge,		
}, {
		name:"Snare Center",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.snareCenter,
}, {
		name:"Rimshot",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rimshot,
}, {
		name:"Rimshot Edge",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rimshotEdge,
}, {
		name:"Side Stick",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.sideStick,
}, {
		name:"Stick",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.stick,
}, {
		name:"Clap",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.clap,								
}, {
		name:"Low Tom",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.lowTom,
}, {
		name:"Low-Mid Tom",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.lowMidTom,
}, {
		name:"High-Mid Tom",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.highMidTom,
}, {
		name:"High Tom",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.highTom,
}, {
		name:"Hi-Hat Closed Tip",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.hhClosedTip,
}, {
		name:"Hi-Hat Shank",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.hhShank,	
}, {
		name:"Hi-Hat Open Edge",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.hhOpenEdge,
}, {
		name:"Hi-Hat Foot Close",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.hhFootClose,
}, {
		name:"Hi-Hat Foot Splash",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.hhFootSplash,
}, {
		name:"Ride Out",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rideOut,
}, {
		name:"Ride In",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rideIn,
}, {
		name:"Ride Edge",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rideEdge,
}, {
		name:"Ride Bell",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.rideBell,
}, {
		name:"Crash Left",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.crashLeft,
}, {
		name:"Crash Right",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.crashRight,
}, {
		name:"Crash Left Stop",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.crashLeftStop,
}, {
		name:"Crash Right Stop",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.crashRightStop,			
}, {
		name:"Tambourine",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.tambourine,
}, {
		name:"Shaker",
		type:"menu",
		valueStrings:NOTES,
		numberOfSteps:127,
		defaultValue:DEFAULT_MAPPINGS.shaker,
}, {
		name:"Invert Hi-Hat Opening",
		type:"checkbox",
		defaultValue:0,
		
}, {
		name:"Hi-Hat Opening Via",
		type:"menu",
		valueStrings:MIDI_CC_NAMES,
		defaultValue:2,
		numberOfSteps:MIDI_CC_NAMES.length - 1
}];



/**
		Drum Kit Designer V-Drum Compatibility 

		This script maps a V-Drum to work with Drum Kit Designer, and is an example
		of how a user can use Scripter to customize their own third party 
		electronic drum kits to work with Drum Kit Designer. Please make sure
		to set the Drum Kit Designer extend parameter "Input Mapping" to GM for this
		script to work correctly. 
*/

//variables to track the current articulation for snare, ride, and hi-hats
var SNARE_POS = 0;
var RIDE_POS = 0;
var HH_OPEN = 0;

function HandleMIDI(event) {
		if (event instanceof ControlChange) {
				// set SNARE_POS variable according to midi CC
				if  (event.number == 16) { 
						if (event.value > 18) {
								SNARE_POS = 1; //offCenter
						} else {
								SNARE_POS = 0; //center
						}
				} 
				// set RIDE_POS variable according to midi CC
				else if (event.number == 17) {
						if (event.value < 90) {
								RIDE_POS = 1; //RideInner
						}	else { 
								RIDE_POS = 0; //normal
						}
				}	
				//set HH_OPEN variable according to midi CC
				else if (event.number == 4) { 
						if (event.value > 114) {
								HH_OPEN = 1;
						} else if (event.value > 94) {
								HH_OPEN = 2;
						} else if (event.value > 74) {
								HH_OPEN = 3
						} else if (event.value > 54) {
								HH_OPEN = 4;
						} else if (event.value > 34) {
								HH_OPEN = 5;
						} else if (event.value > 14) {
								HH_OPEN = 6;
						} else {
								HH_OPEN = 7;
						}
				}
		} else if (event instanceof NoteOn) { 
				switch (event.pitch) {		 		
				case 35: 
						event.pitch = 36; //Kick Rim to Kick 
						break;	 
				case 38: 
						if (SNARE_POS == 1) {
								event.pitch = 34; //Snare OffCenter
						}
						break;	 
				case 22: 
						event.pitch = 46; //HH Rim
						break;	 
				case 26: 
						event.pitch = 46; //HH Edge 
						break;	 
				case 44: 
						event.pitch = 33; //HH foot
						break;
				case 46: 
						event.pitch = 42; //HH top 
						break;
				case 50: 
						event.pitch = 48; //Tom1 Rim 
						break;	 
				case 45: 
						event.pitch = 47; //Tom2 
						break;	 
				case 43: 
						event.pitch = 45; //Tom3 
						break;
				case 58: 
						event.pitch = 45; //Tom3 Rim 
						break;	 
				case 41: 
						event.pitch = 43; //Tom4 
						break;
				case 39: 
						event.pitch = 43; //Tom4 Rim 
						break;	 
				case 59: 
						event.pitch = 52; //Ride Edge 
						break;	 
				case 51: 
						if (RIDE_POS == 1) {
								event.pitch = 59;  //Ride Inner
						}
						break;	
				case 55: 
						event.pitch = 49; //Crash1 
						break;	 
				case 52: 
						event.pitch = 57; //Crash2 Rim 
						break;
				default:
						//do nothing
				}
			
				//set articulation ID for HH opening
				if (event.pitch == 46 || event.pitch == 42) {  
						switch (HH_OPEN) {
						case 1: 
								event.articulationID = 1; 
								break;
						case 2: 
								event.articulationID = 2; 
								break;
						case 3: 
								event.articulationID = 3; 
								break;
						case 4: 
								event.articulationID = 4; 
								break;
						case 5: 
								event.articulationID = 5; 
								break;
						case 6: 
								event.articulationID = 6; 
								break;
						case 7: 
								event.articulationID = 7; 
								break;
						default:
								//do nothing			
						}
				}				

				event.send();
				
				//create and send note off
				var off = new NoteOff(event); 
				off.articulationID = event.articulationID;   
				off.sendAfterMilliseconds(100)	
		}
}


/*
    Drum Probability Sequencer: 
    
        This script generates a probability sequencer that  
        allows you to set the probability of a step being triggered.
    
        A value of 100% will always play, a value of 50% will 
        play half of the time, a value of 0% will never play, etc.
                                
        You can choose the MIDI Note to send, and you can either 
        use a fixed velocity for all steps, or you can have the 
        velocity follow the step level (less probable steps will 
        have lower velocities). 
        
        By default, this script loads with a basic pattern that
        sequences Kick, Snare, Closed HH, and Open HH. You can
        also load a flat pattern (all values at 0), or a random
        pattern. To do this, change the variable "configuration"
        to 0, 1, or 2
        
        You may also change the number of tracks to sequence
        (numberOfTracks), and the number of steps in the
        pattern (numberOfSteps).
        
        A step is always equal to a 16th note, so 16 steps is 
        equal to 1 bar, 32 steps is equal to 2 bars, etc.    
        
        The swing slider is global for all voices.             
*/

//sequencer configuration (you can modify this, and re-run the script)
// 0 = default pattern (always 4 tracks, 16 steps)
// 1 = all sliders set to 0
// 2 = random values for all sliders
var configuration = 0;

//the number of tracks to sequence (you can modify this, and re-run the script)
var numberOfTracks = 4;

//the number of steps to sequence (you can modify this, and re-run the script)
var numberOfSteps = 16;

//define default pattern values for each track
var patternMatrix = 
[
    [ 100,  0,  0, 0, 100, 15,  0,  0, 100,  0,  0,  0, 100,  0, 50,  0],
    [   0,  0,  0, 0, 100,  0,  0, 25,   0, 25,  0,  0, 100, 15,  0, 50],
    [  25, 50,  0, 0,  25, 50,  0, 25,  25, 50,  0,  0,  25, 50,  0,  0],
    [   0,  0, 75, 0,   0,  0, 75,  0,   0,  0, 75, 25,   0,  0, 75,  0]
];

//define default pattern notes
var defaultNotes = [36, 38, 42, 46];

//define default pattern velocities
var defaultVelocity = [127, 100, 100, 100] 

//define default pattern "Velocity Follow" mode
var defaultVelocityFollow = [0, 1, 0, 0];

//needed to call GetTimingInfo()
var NeedsTimingInfo = true;
ResetParameterDefaults = true;

function ProcessMIDI() {

    var musicInfo = GetTimingInfo();
    
	  if (musicInfo.playing) {
		
        // calculate beat to schedule
		    var division = 4;
		    var lookAheadEnd = musicInfo.blockEndBeat;
		    var beatToSchedule = Math.ceil(musicInfo.blockStartBeat * division) / division;
		
		    // when cycling, find the beats that wrap around the last buffer
		    if (musicInfo.cycling && lookAheadEnd >= musicInfo.rightCycleBeat) {
		    
			      if (lookAheadEnd >= musicInfo.rightCycleBeat) {
				        var cycleBeats = musicInfo.rightCycleBeat - musicInfo.leftCycleBeat;
				        var cycleEnd = lookAheadEnd - cycleBeats;
			      }
		    }
		    
				var swing = GetParameter("Swing");
		    	var isSwinging = false;
		    	
		    	//only calculate swing if it is active
				if (swing > 50) {
						var divisionPercent = 1 / division;
						var swingPercent = (swing * 2) / 100;
						
						//calculate swing offset
						var swingOffset = (divisionPercent * swingPercent) - divisionPercent;

						if ((beatToSchedule / divisionPercent) % 2 === 0) {
								isSwinging = false;
						} else {
								isSwinging = true;
						}
				}		
				
        // loop through the beats that fall within this buffer
        while ((beatToSchedule >= musicInfo.blockStartBeat && beatToSchedule < lookAheadEnd)
        // including beats that wrap around the cycle point
        || (musicInfo.cycling && beatToSchedule < cycleEnd))
        {
            // adjust for cycle
            if (musicInfo.cycling && beatToSchedule >= musicInfo.rightCycleBeat) {
                beatToSchedule -= cycleBeats;
            }
			     
            var currentStep = ((beatToSchedule * 4) - 4) % numberOfSteps;
            
            //for each track, get the current step and check if it should be triggered
            for(var currentTrack = 1; currentTrack <= numberOfTracks; currentTrack++) {
                     
                //calculate the base index offset for this track      
                //1 is the offset for the Swing parameter  
                var indexOffset =  1 + (4 + numberOfSteps) * (currentTrack-1);       
                     
                //get the index of the current step        
                var stepIndex = (currentStep + 4) + indexOffset;
                
                probability = GetParameter(stepIndex); 
                                
                if(stepShouldPlay(probability) && GetParameter(indexOffset) == 0) {
                
                    var on = new NoteOn;
                    on.pitch = GetParameter(indexOffset + 1);
                 
                    var velocityLevel;
                    
                    //Velocity Follows: Step Level
                    if(GetParameter(3 + indexOffset) == 1) {
                        //scale range of 0 - 100, to 0 - 127
                        velocityLevel = Math.ceil(probability * 1.27);
                    }
                    //Velocity Follows: Slider
                    else {
                        velocityLevel = GetParameter(2 + indexOffset)
                    }
                            
                    if (isSwinging) {
                    		beatToSchedule += swingOffset;
                    }
                                        
                    on.velocity = velocityLevel;
                    on.sendAtBeat(beatToSchedule);
                
                    var off = new NoteOff(on);
                    off.sendAtBeat(beatToSchedule+ 0.1);      
                }
            }
                       			
			      // advance to next beat
			      beatToSchedule += 0.001;
			      beatToSchedule = Math.ceil(beatToSchedule * division) / division;
		    }
	  }
}

//returns true if a random number is less than or equal to the step probability 
function stepShouldPlay(stepValue) {

   var randomNumber = Math.ceil(Math.random()*100);
   
   if(randomNumber <= stepValue) {
       return true;
   }
   else {
       return false;
   }
}

//define the UI -----------------------------------------------------------------

//initialize PluginParameters
var PluginParameters = [];

//if the configuration is the default pattern
if(configuration == 0) {
    //always use 4 tracks and 16 steps 
    numberOfTracks = 4;
    numberOfSteps = 16;
}

//add a global Swing control
PluginParameters.push({
		name:"Swing",
		type:"linear",
		minValue:50,
		maxValue:99,
		numberOfSteps:49,
		defaultValue:50,
		unit:"%"
});

//create all controls for each track
for(var trackNum = 1; trackNum <= numberOfTracks; trackNum++) {

	//start at C1, and increment by a half step for each new track   
    var defaultNoteValue = 36 + (trackNum-1);
    
    var defaultVelocityValue = 100;
    var defaultVelocityMode = 0;
    
    //default pattern
    if(configuration == 0) {
        //use the default notes
        defaultNoteValue = defaultNotes[trackNum-1];
        defaultVelocityValue = defaultVelocity[trackNum-1];
        defaultVelocityMode = defaultVelocityFollow[trackNum-1];
    }
    //random pattern
    else if(configuration == 2) {
        defaultVelocityMode = 1; 
    }
    //flat values
    else {
        //keep original values
    }
    
    PluginParameters.push({name:"Voice " + trackNum + ": On/Off", 
                           type:"menu", valueStrings:["On", "Off"],
                           numberOfSteps:2,
                           minValue:0,
                           maxValue:1,
                           defaultValue:0,});
    
    //Note menu
    PluginParameters.push({name:"Voice " + trackNum + ": Note", 
                           type:"menu", valueStrings: MIDI._noteNames, 
                           defaultValue:defaultNoteValue, 
                           numberOfSteps:127,});
        
    //Velocity slider                     
    PluginParameters.push({name:"Voice " + trackNum + ": Velocity", 
                           minValue:1, maxValue:127, 
                           numberOfSteps:126, 
                           defaultValue:defaultVelocityValue, 
                           type:"linear",});
         
    //Velocity Follows menu                     
    PluginParameters.push({name:"Voice " + trackNum + ": Velocity Follows:", 
                           type:"menu", valueStrings:["Slider", "Step Level"], 
                           numberOfSteps: 2, 
                           minValue:0,
                           maxValue:1,
                           defaultValue:defaultVelocityMode,});
                         
    //create 16 steps for each track                     
    for(var stepNum = 1; stepNum <= numberOfSteps; stepNum++) {
        
        //probability amount for the current track
        var sliderValue;
        
        //default pattern
        if(configuration == 0) {
            sliderValue = patternMatrix[trackNum -1][stepNum - 1];
        }
        //random pattern
        else if(configuration == 2) {
            sliderValue = Math.ceil(Math.random()*100);
        }
        //flat values
        else {
            sliderValue = 0;
        }
    
        //Step Probability slider
        PluginParameters.push({name:"Voice " + trackNum + ": Step " 
                                  + stepNum + " Probability",
                               minValue:0, maxValue:100, 
                               numberOfSteps:100, 
                               defaultValue:sliderValue, 
                               type:"linear",
                               unit:"%"});
    }
};


/* 
		Guitar Strummer.pst

		Note: The transport must be playing for this effect to work.

		This script mimics the strumming and arpeggiating of a guitar, using actual
		guitar voicings.

		Chords can be mapped to a single note type, so that playing that single note
		will trigger the strumming or arpeggiating of the assigned chord.

		Playing a new chord before the previous chord has finished, will cancel the
		previous chord.

		Use the sustain pedal to let the chord sustain.

		Use the "Type" selector to switch between strumming and arpeggiating 
		settings.

	
		"Direction" determines the strumming direction: 
				Up = always play up strokes
				Down = always play down strokes
				Alternate : Always = alternate between down and up for every trigger
				Alternate : Down On New Chord = alternate between down and up, unless 
										you trigger a new chord, in which case, play down
				Follow Beats : 1/8 = Use the time line to determine when to play down 
										and up beats, at 1/8 note resolution. Down beats will
										trigger down strokes, and up beats will trigger up strokes
				Follow Beats : 1/16 = Use 1/16 note resolution to determine the stroke.
		Division: The time between each string on the guitar being played, (in ms 
				for strums and beats for arpeggios)
		Division Curve: (strum only) negative values speed up the stroke as it 
				plays, positive values slow down the stroke as it plays.
		Random Division: apply randomness to the division value
		Velocity Curve: negative values decrease each strings played velocity as the 
				stroke plays, positive values increase the velocity as the stroke plays.
		Random Velocity: apply randomness to the velocity values

		Keyboard Split Point : Notes <= this value trigger chords, notes above
				trigger single notes, so that you can play melodies over the chords.
		
		Chord menus: assign what chord the note will trigger
*/


//global variables -------------------------------------------------------------
NeedsTimingInfo = true;    //global scripter variable for accessing timing info
ResetParameterDefaults = true; //return controls to default values when running
																//script
var PREVIOUS_STRUM = 0;			//variable to track the last strum direction
var TIMING_ARRAY = [];			//array of times to trigger each string
var VELOCITY_ARRAY = [];		//array of velocities for each string
var CURRENT_CHORD = [];			//array of notes of the chord
var START_BEAT = 0;					//the beat when the chord was first triggered
var CURRENT_BEAT = 0;				//current beat
var PREVIOUS_BEAT = 0;			//last beat
var STRUM_STOPPED = false;	//if the strum was stopped
var SENT_NOTES = [];				//array to track which strings have already played
var LAST_NOTE_PLAYED;				//last note on the keyboard that was played
var PREVIOUS_CHORD = [];		//previous chord
var ACTIVE_NOTES = [];			//currently pressed keys on the keyboard
var DIVISION;								//division
var IS_DOWN_BEAT;						//tracks if current beat is down or up beat
var SUSTAIN_ENABLED;				//track sustain pedal state
var TEMPO;
var DEBUG = false;						//display debug messages in console

//global variables for each PluginParameter 
//these will all be initialized in ParameterChanged() when the Script is run
var TYPE;

var STRUM_DIRECTION;
var STRUM_DIVISION;
var STRUM_DIVISION_CURVE;
var STRUM_RANDOM_DIVISION;
var STRUM_VELOCITY_CURVE;
var STRUM_RANDOM_VELOCITY;

var ARP_DIRECTION;
var ARP_DIVISION;
var ARP_RANDOM_DIVISION;
var ARP_VELOCITY_CURVE;
var ARP_RANDOM_VELOCITY;

var KEYBOARD_SPLIT_POINT;

//tracks the assigned chord for base note
var C_CHORD;
var C_SHARP_CHORD;
var D_CHORD;
var D_SHARP_CHORD;
var E_CHORD;
var F_CHORD;
var F_SHARP_CHORD;
var G_CHORD;
var G_SHARP_CHORD;
var A_CHORD;
var A_SHARP_CHORD;
var B_CHORD;

//values for "Strum Division (beats)" menu 
var TIME_DIVISIONS = [
		"1/16 T", 
		"1/16", 
		"1/8 T", 
		"1/8", 
		"1/4 T", 
		"1/4", 
];
//values for direction
var DIRECTION_TYPES = [
		"Down",
		"Up",
		"Alternate : Always", 
		"Alternate : Down On New Chord", 
		"Follow Beats 1/8", 
		"Follow Beats 1/16"
];

//array to hold all chords
var ARRAY_OF_CHORDS = [];

//define chord names and notes ..........................
//low E = 40

//C
ARRAY_OF_CHORDS.push(new Chord("C Major", [48, 55, 60, 64, 67]));
ARRAY_OF_CHORDS.push(new Chord("C Minor", [48, 55, 60, 63, 67]));
ARRAY_OF_CHORDS.push(new Chord("C Diminished", [48, 54, 57, 63, 69]));
ARRAY_OF_CHORDS.push(new Chord("C Power Chord", [48, 55]));
//C#
ARRAY_OF_CHORDS.push(new Chord("C# Major", [49, 56, 61, 65, 68]));
ARRAY_OF_CHORDS.push(new Chord("C# Minor", [49, 56, 61, 64, 68]));
ARRAY_OF_CHORDS.push(new Chord("C# Diminished", [49, 55, 58, 64, 70]));
ARRAY_OF_CHORDS.push(new Chord("C# Power Chord", [49, 56]));
//D
ARRAY_OF_CHORDS.push(new Chord("D Major", [50, 57, 62, 66, 69]));
ARRAY_OF_CHORDS.push(new Chord("D Minor", [50, 57, 62, 65, 69]));
ARRAY_OF_CHORDS.push(new Chord("D Diminished", [50, 56, 59, 65, 71]));
ARRAY_OF_CHORDS.push(new Chord("D Power Chord", [50, 57]));
//D#
ARRAY_OF_CHORDS.push(new Chord("D# Major", [51, 58, 63, 67, 70]));
ARRAY_OF_CHORDS.push(new Chord("D# Minor", [51, 58, 63, 66, 70]));
ARRAY_OF_CHORDS.push(new Chord("D# Diminished", [51, 57, 60, 66, 72]));
ARRAY_OF_CHORDS.push(new Chord("D# Power Chord", [51, 58]));
//E
ARRAY_OF_CHORDS.push(new Chord("E Major", [40, 47, 52, 56, 59, 64]));
ARRAY_OF_CHORDS.push(new Chord("E Minor", [40, 47, 52, 55, 59, 64]));
ARRAY_OF_CHORDS.push(new Chord("E Diminished", [40, 46, 52, 55, 61, 67]));
ARRAY_OF_CHORDS.push(new Chord("E Power Chord", [40, 47]));
//F
ARRAY_OF_CHORDS.push(new Chord("F Major", [41, 48, 53, 57, 60, 65]));
ARRAY_OF_CHORDS.push(new Chord("F Minor", [41, 48, 53, 56, 60, 65]));
ARRAY_OF_CHORDS.push(new Chord("F Diminished", [41, 47, 53, 56, 62, 68]));
ARRAY_OF_CHORDS.push(new Chord("F Power Chord", [41, 48]));
//F#
ARRAY_OF_CHORDS.push(new Chord("F# Major", [42, 49, 54, 58, 61, 66]));
ARRAY_OF_CHORDS.push(new Chord("F# Minor", [42, 49, 54, 57, 61, 66]));
ARRAY_OF_CHORDS.push(new Chord("F# Diminished", [42, 48, 54, 57, 63, 69]));
ARRAY_OF_CHORDS.push(new Chord("F# Power Chord", [42, 49]));
//G
ARRAY_OF_CHORDS.push(new Chord("G Major", [43, 50, 55, 59, 62, 67]));
ARRAY_OF_CHORDS.push(new Chord("G Minor", [43, 50, 55, 58, 62, 67]));
ARRAY_OF_CHORDS.push(new Chord("G Diminished", [43, 49, 55, 58, 64, 70]));
ARRAY_OF_CHORDS.push(new Chord("G Power Chord", [43,50]));
//G#
ARRAY_OF_CHORDS.push(new Chord("G# Major", [44, 51, 56, 60, 63, 68]));
ARRAY_OF_CHORDS.push(new Chord("G# Minor", [44, 51, 56, 59, 63, 68]));
ARRAY_OF_CHORDS.push(new Chord("G# Diminished", [44, 50, 56, 59, 65, 71]));
ARRAY_OF_CHORDS.push(new Chord("G# Power Chord", [44, 51]));
//A
ARRAY_OF_CHORDS.push(new Chord("A Major", [45, 52, 57, 61, 64]));
ARRAY_OF_CHORDS.push(new Chord("A Minor", [45, 52, 57, 60, 64]));
ARRAY_OF_CHORDS.push(new Chord("A Diminished", [45, 51, 57, 60, 66, 72]));
ARRAY_OF_CHORDS.push(new Chord("A Power Chord", [45, 52]));
//A#
ARRAY_OF_CHORDS.push(new Chord("A# Major", [46, 53, 58, 62, 65]));
ARRAY_OF_CHORDS.push(new Chord("A# Minor", [41, 46, 53, 58, 61, 65]));
ARRAY_OF_CHORDS.push(new Chord("A# Diminished", [46, 52, 58, 61, 67, 73]));
ARRAY_OF_CHORDS.push(new Chord("A# Power Chord", [46, 53]));
//B
ARRAY_OF_CHORDS.push(new Chord("B Major", [47, 54, 59, 63, 66]));
ARRAY_OF_CHORDS.push(new Chord("B Minor", [47, 54, 59, 62, 66]));
ARRAY_OF_CHORDS.push(new Chord("B Diminished", [47, 53, 59, 62, 68, 74]));
ARRAY_OF_CHORDS.push(new Chord("B Power Chord", [47, 54]));

//PUSH YOUR OWN CHORD DEFINITIONS HERE!!! Just re-run the script, and the chords
//will be available via the Chord drop down menus. 

//array to access chord names via the Chord menus: go through all chords and 
//store their names
var CHORD_NAMES = [];
for (i = 0; i < ARRAY_OF_CHORDS.length; i++) {
		CHORD_NAMES.push(ARRAY_OF_CHORDS[i].name);
}

//Scripter Functions -----------------------------------------------------------

//_______________________________ HandleMIDI() _________________________________
/*
		Trigger Chords or Single notes, and track the Sustain Pedal state. Pass all
		other event types.	

		@param event is the incoming MIDI event
*/		
function HandleMIDI (event) {
		//NoteOn ...................................................................
		if(event instanceof NoteOn) {	
				//strum/arpeggio 
				if(event.pitch <= KEYBOARD_SPLIT_POINT) {
						triggerStrum(event);
				}
				//single notes
				else {
						event.send();
				}
		}
		//NoteOff ..................................................................
		else if (event instanceof NoteOff) {
				//cancel strum if the note is in the chord range, or if that note 
				//already triggered a chord (ie if splitpoint was changed while playing)
				if (event.pitch <= KEYBOARD_SPLIT_POINT 
				|| ACTIVE_NOTES.indexOf(event.pitch) !== -1) { 
						cancelStrum(event);												
				} else {
						event.send();  //single notes
				}
		}
		//Sustain Pedal ............................................................
		else if (event instanceof ControlChange && event.number === 64) {
				if (event.value <= 64) {
						SUSTAIN_ENABLED = false;
				}
				else {
						SUSTAIN_ENABLED = true;
				}
				event.send();
		}
		//All other events .........................................................
		else {
				event.send();
		}
}

//___________________________ ParameterChanged () ______________________________
/*
		Update variables when the corresponding UI element is changed.

		@param param is the index of the parameter that changed
		@param value is the new value of the param
*/		
function ParameterChanged (param, value) {
		switch (param) {
				case 1:
						TYPE = value;
						break;
				case 3:
						STRUM_DIRECTION = value;
						break;
				case 4:
						STRUM_DIVISION = value;
						break;
				case 5:
						STRUM_DIVISION_CURVE = value;
						break;
				case 6:
						STRUM_RANDOM_DIVISION = value;
						break;
				case 7:
						STRUM_VELOCITY_CURVE = value;
						break;
				case 8:
						STRUM_RANDOM_VELOCITY = value;
						break;
				case 10:
						ARP_DIRECTION = value;
						break;
				case 11:
						ARP_DIVISION = value;
						break;
				case 12:
						ARP_RANDOM_DIVISION = value;
						break;
				case 13:
						ARP_VELOCITY_CURVE = value;
						break;
				case 14:
						ARP_RANDOM_VELOCITY = value;
						break;
				case 16:
						KEYBOARD_SPLIT_POINT = value; 
						break;
				case 18:
						C_CHORD = value;
						break;
				case 19:
						C_SHARP_CHORD = value;
						break;
				case 20:
						D_CHORD = value;
						break;
				case 21:
						D_SHARP_CHORD = value;
						break;
				case 22:
						E_CHORD = value;
						break;
				case 23:
						F_CHORD = value;
						break;
				case 24:
						F_SHARP_CHORD = value;
						break;
				case 25:
						G_CHORD = value;
						break;
				case 26:
						G_SHARP_CHORD = value;
						break;
				case 27:
						A_CHORD = value;
						break;
				case 28:
						A_SHARP_CHORD = value;
						break;
				case 29:
						B_CHORD = value;
						break;
				default:
						if (DEBUG) {
								Trace("ParameterChanged(): error: unknown parameter index");
						}
		}
}

//______________________________ ProcessMIDI () ________________________________
/*
		Trigger scheduled events that fall within this process block
*/	
function ProcessMIDI () {
		var musicInfo = GetTimingInfo();
		TEMPO = musicInfo.tempo;
	  if (musicInfo.playing) {	
	  		var strumTime = TYPE;	//check if strum is in ms or beats
        switch (strumTime) {		
        		//ms
        		case 0:
		        		DIVISION = musicInfo.meterNumerator * 128;
    						break;
        		//beats
        		case 1:
        				DIVISION = 2 * getDivisionTime(ARP_DIVISION);
        				break;
        		default:
        				if (DEBUG) {
        						Trace("Error in strumTime");
        				}
        }

				//calculate the current beat, and lookahead time
		    var lookAheadEnd = musicInfo.blockEndBeat;
		    var beatToSchedule = 
		    						Math.ceil(musicInfo.blockStartBeat * DIVISION) / DIVISION;
				//track the current beat in a global variable
				CURRENT_BEAT = beatToSchedule;
				
				//calculate if current beat is a downbeat or an upbeat
				var divisionLength = 1/(DIVISION/128);
				var deviation = beatToSchedule - Math.floor(beatToSchedule);
				var direction = 0;
		
				if (TYPE === 0) {
						direction = STRUM_DIRECTION;
				} else {
						direction = ARP_DIRECTION;
				}
				
				if (direction == 4) {        //follow beats 1/8
						divisionLength *= 2;
				} else if(direction == 5) {  //follow beats 1/16
						if(deviation >= .5) {   
								deviation -= .5;
						}
				} else {
						//do nothing
				}
				
				var downRange = divisionLength - divisionLength / 2;
					
				//IS_DOWN_BEAT is accessed in getStrum()
				if ((deviation <= downRange ) || (deviation >= downRange * 3 )) {
						IS_DOWN_BEAT = true;
				} else {
						IS_DOWN_BEAT = false;
				}
	 		     			
       	//for each note in the chord (excluding the first one, which is sent 
       	//immediately)
				for (var i = 1; i < CURRENT_CHORD.length; i++) {
						//if STRUM_STOPPED is flagged
						if (STRUM_STOPPED) {
								if (DEBUG) {
										Trace("stopping loop");
								}
								
								//reset CURRENT_CHORD
								CURRENT_CHORD = [];
								break;
						}
            		            		
						//check if the current chord note is scheduled to be played in this 
						//block and the note has not already been played       
						if (TIMING_ARRAY[i] >= musicInfo.blockStartBeat
						&& TIMING_ARRAY[i] <= musicInfo.blockEndBeat
						&& SENT_NOTES.indexOf(CURRENT_CHORD[i]) === -1) {         
								//create the note event with pitch velocity and timing from the 
								//respective arrays   		
								var noteOn = new NoteOn();
								noteOn.pitch = CURRENT_CHORD[i];
								noteOn.velocity = VELOCITY_ARRAY[i];
								noteOn.sendAtBeat(TIMING_ARRAY[i]);
            				
            		//track that this note has been played 
								SENT_NOTES.push(CURRENT_CHORD[i]);

						} 
						//if the scheduled time has already passed, but the note wasn't sent
						//yet, send the note immediately
						else if (TIMING_ARRAY[i] < musicInfo.blockStartBeat 
							&& SENT_NOTES.indexOf(CURRENT_CHORD[i]) === -1) {
								var noteOn = new NoteOn();
								noteOn.pitch = CURRENT_CHORD[i];
								noteOn.velocity = VELOCITY_ARRAY[i];
								noteOn.sendAfterMilliseconds(i);
								
								SENT_NOTES.push(CURRENT_CHORD[i]);
						}
				} 
		}	else {
				//not playing
		}
}

//custom functions -------------------------------------------------------------

//____________________________ calculateStrum() ________________________________
/*
		This function calculates the note order, velocity, and time for all notes in
		the chord. These calculated values are read in ProcessMIDI() and played at 
		the specifed time
		
		It also immediately sends the first note of the chord as it is played.

		@param chord is the chord to strum
		@param velocity is the played velocity of a key
		@param direction is the calculated direction of the strum
*/	
function calculateStrum (chord, velocity, direction) {
	  SENT_NOTES = [];		//reset the sentNote tracking
		
		switch (direction) {		
				//DOWN 
				case 0:
						//do nothing
						if (DEBUG) { 
								Trace("Down");
						}
						break;
				//UP
				case 1:
						if (DEBUG) { 
								Trace("Up");
						}

						//invert array 
						var reversedChord = [];
						for (var i = chord.length - 1; i >= 0; i--) {
								reversedChord.push(chord[i]);
						}
						
						//replace original chord with reversed chord
						CURRENT_CHORD = reversedChord;
						break;
				default:
						if (DEBUG) {
								Trace("Error calculating strum!");
						}
		}
		
		var divisionLength = STRUM_DIVISION;
		var previousDivision = divisionLength;  //to track incremental offset
		var curve = 1 + STRUM_DIVISION_CURVE; //make positive
		
		//for all notes in the chord
		for (var i = 0; i < CURRENT_CHORD.length; i++) {
				//play the first note immediately ----------------------------------
				if (i === 0) {		
						var firstNote = new NoteOn();
						firstNote.pitch = CURRENT_CHORD[0];
						firstNote.velocity = velocity;
						firstNote.send();
										
						//track when the strum started
						START_BEAT = CURRENT_BEAT;
						
						//check what division the start beat is on
						var deviation = Math.abs(START_BEAT - Math.round(START_BEAT));
						
						//if the deviation is not on beat
						if(deviation > 0 && deviation < 1/(DIVISION/2)) {
								//remove the deviation from the start beat 
								//(so that all following beats are not pushed back in time)
								START_BEAT = START_BEAT - deviation;
						}
				}
						
				var maxRandomVelocity = 0;		
								
				//calculate time ---------------------------------------------------
				//if calculating milliseconds
				if (TYPE == 0) {		
						var timeOffset;
				
						if (i === 0) {
								timeOffset = 0;
						} else if (i === 1) {
								timeOffset = divisionLength;
						} else {
								timeOffset = divisionLength + (curve * previousDivision);	
						}		
						
						previousDivision = timeOffset;
												
						//generate a random time offset						
						var maxRandom = Math.random() * STRUM_RANDOM_DIVISION;
						var minRandom = maxRandom - (maxRandom * 2);
						var totalOffset = timeOffset + randomInRange(minRandom,maxRandom);

						//convert the total offset from ms to beat and add to current beat
						var beatToPlay = START_BEAT + convertToBeat(totalOffset);
						
						//prevent timing to happen before the previous string's time
						while (beatToPlay < TIMING_ARRAY[i - 1]) {
								//generate a random time offset						
								var maxRandom = Math.random() * STRUM_RANDOM_DIVISION;
								var minRandom = maxRandom - (maxRandom * 2)
								var totalOffset = timeOffset 
																		+ randomInRange(minRandom,maxRandom);
								
								beatToPlay = START_BEAT + convertToBeat(totalOffset);
						}
								
						//add the beat to the timing array
						TIMING_ARRAY.push(beatToPlay);					
						
						//scale the velocity so that velocity either increases or decreases 
						//as the strum advances
						velocity = velocity + (i * STRUM_VELOCITY_CURVE);	
						maxRandomVelocity = STRUM_RANDOM_VELOCITY;
				}
				//if calculating beats
				else if (TYPE === 1) {						
						var divisionLength = 1 / getDivisionTime(ARP_DIVISION);
						var maxRandom = Math.random() * ARP_RANDOM_DIVISION;
						var minRandom = maxRandom - (maxRandom * 2)
						var randomPercentage = randomInRange(minRandom,maxRandom);
						var offset = divisionLength / 100 * randomPercentage;		
						var timeToSchedule = START_BEAT 
																		+ ((1 / (DIVISION / 2)) * i) + offset;
						
						while (timeToSchedule < TIMING_ARRAY[i - 1] 
						|| timeToSchedule < START_BEAT) {
								var maxRandom = Math.random() * ARP_RANDOM_DIVISION;
								var minRandom = maxRandom - (maxRandom * 2);
								var randomPercentage = randomInRange(minRandom,maxRandom);
								var offset = divisionLength / 100 * randomPercentage;
								
								timeToSchedule = START_BEAT 
																		+ ((1 / (DIVISION / 2)) * i) + offset;
						}
						
						//add the time to the TIMING_ARRAY
						TIMING_ARRAY.push(timeToSchedule);
						
						//scale the velocity so that velocity either increases or decreases 
						//as the strum advances
						velocity = velocity + (i * ARP_VELOCITY_CURVE);
						maxRandomVelocity = ARP_RANDOM_VELOCITY;
				}		
				else {
						if (DEBUG) {
								Trace("Error in calculating beats");
						}
				}	
				
				//calculate velocity -----------------------------------------------
				//add randomness to velocity				
				var minRandomVelocity = maxRandomVelocity - (maxRandomVelocity * 2);
				
				velocity += randomInRange(minRandomVelocity, maxRandomVelocity);
																
				//keep velocity in range
				if(velocity > 127) {
						velocity = 127;
				}

				if(velocity < 1) {
						velocity = 1;
				}
								
				//add velocity to array
				VELOCITY_ARRAY.push(MIDI.normalizeData(velocity));
		}
}

//______________________________ cancelStrum () ________________________________
/*
		This function cancels the previous strum, by stopping all of the notes in
		the currently playing chord. Only stop a chord, if all octaves of that chord
		are released. Remove this note from ACTIVE_NOTES

		@param event is the incoming note off event, whose pitch to check against	
*/		
function cancelStrum (event) {
		if (DEBUG) {
				Trace("Cancel Strum");
		}
						
		//remove the note from ACTIVE_NOTES
		var noteIndex = ACTIVE_NOTES.indexOf(event.pitch);
		ACTIVE_NOTES.splice(noteIndex, 1);

		var octaveIsDown = false; //same note is being triggered in another octave
		for (i = 0; i < ACTIVE_NOTES.length; i++) {
				if (event.pitch % 12 === ACTIVE_NOTES[i] % 12) {
						octaveIsDown = true;
						break;
				}
		}
						
		if (! octaveIsDown) {
				stopChord(getChord(event.pitch));
		}
						
		//if there are no active notes, reset the chord, timing, and velocity arrays
		//set STRUM_STOPPED to true
		if (ACTIVE_NOTES.length === 0) {
				STRUM_STOPPED = true;
				CURRENT_CHORD = [];
				TIMING_ARRAY = [];
				VELOCITY_ARRAY = [];
		}
}

//_________________________________ Chord () ___________________________________
/**
		This is a Chord object, which contains the name and notes of a chord. The 
		object must be instantiated with a name and notes. 

		var chord = new Chord(<string>, <array>);

		@param name is a string name of the chord
		@param notes is an array of pitches that make up the chord
 */
function Chord (name, notes) {
		this.name = name;
		this.notes = notes;
}

//_______________________________ convertToBeat() ______________________________
/*
		This function converts a time in milliseconds to a beat length.

		@param ms is the millisecond value to convert
		@return the converted beat value
*/	
function convertToBeat (ms) {
		var quarterNote = 60000 / TEMPO;	
		var convertedBeat = ms / quarterNote;
		return convertedBeat;
}

//_________________________________ getChord() _________________________________
/*
		This function returns a copy of the chord that is assigned to the incoming 
		note.

		@param note is the note that was triggered
		@return a copy of the associated chord to play
*/	
function getChord (note) {
		//get the note that is triggered, regardless of octave
		var baseNote = note % 12;
		var chordToPlay;
				
		switch (baseNote) {
				case 0:
						chordToPlay = ARRAY_OF_CHORDS[C_CHORD].notes.slice(0);
						break;
				case 1:
						chordToPlay = ARRAY_OF_CHORDS[C_SHARP_CHORD].notes.slice(0);
						break;
				case 2:
						chordToPlay = ARRAY_OF_CHORDS[D_CHORD].notes.slice(0);
						break;					
				case 3:
						chordToPlay = ARRAY_OF_CHORDS[D_SHARP_CHORD].notes.slice(0);
						break;
				case 4:
						chordToPlay = ARRAY_OF_CHORDS[E_CHORD].notes.slice(0);
						break;
				case 5:
						chordToPlay = ARRAY_OF_CHORDS[F_CHORD].notes.slice(0);
						break;				
				case 6:
						chordToPlay = ARRAY_OF_CHORDS[F_SHARP_CHORD].notes.slice(0);
						break;
				case 7:
						chordToPlay = ARRAY_OF_CHORDS[G_CHORD].notes.slice(0);
						break;				
				case 8:
						chordToPlay = ARRAY_OF_CHORDS[G_SHARP_CHORD].notes.slice(0);
						break;
				case 9:
						chordToPlay = ARRAY_OF_CHORDS[A_CHORD].notes.slice(0);
						break;				
				case 10:
						chordToPlay = ARRAY_OF_CHORDS[A_SHARP_CHORD].notes.slice(0);
						break;
				case 11:
						chordToPlay = ARRAY_OF_CHORDS[B_CHORD].notes.slice(0);
						break;
				default:
						chordToPlay = ARRAY_OF_CHORDS[0].notes.slice(0);
						if (DEBUG) {
								Trace("error in chord select");
						}
		}
		
		return chordToPlay;
}

//_____________________________ getDivisionTime() ______________________________
/*
		This function returns the division for the corresponding menu index

		@param index is the index of the division menu item
		@return the beat division as a division of a quarter note
*/	
function getDivisionTime (index) {
		//value to return
    var convertedValue;
	
		switch (index) {
				case 0:
						convertedValue = 6; //1/16T
						break;
				case 1:
						convertedValue = 4;  //1/16
						break;
				case 2: 
						convertedValue = 3; //1/8T
						break;
				case 3: 
						convertedValue = 2;  //1/8
						break;
				case 4: 
						convertedValue = 1.5; //1/4T
						break;
				case 5:
						convertedValue = 1; //1/4
						break;
				default:
						if (DEBUG) {
		            Trace("error in getDivisionTime()");
    					}
    }
    
    return convertedValue;
}

//_________________________________ getStrum() _________________________________
/*
		This function returns an int representing an up or down strum
		0 = down
		1 = up

		@param pitch is the triggering note, and is used for reseting the down
		       strum when TYPE = Alternate: Down On New Note
		@return 0 or 1 as the direction of the strum
*/	
function getStrum (pitch) {
		var strumToPlay;
		var direction = 0;
		
		if(TYPE === 0) {
				direction = STRUM_DIRECTION;
		} else {
				direction = ARP_DIRECTION;
		}
								
		switch (direction) {
				//down
				case 0:
						strumToPlay = 0;
						break;
				//up						
				case 1:
						strumToPlay = 1;
						break;		
				//alternate		
				case 2:
						if (PREVIOUS_STRUM === 0) {
								strumToPlay = 1;
						} else {
								strumToPlay = 0
						}	
						break;
				//down on new note						
				case 3:
						//if a new chord is triggered, reset to down
						if (LAST_NOTE_PLAYED != pitch % 12) {
								strumToPlay = 0;
						}
						//if the same chord is triggered, alternate
						else {
								if (PREVIOUS_STRUM === 0) {
										strumToPlay = 1;
								} else {
										strumToPlay = 0
								}
						}
						break;		
				//follow beats	1/8 and 1/16	
				case 4:
				case 5:
						if (IS_DOWN_BEAT) {
								strumToPlay = 0;
						} else {
								strumToPlay = 1;
						}
						break;
				default:
						if (DEBUG) {
								Trace("error in strum direction");
						}
			}
			
			//track the last strum direction
			PREVIOUS_STRUM = strumToPlay;

			return strumToPlay;
}

//______________________________ randomInRange() _______________________________
/*
		This function returns a random number in the given range

		@param min is the minimum range of the random number
		@param max is the maximum range of the random number
		@return a random numnber in the given range
*/	
function randomInRange (min, max) {
		return Math.random() * (max - min) + min;
}

//________________________________ stopChord() _________________________________
/*
		This function sends noteOffs for all notes in the given chord

		@param chord is the chord array containing the notes to stop
*/	
function stopChord (chord) {		
		//send noteOffs for all notes in the chord
		for (i = 0; i < chord.length; i++) {
				var noteOff = new NoteOff();
				noteOff.pitch = chord[i];
				noteOff.send();
		}
}

//_____________________________ triggerStrum () ________________________________
/*
		This function updates all required variables to trigger a new strum. 

		@param event is the incoming MIDI note on event
*/	
function triggerStrum (event) {

		stopChord(PREVIOUS_CHORD);			//prevent overlapping chords
		
		//quickly turn sustain off and on again, to prevent sustained notes from the
		//previous chord
		if (SUSTAIN_ENABLED) {
				var sustainPedal = new ControlChange;
				sustainPedal.number=64;
				sustainPedal.value=0;
				sustainPedal.send();		
								
				sustainPedal.value= 64;
				sustainPedal.sendAfterMilliseconds(5);
		}

		STRUM_STOPPED = false;		//set flag to false (strum IS playing)
				
		ACTIVE_NOTES.push(event.pitch);		//add this pitch to list of active notes

		var chordToPlay = getChord(event.pitch);		//get the chord for this pitch	
	
		//reset the chord, timing, and velocity arrays
		CURRENT_CHORD = [];
		TIMING_ARRAY = [];
		VELOCITY_ARRAY = [];
						
		//make a COPY of the chord and store it in a global variable
		CURRENT_CHORD = chordToPlay.slice(0);
						
		//caclulate the strum time, and velocity of the chord to play
		calculateStrum(chordToPlay, event.velocity, getStrum(event.pitch));
						
		//make a COPY of the current chord to track the last chord that was played
		PREVIOUS_CHORD = CURRENT_CHORD.slice(0);
						
		//track the last note that was played
		LAST_NOTE_PLAYED = event.pitch % 12;		
}


//Create UI --------------------------------------------------------------------
var PluginParameters = [{
		name:"------ Select ------",
		type:"text",
}, {
		name:"Type",
		type:"menu",
		valueStrings:["Strum","Arpeggio"],
		numberOfSteps:2,
		defaultValue:0
}, {
		name:"------ Strum Controls ------",
		type:"text"
}, {
		name:"Direction (Strum)",
		type:"menu",
		valueStrings:DIRECTION_TYPES,
		numberOfSteps:DIRECTION_TYPES.length - 1,
		defaultValue:4
}, {
		name:"Division (Strum)",
		type:"linear",
		minValue:1,
		maxValue:500,
		numberOfSteps:499,
		defaultValue:4,
		unit:"ms"
}, {
		name:"Division Curve (Strum)",
		type:"linear",
		minValue:-.5,
		maxValue:.5,
		defaultValue:-.1,
		numberOfSteps:100
}, {
		name:"Random Division (Strum)",
		type:"linear",
		minValue:0,
		maxValue:100,
		numberOfSteps:100,
		defaultValue:10,
		unit:"%"
}, {
		name:"Velocity Curve (Strum)",
		type:"linear",
		minValue:-10,
		maxValue:10,
		numberOfSteps:200,
		defaultValue:-4
}, {
		name:"Random Velocity (Strum)",
		type:"linear",
		minValue:0,
		maxValue:50,
		numberOfSteps:50,
		defaultValue:10,
		unit:"%"
}, {
		name:"------ Arpeggio Controls ------",
		type:"text"
}, {
		name:"Direction (Arpeggio)",
		type:"menu",
		valueStrings:DIRECTION_TYPES,
		numberOfSteps:DIRECTION_TYPES.length - 1,
		defaultValue:3
}, {
		name:"Division (Arpeggio)",
		type:"menu",
		valueStrings:TIME_DIVISIONS,
		defaultValue:3,
		numberOfSteps:6		
}, {
		name:"Random Division (Arpeggio)",
		type:"linear",
		minValue:0,
		maxValue:100,
		numberOfSteps:100,
		defaultValue:10,
		unit:"%"
}, {
		name:"Velocity Curve (Arpeggio)",
		type:"linear",
		minValue:-10,
		maxValue:10,
		numberOfSteps:200,
		defaultValue:-2
}, {
		name:"Random Velocity (Arpeggio)",
		type:"linear",
		minValue:0,
		maxValue:50,
		numberOfSteps:50,
		defaultValue:10,
		unit:"%"
}, {
		name:"------ Chord Assignments ------",
		type:"text"
}, {
		name:"Keyboard Split Point",
		type:"menu",
		valueStrings:MIDI._noteNames,
		numberOfSteps:MIDI._noteNames.length - 1,
		defaultValue:59
}, {
		name:"",
		type:"text"
}, {
		name:"C Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:0
}, {
		name:"C# Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:36
}, {
		name:"D Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:9
}, {
		name:"D# Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:1
}, {
		name:"E Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:17
}, {
		name:"F Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:20
}, {
		name:"F# Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:8
}, {
		name:"G Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:28
}, {
		name:"G# Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:16
}, {
		name:"A Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:37
}, {
		name:"A# Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:40
}, {
		name:"B Chord",
		type:"menu",
		valueStrings:CHORD_NAMES,
		numberOfSteps:CHORD_NAMES.length - 1,
		defaultValue:46
}]; 


//-----------------------------------------------------------------------------
// Harmonizer
//-----------------------------------------------------------------------------

// number of harmonies, also drives parameter creation. change, then run script
var numVoices = 2;

// global array of active notes for record keeping
var activeNotes = [];

//-----------------------------------------------------------------------------
function HandleMIDI(event) {
	if(event instanceof NoteOn) {
		
		// store a copy for record keeping and send it
		var originalNote = new NoteOn(event);
		var record = {originalPitch:event.pitch, events:[originalNote]};
		event.send();
		
		// now harmonize
		for (var i=1; i<numVoices + 1; i++) {
			
			// create a copy of the note on and apply parameters
			var harmony = new NoteOn(event);
			harmony.pitch += GetParameter("Transposition " + i);
			harmony.velocity = GetParameter("Velocity " + i);
			
			// store it alongside the original note and send it
			record.events.push(harmony);
			harmony.send();
		}
		
	// put the record of all harmonies in activeNotes array
	activeNotes.push(record);
	}
	
	else if(event instanceof NoteOff) {
		
		// find a match for the note off in our activeNotes record
		for (var i in activeNotes){
        	if(activeNotes[i].originalPitch == event.pitch) {
	
				// send note offs for each note stored in the record
				for (var j=0; j<activeNotes[i].events.length; j++) {
					var noteOff = new NoteOff(activeNotes[i].events[j]);
					noteOff.send();
				}
				
				// remove the record from activeNotes
				activeNotes.splice(i,1);
				break;
			}
		}
	}
	
	// pass non-note events through
	else {
      event.send();
    }
}

//-----------------------------------------------------------------------------
// when a parameter changes, kill active note and send the new one
function ParameterChanged(param, value) {
	
	// which voice is it?
	var voiceIndex = (param % 2 == 0) ? param / 2 : (param - 1) / 2;

	for(var i in activeNotes){
		var voiceToChange = activeNotes[i].events[voiceIndex+1];

		// send note off
		var noteOff = new NoteOff(voiceToChange);
		noteOff.send();

		// modify according to param change
		if (param % 2 == 0)
			voiceToChange.pitch = activeNotes[i].originalPitch + value; 
		else
			voiceToChange.velocity = value;
			
		// send
		voiceToChange.send();
	}
}

//-----------------------------------------------------------------------------
// Parameter Definitions
var pitches = [5, -12, 3, 7, 12, 19, 24];

var PluginParameters = [];
for (var i=0; i<numVoices; i++) {
	var voiceNumber = i + 1;
	PluginParameters.push({	name:"Transposition " + voiceNumber,
						  type:'lin', minValue:-24, maxValue:24, numberOfSteps:48, 
							defaultValue:pitches[i % pitches.length]
						  });
			
	PluginParameters.push({	name:"Velocity " + voiceNumber, 
							type:'lin',
							minValue:1, maxValue:127, numberOfSteps:126,
							defaultValue:100
						  });
}


//--------------------------------------------------------------------------------------------------
// Harpeggiator
//--------------------------------------------------------------------------------------------------

/* notes are generated in ProcessMIDI, and HandleMIDI only updates the note array (which is 
   activeNotes[]) and triggers pointer/cursor initialization. ParameterChanged is called when a 
   slider is moved. If it is the octave slider octave and maximumNotesToSend are updated. If it is 
   the note division slider, stdFlam is updated.
*/

activeNotes = [];

// index for the to-be-sent note.
var currPtr = 0; 

// octave shifting value for the to-be-sent note.
var currOct = 0; 

// time delay value for the to-be-sent note.
var noteSendDelay = 0; 

/* a counter recording how many notes are sent. This is used to compute the noteSendDelay 
   when an accelerando is involved, and control the moving direction of currPtr.*/
var totalNoteSent = 0; 

/* either 1 or -1, where 1 means the currPtr is moving right, 
 and -1 means it is moving left.*/
var direction = 0; 

// standard flam value computed in initializeCursor().
var stdFlam = 0; 

// the octave value directly read from the octave slider on the panel.
var octave = 0; 

// the start time of sending the first note, set in initializeCursor().
var timerStartTime = 0; 

//tracks the current channel pressure value 
var currentPressure = 0;

/* the total number of notes that should be sent before changing the pointer moving direction.*/
var maximumNotesToSend = 0; 

// reset before each note is sent
randomTimeShift = 0; 

// to make it sound more like real person
maxRandomTimeShift = 10; 

//set this flag to true, to access the host timing info
NeedsTimingInfo = true;

//**************************************************************************************************
function ProcessMIDI(){

  var info = GetTimingInfo();
    
  // if it is time to send out the note
  if(activeNotes.length != 0 && 
    (new Date().getTime()) - timerStartTime > noteSendDelay + randomTimeShift){ 
      
    randomTimeShift = (Math.random() - 0.5) * 2 * maxRandomTimeShift;
      
    if(currPtr < activeNotes.length){
        
      //generate and send out the note -------------------------------------------------------------
      noteToSend = new NoteOn(activeNotes[currPtr]);
      noteToSend.pitch += 12 * currOct;
      if(noteToSend.pitch <= 127 && noteToSend.pitch >= 0){
          
        // the value 16 in the line below is a scalor and is arbiturary
        velocitySubtractor = Math.round(GetParameter("Diminuendo") * totalNoteSent / 16);
          
        //if "Velocity Follows Aftertouch" is set to on
        if(GetParameter("Velocity Follows Aftertouch") == 0)
        {
             //use pressure to define velocity
             noteToSend.velocity = MIDI.normalizeData(currentPressure - velocitySubtractor);
        }
        else
        {
              //use played velocity
              noteToSend.velocity = MIDI.normalizeData(noteToSend.velocity - velocitySubtractor); 
        }


        noteToSend.send();
        noteOffToSend = new NoteOff(noteToSend);
        noteOffToSend.sendAfterMilliseconds(GetParameter("Note Length"));
      }
        
      //update controller variables ----------------------------------------------------------------
      notesSentInCurrOct = (octave>=0? currPtr : activeNotes.length - currPtr -1);
      signOfOctave = (octave == 0 ? 1 : Math.sign(octave));
      totalNoteSent = currOct * signOfOctave * activeNotes.length + notesSentInCurrOct;
        
      noteSendDelaySubtractor = totalNoteSent * GetParameter("Accelerando");
      noteSendDelay += stdFlam - noteSendDelaySubtractor;
      currPtr = currPtr + direction;
      if(currPtr >= activeNotes.length || currPtr < 0){
        currPtr = direction >= 0 ? 0 : activeNotes.length-1;
        currOct +=  direction;	
      }
        
    }else{
        
      // currPtr is out of bound due to whatever reason.
      	currPtr = activeNotes.length -1;
    }
      
    // test and change direction -------------------------------------------------------------------
    if(totalNoteSent >= maximumNotesToSend -1 || totalNoteSent <= 1){
      octaveMultiplier = octave == 0 ? 1 : octave;
      if(octaveMultiplier * direction >0 
         && totalNoteSent >= maximumNotesToSend -1
         || octaveMultiplier * direction <0 
         && totalNoteSent <= 1)
      {
        direction *= -1;
      }
    }//end test and change direction
  }//end time to send out note
}//ProcessMIDI

//**************************************************************************************************
function HandleMIDI(note){

  var info = GetTimingInfo();
  /* if a note on is received, add the note in activeNotes[] and re-initialized the cursor, and 
      update the maximumNotesToSend --------------------------------------------------------------*/
  if(note instanceof NoteOn){ 
    activeNotes.push(note);
    activeNotes.sort(sortByPitchAscending);
    octave = GetParameter("Octave");
    if(activeNotes.length == 1){
      initializeCursor(info);
    }
    maximumNotesToSend = (Math.abs(octave) + 1) * activeNotes.length; 

    //set current pressure to current played velocity
    currentPressure = note.data2;
  }
  
  /* get ChannelPressure. If it is less than the min value set in the "Minimum Aftertouch", set it 
     to the minimum value. -----------------------------------------------------------------------*/
  if(note instanceof ChannelPressure){

    currentPressure = note.value;

    var minAfterTouch = GetParameter("Minimum Aftertouch");
    if(currentPressure < minAfterTouch){
      currentPressure = minAfterTouch;
    }
  }
  
  /* note off message removes the off-ed note from activeNotes, and clears all the controller 
     variables if all the notes are off-ed -------------------------------------------------------*/
  if(note instanceof NoteOff){ 
    for(var i in activeNotes){
      if (activeNotes[i].pitch == note.pitch) {
        activeNotes.splice(i, 1);
        maximumNotesToSend = (Math.abs(octave) + 1) * activeNotes.length; 
      }
    }
    if(activeNotes.length == 0){ 
    		note.send();
      noteSendDelay = 0;
      stdFlam = 0;
      currPtr = 0;
      currOct = 0;
      totalNoteSent = 0;
      totalNoteSent = 0;
      octave = 0;
      timerStartTime = 0;
      direction = 0;
    }
  }
  
  //------------------------------------------------------------------------------------------------
  if(!((note instanceof Note) || (note instanceof ChannelPressure))){
    note.send();
  }
  
}

//**************************************************************************************************
function ParameterChanged(param, value){

  //if beat division slider is moved ---------------------------------------------------------------
  if(param == 1){
      var info = GetTimingInfo();
      stdFlam = 60000/info.tempo/GetParameter("Beat Division");
  }

  //if Octave slider is moved ----------------------------------------------------------------------
  if(param == 2){
    octave = GetParameter("Octave");
    maximumNotesToSend = (Math.abs(octave) + 1) * activeNotes.length;     
  }
}

//**************************************************************************************************
// initialization of new elements in the controller variable arrays
initializeCursor = function(info){ 
  noteSendDelay = 0;
  stdFlam = 60000/info.tempo/GetParameter("Beat Division");
  currOct = 0;
  totalNoteSent = 0;
  currPtr = octave<0? activeNotes.length-1 : 0;
  timerStartTime = new Date().getTime();
  direction = octave==0? 1 : Math.sign(octave);
}

//**************************************************************************************************
Math.sign = function(num){
	if(num>0) return 1;
	if(num==0) return 0;
	if(num<0) return -1;
}
 
function sortByPitchAscending(a,b) {
  if (a.pitch < b.pitch)
		return -1;
  if (a.pitch > b.pitch)
		return 1;
  return 0;
}

//**************************************************************************************************
//define the UI controls here

var PluginParameters = 
[{name:"Note Length", type:"lin", unit:"ms", 
minValue:0.0, maxValue:8000.0, numberOfSteps:800, defaultValue: 500.0},
 {name:"Beat Division", type:"lin",
minValue:1, maxValue: 32, numberOfSteps: 310,defaultValue: 8},
 {name:"Octave", type:"lin", 
minValue:-10, maxValue:10, numberOfSteps:20, defaultValue:2},
 {name:"Accelerando", type:"lin",
minValue:-100, maxValue:10, numberOfSteps:110, defaultValue:-5},
 {name:"Diminuendo", type:"lin", 
minValue:-127, maxValue:127, numberOfSteps:254, defaultValue:32},
 {name:"Velocity Follows Aftertouch", type:"menu", valueStrings:["On", "Off"],
 numberOfSteps:2, defaultValue:1},
 {name:"Minimum Aftertouch", type:"lin",
minValue:0, maxValue:127, numberOfSteps:127, defaultValue:40},
 ];


 //------------------------------------------------------------------------------------
// Invert Notes
//------------------------------------------------------------------------------------

// This script inverts MIDI notes based around the center pitch the user provides

var defaultCp = 127			// Center Pitch about which everything is inverted
var curCp = defaultCp;		// updated only when all notes are off
var fCp = defaultCp;		// future center pitch, updated every time user moves slider
var noteCount = 0;			// Track any outstanding note Off events

// take in the current midi event, invert it, send it, track the note count
function HandleMIDI(event) {
	// update center pitch when all notes are released
	if (noteCount == 0)
		curCp = getInvPoint(GetParameter('Center Pitch'));
		
	// invert note events
	if (event instanceof Note) {
		event.pitch = calcInversion(event.pitch);
		event.send();
	}
	
	// keep track of current played notes
	if(event instanceof NoteOn)
		noteCount++;
	else if(event instanceof NoteOff)
		noteCount--;	
		
	// pass through non-note events
	else
		event.send();
}

//Return a midi note number for the user center pitch selection
function getInvPoint(index) {
	if (index == 0)
		return 127;
	else
		return MIDI.noteNumber(invPoints[index]);
}

// calculate inverted pitch
function calcInversion(pitch) {
	var invPitch;
	if (curCp == 127)
		invPitch = 127 - pitch;		// fully invert
	else
		invPitch = (curCp - pitch) + curCp;	// invert about the center pitch	
	return invPitch;
}

//-----------------------------------------------------------------------------
var invPoints = ['None', 'C3', 'C#3', 'D3', 'D#3', 'E3', 'F3', 'F#3', 'G3', 'G#3', 'A3', 'A#3'];
var PluginParameters = [
			{name:"Center Pitch", 
 			type:"menu", 
  			valueStrings:invPoints,
  			numberOfSteps: 13, 
  			minValue:0,
 			maxValue:1,
			defaultValue:0,}
];


// limit pitch range of notes

var activeNotes = [];

function HandleMIDI(event)
{
	if (event instanceof NoteOn) {
		if (event.pitch > GetParameter('Maximum Pitch'))
			return undefined;  // don't send if too high
		if (event.pitch < GetParameter('Minimum Pitch'))
			return undefined;  // don't send if too low
		else {
			activeNotes.push(event);
			event.send()
		}
	}
	else if (event instanceof NoteOff) {
		for (i=0; i < activeNotes.length; i++) {
			if (event.pitch == activeNotes[i].pitch) {
				event.send();
				activeNotes.splice(i,1);
				break;
			}
		}
	}
	else { // pass non-note events through
		event.send();
	}
}

var PluginParameters = [
	{	name:'Maximum Pitch', type:'lin', 
		minValue:0, maxValue:127, numberOfSteps:127, defaultValue:115},
	{	name:'Minimum Pitch', type:'lin',
		minValue:0, maxValue:127, numberOfSteps:127, defaultValue:30}
];

//-----------------------------------------------------------------------------
// Logger
//-----------------------------------------------------------------------------

function HandleMIDI(e) {
  // pass through
  e.send();
  
  // Event Objects
	if (displayMode == 0) {
		Trace(e);
  }
  // MIDI Bytes
  else if (displayMode == 1) {
		Trace((e.status + e.channel-1) + 
          '\t' + e.data1 + 
          '\t' + e.data2);
	}
  // Hex
	else if (displayMode == 2) {
    var hex = function(num) { return num.toString(16) };
		Trace('0x' + hex((e.status + e.channel-1)) + 
          '  0x' + hex(e.data1) + 
          '\t0x' + hex(e.data2));
	}
}

//-----------------------------------------------------------------------------
var PluginParameters = 
[
  {name:"Display Mode", 
  type:"menu", valueStrings:["Event Objects", 
                             "MIDI Bytes", 
                             "Hex"],
   numberOfSteps: 3, defaultValue: 0}
];

var displayMode = 0;

function ParameterChanged(param, value) {
	displayMode = value;
}



//-----------------------------------------------------------------------------
// Mod Wheel Glissando
//-----------------------------------------------------------------------------

function HandleMIDI(e) {
	if (e instanceof ControlChange) {
		if (e.number == 1) {
		  var note = new NoteOn;
      if(e.value == 0)
        e.value = 1;
			note.pitch = e.value;
			note.velocity = GetParameter("Note Velocity");
	    note.send();
	    var off = new NoteOff(note);
			off.sendAfterMilliseconds(GetParameter("Note Length")+0.1);
		}
	}
	e.send();
	e.trace();
}

//-----------------------------------------------------------------------------
var PluginParameters =
[
	{name:"Note Velocity", type:"lin", 
   minValue:1, maxValue:127, numberOfSteps:126, defaultValue:80},
 
	{name:"Note Length", type:"lin", unit:"ms", 
   minValue:0.0, maxValue:500.0, numberOfSteps:100, defaultValue:100.0}
];


/* 
    Note Repeater: This script demonstrates how to achieve a similiar 
                   functionality as the MIDI Plug-in "Note Repeater". 
*/

NeedsTimingInfo = true;

//track if MIDI.allNotesOff() has been sent after stopping playback
var notesOffSent = false;

//track the number of notes that are currently being pressed
var numberOfNotes = 0;

//track the number of repeats to perform
var numberOfRepeats = 3;

//track the transposition to apply
var trasnposeAmount = 0;

function HandleMIDI(e) {

    var info = GetTimingInfo();
    
    if (e instanceof NoteOn) {
 
        numberOfNotes++ ;
    
        //only update these variables after the user releases all notes
        //and plays a new set of notes
        if(numberOfNotes == 1) {
            numberOfRepeats = GetParameter("Repeats");
            transposeAmount = GetParameter("Transpose");
        }
 
        //originally played note on
 	      e.send();
 	
        //delayed note ons
 	      for(var i=0; i< numberOfRepeats ;i++) {
 	
 	          e.pitch += transposeAmount;
 	        
 	          if(e.pitch > 127) {
 	              e.pitch = 127;
 	          }  
 	          
 	          e.velocity = e.velocity * (GetParameter("Velocity Multiplier")/100);

 	          if(e.velocity < 1) {
 	              e.velocity = 1;
 	          }
 	  
  	          if(e.velocity > 127) {
   	            e.velocity = 127;
 	          }
 	 	    
 	          e.sendAfterBeats((i+1) * getTime(GetParameter("Time")));
 	      }
    }
    else if(e instanceof NoteOff) {
        
        //originally played note off
        e.send();
        
        numberOfNotes--;
      
        //delated note offs
        for(var i=0; i< numberOfRepeats ;i++) {
      
            e.pitch += transposeAmount;
            
            if(e.pitch > 127) {
 	              e.pitch = 127;
 	          }  

  	          e.sendAfterBeats((i+1) * getTime(GetParameter("Time")));
   	    }
    }
    else {
        e.send() //pass all other MIDI events
    }
}


function ProcessMIDI() {

    var info = GetTimingInfo();
	 
    //if the transport stops, and allNotesOff() has not yet been sent
    if (!info.playing && !notesOffSent){
        MIDI.allNotesOff();
        notesOffSent = true;
    }  
  
    //reset the notesOffSent flag
    if(info.playing && notesOffSent) {
        notesOffSent = false;
    }
}

//get the division for the associated menu index
function getTime(index) {

    var convertedValue = 1;
	
    switch(index) {
        case 0:
            convertedValue = .166; //1/16T
            break;
        case 1:
            convertedValue = .25;  //1/16
            break;
        case 2:
            convertedValue = .375;  //1/16.
            break;
        case 3: 
            convertedValue = .333; //1/8T
            break;
        case 4: 
            convertedValue = .5;  //1/8
            break;
        case 5:
            convertedValue = .75; //1/8.
            break;
        case 6: 
            convertedValue = .666; //1/4T
            break;
        case 7:
            convertedValue = 1; //1/4
            break;
        case 8: 
            convertedValue = 1.5; //1/4.
            break;
        case 9:
            convertedValue = 1.333; //1/2T
            break;
        case 10: 
            convertedValue = 2; //1/2
            break;
        case 11:
            convertedValue = 3; //1/2.
            break;
        default:
            Trace("error in getTime()");
    }
    
    return convertedValue;
}

//define UI parameters 
var PluginParameters = [{name:"Time", type:"menu", 
                        valueStrings:["1/16 T", "1/16", "1/16 .", "1/8 T", "1/8", 
                        "1/8 .", "1/4 T", "1/4", "1/4 .", "1/2 T", "1/2", "1/2 ."],
                         defaultValue:5, numberOfSteps:11}, 
                        {name:"Repeats", defaultValue:3, minValue:1, maxValue:32, 
                         numberOfSteps:31, type:"linear"}, 
                        {name:"Transpose", defaultValue:0, minValue:-48, 
                         maxValue:48, numberOfSteps:96, type:"linear"},
                        {name:"Velocity Multiplier", defaultValue:66, 
                        minValue:1, maxValue:200, numberOfSteps:199, 
                        unit:"%", type:"linear" }];


//-----------------------------------------------------------------------------
// Note Stepper
//-----------------------------------------------------------------------------

var activeNotes = [];
var lastKey = 0;

var counters = []; // array of counters for each key
for (var i=0; i<127; i++) {
	counters.push(0);
}

// set all counters to step n
function resetCounters(n) {
	for (var i=0; i<counters.length; i++)
		counters[i] = n;
}

/* Step through notes and pass all other events through.  Held notes are 
held in activeNotes array to keep track of the corresponding note offs.*/
function HandleMIDI(event) {
	if (event instanceof NoteOn) {
		if (GetParameter('Global Reset Via Key') && 
		event.pitch == GetParameter('Global Reset Key')) {
			resetCounters(0);
		}
		if (GetParameter('Reset On New Note') && event.pitch != lastKey) {
			lastKey = event.pitch;
			resetCounters(1);
			storeAndSend(event);
		}
		else { // start steppin
				lastKey = event.pitch;
				event.pitchOffset = counters[lastKey] * GetParameter('Step Size');
				storeAndSend(event);
		    	counters[lastKey] += 1;
				if (counters[lastKey] >= GetParameter('Max Steps'))
		  			counters[lastKey] = 0;
	  	}
	}
	else if (event instanceof NoteOff) {
		clearAndSend(event);
	}
	else 
		event.send();
}

/* store the note in the activeNotes array along with it's pitchOffset
also apply the pitchOffset and send*/
function storeAndSend(event) {
	if (GetParameter('Global Reset Via Key') && 
			GetParameter('Send Reset Key') == 0 && 
			event.pitch == GetParameter('Global Reset Key'))
		return 0;
	activeNotes.push(event);
	var on = new NoteOn(event);
	if (event.pitchOffset)
		on.pitch += event.pitchOffset;
	on.send();
}

/* match the note off with a note on in activeNotes array and then re-apply the
pitchOffset and send*/
function clearAndSend(event) {
	for (i=0; i < activeNotes.length; i++) {
		if (event.pitch == activeNotes[i].pitch) {
			var foundNote = activeNotes[i];
			var off = new NoteOff(foundNote);
			if (foundNote.pitchOffset)
				off.pitch += foundNote.pitchOffset;
			off.send();
			activeNotes.splice(i, 1);
			break;
		}
	}
}

// parameter definitions
var PluginParameters = [
{name:"Step Size", type:"linear", minValue:-12, maxValue:12,
    defaultValue:3, numberOfSteps:24,},
{name:"Max Steps", type:"linear", minValue:2, maxValue:10,
    defaultValue:5, numberOfSteps:8},
{name:'Reset On New Note', type:'menu', valueStrings:['off','on'], 
    defaultValue:0, numberOfSteps:100},
{name:'Global Reset Via Key', type:'menu', valueStrings:['off','on'],
    defaultValue:0, numberOfSteps:100},
{name:'Global Reset Key', type:'menu', valueStrings:MIDI._noteNames,
    defaultValue:MIDI.noteNumber('C2'), numberOfSteps: 100},
{name:'Send Reset Key', type:'menu', valueStrings:['off', 'on'],
    defaultValue:0, numberOfSteps: 100}];



/*
		Note Triggered MIDI Event Gate.pst
		
		When the corresponding check-box is enabled, the incoming CC, PitchBend, or
		Channel Pressure values are always tracked, but only sent when a NoteOn
		message is received. When the check-box is disabled, the values are sent
		normally. Sustain Pedal (CC 64) may optionally be excluded from the gate
		behavior.
		
		One example would be to place this pst after a Modulator MIDI Plug-In that
		uses an LFO to send MIDI CC 1 (mod wheel) data. If, for instance, the 
		mod wheel was controlling filter cutoff on a synthesizer, you would normally
		hear the filter cut off rise and fall with the LFO shape if this pst was 
		disabled. When enabled, the filter cutoff would change only at the start
		of a NoteOn event, and would not change again until a new NoteOn event 
		was received. The result is discrete value changes for every new note.
*/

//global variables ----------
var CC_VALUES = {};						//dictionary to track all CC numbers and values
var PB_VALUE = 0;						//pitch bend value
var CP_VALUE = 0;						//channel pressure value
var CC_ENABLED = true;				//if MIDI CCs should be gated
var PB_ENABLED = false;				//if PitchBend should be gated
var CP_ENABLED = false;				//if ChannelPressure should be gated
var EXCLUDE_SUSTAIN = true; 	//if Sustain Pedal messages should be excluded from
														//the gate behavior

//------------------------------- HandleMIDI() ---------------------------------
/*
		This function is called once for every incoming MIDI event. 
		
		If the incoming message is a ControlChange, PitchBend, or ChannelPressure
		event, and the corresponding check-box is enabled in the UI, track the 
		value, otherwise, send the message.
		
		When a NoteOn event is received, send all MIDI messages that have been 
		tracked. 
		
		event = the incoming MIDI event
*/
function HandleMIDI(event) {
		//NoteOn ...................................................................
		if (event instanceof NoteOn) {
				if (CC_ENABLED) {
						for (cc in CC_VALUES) {
								var ccMsg = new ControlChange();
								ccMsg.number = cc;
								ccMsg.value = CC_VALUES[cc];
								ccMsg.send();
						}
						CC_VALUES = {};
				}
				
				if (PB_ENABLED) {
						var pb = new PitchBend();
						pb.value = PB_VALUE;
						pb.send();
				}
				
				if (CP_ENABLED) {
						var cp = new ChannelPressure();
						cp.value = CP_VALUE;
						cp.send();
				}
				
				event.send();	
		} 
		//NoteOff ..................................................................
		else if (event instanceof NoteOff) {
				event.send();
		}
		//Sustain Pedal ............................................................ 
		else if (event instanceof ControlChange && event.number === 64) {
				if (EXCLUDE_SUSTAIN) {
						event.send();
				} else {
						CC_VALUES[event.number] = event.value;
				}
		}
		//All Control Changes (except Sustain) .....................................
		else if (event instanceof ControlChange) {
				if (CC_ENABLED) {		
						CC_VALUES[event.number] = event.value;
				} else {
						event.send();
				}
		} 
		//PitchBend ................................................................
		else if (event instanceof PitchBend) {
				if (PB_ENABLED) {
						PB_VALUE = event.value;
				} else {
						event.send();
				}
		} 
		//ChannelPressure ..........................................................
		else if (event instanceof ChannelPressure) {
				if (CP_ENABLED) {
						CP_VALUE = event.value;
				} else {
						event.send();
				}
		} else {
				event.send();
		}
}

//----------------------------- ParameterChanged() -----------------------------
/*
		This function is called whenever value is changed in the Scripter UI. 
		
		Enabled/disable ControlChanges, PitchBend, ChannelPressure, and Sustain
		Pedal gate behavior.
		
		param = the index of the changed UI parameter 
		value = the new value for that parameter
*/
function ParameterChanged(param, value) {
		switch(param) {
				case 0:
						CC_ENABLED = value;
						break
				case 1:
						PB_ENABLED = value;
						break;
				case 2:
						CP_ENABLED = value;
						break;
				case 3:
						EXCLUDE_SUSTAIN = value;
						break;
				default:
						Trace("Unknown parameter index in ParameterChanged()");
		}
}

//create the UI ----------------------
PluginParameters = [{
		name:"Gate All Control Changes",
		type:"checkbox",
		defaultValue:1
}, {
		name:"Gate Pitch Bend",
		type:"checkbox",
		defaultValue:0
}, {
		name:"Gate Channel Pressure",
		type:"checkbox",
		defaultValue:0
}, {
		name:"Exclude Sustain Pedal",
		type:"checkbox",
		defaultValue:1
}];	



/*
		Octave Wrapper.pst
		
		This PST allows you to set a range of active notes. If notes fall outside of
		this range, they will be transposed up or down until they are within the 
		range. If a transposed note falls outside of the defined range, it will be
		filtered out (not sent). Any actives notes that fall outside of this range
		will be canceled (noteOff will be sent). 
		
		The high range can never be less than the low range, and vice versa. 
*/

var ACTIVE_NOTES = {};
var HIGH_RANGE;
var LOW_RANGE;

function HandleMIDI (event) {	
		if (event instanceof NoteOn) {
				var originalNote = event.pitch;
				var newNote = originalNote;
			
				if (newNote > HIGH_RANGE) {
						while (newNote > HIGH_RANGE) {
								newNote -= 12;
						}
				}
				
				if (newNote < LOW_RANGE) {
						while (newNote < LOW_RANGE) {
								newNote += 12;
						}
				}
								
				if(newNote > HIGH_RANGE || newNote < LOW_RANGE) {
						//don't send 		
				} else {
						ACTIVE_NOTES[originalNote] = newNote;
						event.pitch = newNote;
						event.send();
 				} 				
		} else if (event instanceof NoteOff) {
				var temp = event.pitch;
				event.pitch = ACTIVE_NOTES[event.pitch];
				delete ACTIVE_NOTES[temp];
				event.send();
		} else {
				event.send();		
		}
}

function ParameterChanged (param, value) {
		switch (param) {
				case 0:
						HIGH_RANGE = value;
						if (value < LOW_RANGE) {
								SetParameter(1, value);
						} 
						break;
				case 1:
						LOW_RANGE = value;
						if (value > HIGH_RANGE) {
								SetParameter(0, value);
						}
						break;
				default:
						//nothing
		}
		
		for (var key in ACTIVE_NOTES) {				
				if (ACTIVE_NOTES[key] > HIGH_RANGE || ACTIVE_NOTES[key] < LOW_RANGE) {
						var noteOff = new NoteOff();
						noteOff.pitch = ACTIVE_NOTES[key];
						noteOff.send();
						delete ACTIVE_NOTES[key];
				}
		}
}

var PluginParameters = [{
		name:"High Range",
		type:"linear",
		minValue:0,
		maxValue:127,
		numberOfSteps:127,
		defaultValue:71
}, {
		name:"Low Range",
		type:"linear",
		minValue:0,
		maxValue:127,
		numberOfSteps:127,
		defaultValue:60
}];



/*
		Probability Gate.pst
		
		This script uses probability to determine if an incoming NoteOn event 
		should be sent. The probability can be set via the "Probability" PluginParameter.
*/

var PROBABILITY = 50;

function HandleMIDI(event) {
    if(event instanceof NoteOn) {
        if(eventShouldSend()){
            event.send();
        } 
    } else {
		    event.send();
    }
}

function ParameterChanged(param, value) {
    if (param === 0) {
        PROBABILITY = value;
    }
}

function eventShouldSend() {
   return (Math.ceil(Math.random() * 100) <= PROBABILITY) ? true : false;
}

var PluginParameters = [{
    name:"Probability", 
    type:"linear",
    minValue:0,
    maxValue:100, 
    numberOfSteps:100, 
    defaultValue:50,
    unit:"%"
}];



/*
		Random Offset Probability.pst
		
		This PST transposes an incoming note up or down by random octaves and then
		applies a fixed semitone offset. The chance of an offset being applied,
		is determined by the Offset Probability slider. The range of the random
		octaves are defined with the Octave Offset Range: High and Low sliders,
		which are relative to the incoming note. 
		
		For example: if the incoming note is 60, and the high range is 2 and the low
		range is -1, a random octave range between 48 and 84 will be selected.

		After the note is offset by the random octave, a semitone offset is applied.
		For example: if the Semitone Offset is 3, the possible offset values would
		be 51 (-12 + 3), 63 (0 + 3), 75 (12 + 3), or 87 (24 + 3).
		
		Any notes that are transposed out of the normal MIDI note range (0-127) will
		be transposed up or down, until it is within the normal MIDI note range. 
*/

var NOTE_TRACKING = {};
var PROBABILITY = 50;
var HIGH_RANGE = 2;
var LOW_RANGE = -1;
var SEMINTONE_OFFSET = 0;
 
function HandleMIDI (event) {
		if(event instanceof NoteOn) {
				//check if the offset should be applied, based on the probability
				if(applyOffset(PROBABILITY)) { 

						var originalNote = event.pitch;

						//select random octave offset in the defined range
	 					var randomInRange = 
	 						Math.round(Math.random() * (HIGH_RANGE - LOW_RANGE) + LOW_RANGE);
						
						//apply octave and semitone offsets
 						event.pitch += (randomInRange * 12) + SEMINTONE_OFFSET;
						
						//if the pitch is out of range, tranpose it into range
						while(event.pitch > 127) {
								event.pitch -= 12;
						}
						while(event.pitch < 0) {
								event.pitch += 12;
						}
		  
		  			//keep track of the original and offset note pairs
		  			NOTE_TRACKING[originalNote] = event.pitch;

						event.send();

				} else {
						//send original note without offset
						event.send();   
				}
			
		} else if (event instanceof NoteOff) {
				//if the pitch was paired to an offset pitch, get the offset pitch
				if(event.pitch in NOTE_TRACKING) {				
						var temp =	event.pitch;
						event.pitch = NOTE_TRACKING[event.pitch];							
						delete NOTE_TRACKING[temp];
				}
		
				event.send();
		
		} else {
				//send all other MIDI events
				event.send();
		}
}

function ParameterChanged (param, value) {
		switch (param) {
				case 0:
						PROBABILITY = value;
						break;
				case 1: 
						HIGH_RANGE = value;
						break;
				case 2: 
						LOW_RANGE = value;
						break;
				case 3:
						SEMINTONE_OFFSET = value;
						break;
				default:
						Trace ("ParameterChanged(): error: invalid parameter index");
		}
}

//use the probability value to determine if an offset should be applied
function applyOffset (probability) {   
		return  (Math.ceil(Math.random()*100) <= probability) ? true : false;
}

//initialize PluginParameters
var PluginParameters = [{
		name:"Offset Probability",
		minValue:0, 
		maxValue:100, 
		numberOfSteps:100, 
		defaultValue:50, 
		type:"linear",
		unit:"%"
}, {
		name:"Octave Offset Range: High",
		minValue:0, 
		maxValue:5, 
		numberOfSteps:5, 
		defaultValue:2, 
		type:"linear",
		unit:""
}, {
		name:"Octave Offset Range: Low",
		minValue:-5, 
		maxValue:0, 
		numberOfSteps:5, 
		defaultValue:-1, 
		type:"linear",
		unit:""
}, {
		name:"Semitone Offset",
		type:"linear",
		minValue:-11,
		maxValue:11,
		numberOfSteps:22,
		defaultValue:0,
		unit:"semi"
}];


//************************************************
// Send Delayed Notes
//************************************************

var delayTime, noteLength;

function HandleMIDI(event) {
	if (event instanceof NoteOn) {
		event.sendAfterMilliseconds(delayTime);		
		var off = new NoteOff(event);
		off.sendAfterMilliseconds(delayTime + noteLength);
	}
	else if (event instanceof NoteOff) return;
	else event.send();
}

function ParameterChanged(param, value) {
	var timeInMilliseconds = value * 1000;
	if (param == 0) delayTime = timeInMilliseconds;
	if (param == 1) noteLength = timeInMilliseconds;
}

var PluginParameters = [
	{name:'Delay Time', type:'lin', unit:'sec',
	minValue:0, maxValue:120, defaultValue:1, numberOfSteps:120},	
	{name:'Note Length', type:'lin', unit:'sec',
	minValue:0.1, maxValue:10, defaultValue:1, numberOfSteps:99}
];



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*
		Sequencer.pst

		This PST creates a user customizable MIDI sequencer that can sequence either
		MIDI notes, or MIDI Control Change messages (or both!). For more information
		on how to customize the sequencer, scroll to the end of the Script, and read
		the comments that begin after the "Global Variables" box (line 4276)
*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


//******************************************************************************
//                                                                             *
//                                  Functions                                  *
//                                                                             *
//******************************************************************************

//^^^^^^^^^^^^^^^^^^^^^^^^^^^ Step Object & Functions ^^^^^^^^^^^^^^^^^^^^^^^^^^

//____________________________________ Step() __________________________________
/*
		Step is an object that represents a single step in a track. A track is made
		up of 1 or more step objects.

		The Step object can contain properties for both a 'note' step (triggers a
		MIDI Note event), or a 'cc' step (triggers a MIDI CC event). The Track
		object determines the mode of the track, and accesses the relevant variables
		from the Step object.

		value = the pitch or CC value to initialize with
*/
function Step (value) {
		//note variables ...........................................................
    this.pitch = MIDI.normalizeData(value); //base pitch
    this.octave = 3;												//octave range
    this.velocity = 96;    									//velocity (if the track option:
    																				//velocityByStep = false,
    																				//this value will be ignored, and a
    																				//track level velocity will be used
    																				//instead.)
    this.gateLength = 100;                  //length of the current step, as a
    																				//percentage of the current division
    																				//(if the track option
    																				//gateLengthByStep = false, this
    																				//value will be ignored, and a track
    																				//level length will be used instead.
		this.isActive = true;										//if the step is active or not
    //optional note variables ..................................................
    this.probability = 100;									//chance of step being triggered
    																				//(only used if the track option:
    																				//usesProbability = true)
    this.retrigger = 1;											//number of times a step will be
    																				//retriggers per beat (only used if
    																				//the track option:
    																				//usesRetrigger = true)							
		this.retriggerTransposition = 0;				//amount of semitones to add to each
																						//successive retrigger
		this.retriggerEndVelocity = 96;         //if != velocity each retrigger will
    																				//ramp to this final value, in equal
    																				//increments (only used if track
    																				//option: usesRetrigger = true)
    this.articulationID = 0;								//the articulationID of the current
    																				//step (only used if track option:
    																				//usesArticulationID = true;
		//cc variables .............................................................
    this.value = MIDI.normalizeData(value); //cc value
    this.shouldGlide = false;               //if this cc step should glide
    this.glideCurve = 0 ; 								  //range -1 to 1. 0 is no curve,
    																				//< 0 is logarithmic, > 0 is
    																				//exponential (if the track option:
    																				//glideCurveByStep = false, this
    																				//value will be ignored and a track
    																				//level glide curve will be used
    																				//instead
		//optional cc variables ....................................................
    this.endGlide = 0;											//the ending value of a glide (only
    																				//used if track option: usesEndGlide
    																				// = true. Otherwise, the ending
    																				//value of a glide will be the
    																				//value of the next step in the
    																				//sequence
}

//--------------------------------- shouldPlay() -------------------------------
/*
		This function is called only when the track mode is "note" and the option:
		usesProbability = true

		If the steps probability is greater than a random number, the step should be
		triggered.

		return true or false, if a step should play
*/
Step.prototype.shouldPlay = function () {
		var shouldPlay = true;

		//if probability is less than 100%, calculate if the step should play
		if (this.probability < 100) {
				shouldPlay = (Math.ceil(Math.random() * 100) <= this.probability);
		}

		return shouldPlay;
}

//^^^^^^^^^^^^^^^^^^^^^^^^^^ Track Object & Functions ^^^^^^^^^^^^^^^^^^^^^^^^^^

//___________________________________ Track() __________________________________
/*
		Track is an object that contains information for how a track should be
		played back, and holds all of the Step objects that make up a track. The
		Track object also has many variables and methods for the calculation of
		playing back the sequences. The majority of the sequencing work is doing
		in Track functions. By having the Track object make these calculations, as
		opposed to the Sequencer object, each track can have completely independent
		controls: division, swing, direction etc.

		numSteps = max number of steps for the track
		mode = 'cc' or 'note'
		id = track number
		seq = the Sequencer object that holds this track
*/
function Track (numSteps, mode, id, seq) {
		//track variables ..........................................................
		this.steps = [];                       //array of all step objects
		for (var i = 0; i < numSteps; i++) {   //instantiate the max number of steps
				this.steps.push(new Step(0));			 //for this track
		}

		this.sequencer = seq;                  //pointer to the sequencer object
																				 //that holds this track
		this.mode = mode.toLowerCase();        //'note' or 'cc' mode
		this.currentLength = this.steps.length;//current Cycle Length
		this.isActive = true; 		          		 //current bypass state
		this.ccNumber = 1;                     //CC number to sequence
		this.currentOffset = 1; 							 //amount of offset (the value = step,
																				 //so a value of 1 means no offset)
		this.division = 4;										 //track Rate (division) 4 = 1/16th
		this.swing = 50;                       //swing amount
		this.trackID = id;                     //the index of this track in the
																				 //Sequencer objects array of tracks
		//0 forward, 1 backwards, 2 forwards <-> backward, 3 backwards <-> forwards
		//4 random, 5 switch, 6 envelope: forwards, 7 envelope: backwards
		this.direction = 0;
		this.retriggerType = 0;          		   //0 = On New Note
																				 //1 = On New Phrase
																				 //2 = Follow Global Position
		this.sustainType = 0;               	 //0 = While Notes Are Active
																				 //1 = Always
		this.switchProbability = 0;            //when direction = 5 (switch) this
																				 //track level variable controls the
																				 //probability of the direction
																				 //switching
		//note mode track variables ................................................
		this.gateLengthByStep = false;        //if true, set step length per step,
																				//if false, use the track level value
																				//trackGateLength
		this.trackGateLength = 100;           //track level step length value
		this.velocityByStep = false;          //if true, set step velocity per step
																				//if false, use the track level value
																				//trackVelocity
		this.trackVelocity = 96;              //track level velocity value
		this.usesRetrigger = false;						//if true all steps have get a control
																				//for setting the number of retriggers
																				//to perform, as well as a control
																				//to set the end value of a velocity
																				//ramp
		this.retriggerActive = true;          //track level retrigger bypass, to
																				//to enable/disable retriggers in
																				//real time
		this.usesProbability = false;				  //if true uses step probability slider
		this.usesArticulationID = false;      //if true, uses Articulation ID slider
		this.minArticulation = 0;							//min articulation ID range
		this.maxArticulation = 255;						//max articulation ID range
		this.pitchByTrack = false;						//if true, a pitch is set per track,
																				//if false, a pitch is set per step
		this.trackPitch = 0;									//track level pitch
		this.trackOctave = 3;									//track level octave

		//cc mode track variables ..................................................
		this.scaleLowAmount = 1; 						//min CC value to scale within
		this.scaleHighAmount = 127;         //max CC value to scale within
		this.curveArray = []; 							//array to store values of the
																			//current glide curve
		this.glideCurveByStep = false;      //if true, set cc glide curve per step
																			//if false, use the track level value
																			//trackGlideCurve
		this.trackGlideCurve = 1;						//track level glide curve value
		this.usesEndGlide = false;          //if true, show per step sliders for
																			//setting the ending value of a CC glide
																			//if false, no controls are provided and
																			//glides end on the value of the next
																			//step in the sequence
		//playback variables .......................................................
		this.currentStep = 0;               //the currently active step
		this.previousStep = 0;              //to store the previously active step
																			//and be able to know about what the
																			//previous step was doing
		this.isPlaying = false;             //true, if the user is enabling playback
																			//by holding down a note or if
																			//sustainType = always
		this.activeNotes = [];              //array to track which notes the user
																			//is holding down/playing
		this.isSustaining = false;					//tracks if the sustain pedal is
																			//currently pressed
		this.currentDirection = 0;          //used to track the current direction of
																			//playback, when in modes that allow for
																			//switching directions(switch,f<>b,b<>f)
																			//0 = forwards 1 = backwards
		this.hasFinished = false;           //used to track if the envelope playback
																			//modes have reached the end of their
																			//sequence, and have finished playing
		this.playbackMode = 0;							//sets how note sequences should be
																			//be played back, either:
																			//0 = as is or 1 = transposed which
																			//transposes the sequence at an interval
																			//relative to C3 (MIDI note 60)
		this.transpositionSliderIndex = 0;																	
		this.currentPitchOffset = 0;        //the amount of transposition being
																			//applied to the note sequence, when
																			//playbackMode = 1
		//UI variables .............................................................
		//these variables assist in calculating which parameters are being modified
		//when UI controls are touched, as well as pairing the correct functions to
		//the parameters. There are handled in the Sequencer function: createUI()
		this.numberOfControls = 0;         //total number of controls for this track
		this.numberOfGlobalControls = 0;   //total number of global(track) controls
		this.numberOfStepControls = 0;     //total number of step controls
		this.numberOfRandomControls = 0;   //total number of random controls
		this.numberOfEditingControls = 0;  //total number of editing controls
		this.usesLED = true;							 //if true, display LEDs in the UI for
																		 //visualizing current step position
		//variables for controlling random values ..................................
		this.enableRandom = true;					 //if random controls should be used
		this.randomAmount = 100;					 //amount of randomization to apply
		//if the related parameter(s) should be randomized
		//global
		this.enableRandomRate = 0;
		this.enableRandomDirection = 0;
		this.enableRandomStartStep = 0;
		this.enableRandomCycleLength = 0;
		this.enableRandomSwing = 0;
		//notes
		this.enableRandomPitch = 0;
		this.enableRandomOctave = 0;
		this.enableRandomArticulationID = 0;
		this.enableRandomGateLength = 0;
		this.enableRandomProbability = 0;
		this.enableRandomVelocity = 0;
		this.enableRandomRetrigger = 0;
		this.enableRandomRetrigTrans = 0;
		this.enableRandomRetriggerVelocity = 0;
		this.enableRandomGate = 0;
		//cc
		this.enableRandomValues = 0;
		this.enableRandomScaling = 0;
		this.enableRandomGlide = 0;
		this.enableRandomGlideCurve = 0;
		this.enableRandomGlideTo = 0;
		//editing tools variables ..................................................
		this.editingTools = false;				//if true, controls will be added that
																		//allow you to set all steps controls
																		//to the same values
		//variables for setting all steps
		//note ----
		this.editPitch = 0;
		this.editOctave = 3;
		this.editArtID = 0;
		this.editGateLength = 100;
		this.editProbability = 100;
		this.editVelocity = 96;
		this.editRetrigger = 1;
		this.editRetrigTrans = 0;
		this.editRetrigEndVel = 96;
		this.editGate = 0;
		//cc ----
		this.editValue = 0;
		this.editGlideTo = 0;
		this.editGlideCurve = 0;
		this.editGlide = 0;
		//variables to track the indexes of the various parameters, to be accessed
		//when randomizing values. these are initialized in the createUI Sequencer
		//function
		//global ------------
		this.rateIndex;
		this.directionIndex;
		this.switchProbabilityIndex;
		this.startStepIndex;
		this.cycleLengthIndex;
		this.swingIndex;
		//notes ---------------
		this.pitchIndexes = [];
		this.octaveIndexes = [];
		this.articulationIndexes = [];
		this.gateLengthIndexes = [];       //if is by track, just store single value
																	   //if by step, add all values
		this.probabilityIndexes = [];
		this.velocityIndexes = [];         //if is by track, just store single value
																	   //if by step, add all values
		this.retriggerIndexes = [];
		this.retrigTransIndexes = [];
		this.retriggerVelocityIndexes = [];
		this.gateIndexes = [];
		//cc-------------------
		this.scaleLowIndex;
		this.scaleHighIndex;
		this.valueIndexes = [];
		this.glideIndexes = [];
		this.glideCurveIndexes = [];       //if is by track, just store single value
																	   //if by step, add all values
		this.glideToIndexes = [];
}

//--------------------------- getRandomParameterValue() ------------------------
/*
		This function generates a random value for a parameter. The index argument
		is the index of the PluginParameter. The Plug-in Parameter is accessed,
		and the min and max ranges are found. A random number is calculated in that
		range, based on the Randomize Amount percentage slider for this track.

		index = the index of the parameter to create a random value for

		returns a random value within the min and max range of the parameter, based
						on the Randomize Amount value for the track.
*/
Track.prototype.getRandomParameterValue = function (index) {
		var plugParam = this.sequencer.paramArray[index];
		var currentValue = GetParameter(index);
		var randomValue;
		var min;
		var max;
		var lowLimit;
		var highLimit;

		if (plugParam.type.toLowerCase() === "menu") {
				min = 0;
				max = plugParam.valueStrings.length - 1;
		} else if (plugParam.type.toLowerCase() === "linear") {
				min = plugParam.minValue;
				max = plugParam.maxValue;
		} else if (plugParam.type.toLowerCase() === "checkbox") {
				min = 0;
				max = 1;
		}

		var fullRange = max - min;
		var lowerRange = Math.abs(currentValue - min);
		var upperRange = Math.abs(max - currentValue);

		if (lowerRange > upperRange) {
				var percentOfRange = lowerRange * (this.randomAmount / 100);
				lowLimit =  currentValue - percentOfRange;
				highLimit = currentValue + (currentValue - lowLimit);
				if (highLimit > max) {
						highLimit = max;
				}
		} else if (upperRange > lowerRange) {
				var percentOfRange = upperRange * (this.randomAmount / 100);
				highLimit = currentValue + percentOfRange;
				lowLimit = currentValue - (highLimit - currentValue);
				if (lowLimit < min) {
						lowLimit = min;
				}
		} else {
				var offset = (fullRange / 2) * (this.randomAmount / 100);
				lowLimit = currentValue - offset ;
				highLimit = currentValue + offset;
		}
		
		var rand;
		var num;

		//if the UI element is on/off get a random value based on probability, 
		//instead of by range
		if (plugParam.type.toLowerCase() === "checkbox") {
				if (Math.ceil(Math.random() * 100) <= this.randomAmount) {
						num = Math.round(Math.random());
				} else {
						num = currentValue;
				}
		} else {
				rand = Math.random() * 100;
				num = this.scaleValue(rand, 0, 100, lowLimit, highLimit);
		}

		return num;
}

//---------------------------------- randomize() -------------------------------
/*
		This function is called when the "RANDOMIZE!" button is pressed. It checks
		the available option check-boxes, and randomizes any elements that are 
		enabled to be randomized.
*/
Track.prototype.randomize = function () {
		//randomize globals ........................................................
		if (this.enableRandomRate === 1) {
				SetParameter(this.rateIndex,
										this.getRandomParameterValue(this.rateIndex));
		}

		if (this.enableRandomDirection === 1) {
				SetParameter(this.directionIndex,
										this.getRandomParameterValue(this.directionIndex));
				SetParameter(this.switchProbabilityIndex,
										this.getRandomParameterValue(this.switchProbabilityIndex));
		}

		if (this.enableRandomStartStep === 1) {
				SetParameter(this.startStepIndex,
										this.getRandomParameterValue(this.startStepIndex));
		}

		if (this.enableRandomCycleLength === 1){
				SetParameter(this.cycleLengthIndex,
										this.getRandomParameterValue(this.cycleLengthIndex));
		}

		if (this.enableRandomSwing === 1) {
				SetParameter(this.swingIndex,
										this.getRandomParameterValue(this.swingIndex));
		}

		//randomize notes controls .................................................
		if (this.mode === "note") {
				if (this.enableRandomPitch === 1) {
						for (index in this.pitchIndexes) {
								var paramIndex = this.pitchIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.enableRandomOctave === 1) {
						for (index in this.octaveIndexes) {
								var paramIndex = this.octaveIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.usesArticulationID && this.enableRandomArticulationID === 1) {
						for (index in this.articulationIndexes) {
								var paramIndex = this.articulationIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.enableRandomGateLength === 1) {
						for (index in this.gateLengthIndexes) {
								var paramIndex = this.gateLengthIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.usesProbability && this.enableRandomProbability === 1) {
						for (index in this.probabilityIndexes) {
								var paramIndex = this.probabilityIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.enableRandomVelocity === 1) {
						for (index in this.velocityIndexes) {
								var paramIndex = this.velocityIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.usesRetrigger) {
						if (this.enableRandomRetrigger === 1) {
								for (index in this.retriggerIndexes) {
										var paramIndex = this.retriggerIndexes[index];
										SetParameter(paramIndex,
																this.getRandomParameterValue(paramIndex));
								}
						}
						
						if (this.enableRandomRetrigTrans === 1) {
								for (index in this.retrigTransIndexes) {
										var paramIndex = this.retrigTransIndexes[index];
										SetParameter(paramIndex, 
																this.getRandomParameterValue(paramIndex));
								}
						}

						if (this.enableRandomRetriggerVelocity === 1) {
								for (index in this.retriggerVelocityIndexes) {
										var paramIndex = this.retriggerVelocityIndexes[index];
										SetParameter(paramIndex,
																this.getRandomParameterValue(paramIndex));
								}
						}
				}

				if (this.enableRandomGate === 1) {
						for (index in this.gateIndexes) {
								var paramIndex = this.gateIndexes[index];								
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}
		}
 		//randomize CC controls ....................................................
		else if (this.mode === "cc") {
				if (this.enableRandomScaling === 1) {
						SetParameter(this.scaleLowIndex,
												this.getRandomParameterValue(this.scaleLowIndex));
						SetParameter(this.scaleHighIndex,
												this.getRandomParameterValue(this.scaleHighIndex));
				}

				if (this.enableRandomValues === 1) {
						for (index in this.valueIndexes) {
								var paramIndex = this.valueIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.enableRandomGlide === 1) {
						for (index in this.glideIndexes) {
								var paramIndex = this.glideIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.enableRandomGlideCurve === 1) {
						for (index in this.glideCurveIndexes) {
								var paramIndex = this.glideCurveIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}

				if (this.usesEndGlide && this.enableRandomGlideTo === 1) {
						for (index in this.glideToIndexes) {
								var paramIndex = this.glideToIndexes[index];
								SetParameter(paramIndex,
														this.getRandomParameterValue(paramIndex));
						}
				}
		}
}

//---------------------------------- getCurve() --------------------------------
/*
		This function calculates the curve of a CC glide, in MIDI CC values.

		startValue = the starting value
		endValue = the ending value
		curve = the shape of the curve that should be applied to the values between
						startValue and endValue

		return an array of scaled values from startValue to endValue
*/
Track.prototype.getCurve = function (startValue, endValue, curve) {
		var tempArray = [];              //array to hold the calculated curve values
		var inverted = false;						 //if startValue > endValue

		if (startValue > endValue) {
				//swap start and end values
				var temp = startValue;
				startValue = endValue;
				endValue = temp;
				inverted = true;
		}

		//create initial values with specified curve (results will be out of the
		//standard MIDI CC range
		for (var i = startValue; i < endValue; i++) {
				tempArray.push(Math.pow(i, curve));
		}

		//calculate scaling amount
		var scaleAmount = endValue/tempArray[tempArray.length - 1];

		//scale values to be in range of standard MIDI CC range
		for (var i = 0; i < tempArray.length; i++) {
				tempArray[i] = MIDI.normalizeData(tempArray[i] * scaleAmount);
		}

		//if inverted, flip the inverted array so that the values decrease instead
		//of increase
		if (inverted) {
				tempArray.reverse();
		}

		//return the array of calculated MIDI CC values
		return tempArray;
}

//--------------------------------- scaleValue() -------------------------------
/*
		This function scales a value from one range, into another range.

		value = the value to scale
		oldMin = the min value of the old range
		oldMax = the max value of the old range
		newMin = the min value of the new range
		newMax = the max value of the new range

		return the scaled value
*/
Track.prototype.scaleValue = function (value, oldMin, oldMax, newMin, newMax) {
		return (value / ((oldMax - oldMin) / (newMax - newMin))) + newMin;
}

//-------------------------------- scaleCCValue() ------------------------------
/*
		This function calls scaleValue() to scale a value from a standard MIDI range
		of 0-127, into the range defined in a CC track's Scale Low and Scale High
		controls. If Scale Low > Scale High, the result is an inverted value,
		relative to the position in the range.

		value = the value to scale

		return the scaled value with normalized data
*/
Track.prototype.scaleCCValue = function (value) {
		var scaledValue = this.scaleValue(value,
																		0,
																		127,
																		this.scaleLowAmount,
																		this.scaleHighAmount);
		return MIDI.normalizeData(scaledValue);
}

//------------------------------ scaleGlideCurve() -----------------------------
/*
		This function interprets the glide curve range of -1 to 1 into the actual
		values for calculating the glide curve.

		Glide curve of 0 is actually 1
		Glide curve range of -1 to 0 is actually .1 to 1
		Glide curve range of 0 to 1 is actually 1 to 10

		value = the value to scale

		return the scaled value
*/
Track.prototype.scaleGlideCurve = function (value) {
		var result;

		//for negative values, offset to be in .1 to 1 range up to 3 decimals
		if (value < 0) {
				result = (1.1 - Math.abs(value)).toFixed(3);
		}
		//for positive values, scale the value to the 1 to 10 range up to 3 decimals
		else if (value >= 0) {
				result = this.scaleValue(value, 0, 1, 1, 10).toFixed(3);
		}

		return result;
}

//----------------------------- convertOctave() --------------------------------
/*
		This function converts an octave value (-2 through 8) into a base offset
		amount, which will be added to the base pitch value.

		octave = the octave number to convert

		return basePitch which is the converted offset
 */
Track.prototype.convertOctave = function (octave) {
		var basePitch;

		switch (octave) {
				case -2:
						basePitch = 0;
						break;
				case -1:
						basePitch = 12;
						break;
				case 0:
						basePitch = 24;
						break;
				case 1:
						basePitch = 36;
						break;
				case 2:
						basePitch = 48;
						break;
				case 3:
						basePitch = 60;
						break;
				case 4:
						basePitch = 72;
						break;
				case 5:
						basePitch = 84;
						break;
				case 6:
						basePitch = 96;
						break;
				case 7: 
						basePitch = 108;
						break;
				case 8:
						basePitch = 120;
						break;
				default:
						basePitch = 60
		}

		return basePitch;
}

//------------------------------- calculatePitch() -----------------------------
/*
		This function applies transposition to an incoming pitch, based on the
		currently played note on the keyboard, so that a sequence can be transposed
		relative to middle C (C3/60)

		pitch = the pitch to transpose

		return transposed pitch
*/
Track.prototype.calculatePitch = function (pitch) {
		var transposedPitch = pitch;

		if (this.playbackMode === 1) {
	 			//if notes above C3 are triggering the sequence
	 			if (this.currentPitchOffset > 60) {
	 					//transpose the note up
	 					transposedPitch += (this.currentPitchOffset - 60);
	 			}
	 			//if notes below C3 are triggering the sequence
	 			else if (this.currentPitchOffset < 60) {
	 					//transpose the note down
	 					transposedPitch -= (60 - this.currentPitchOffset);
	 			}
	 	} 
	 	//if follow slider
	 	else if (this.playbackMode === 2) {
	 			transposedPitch += this.currentPitchOffset;
	 	}

	 	return transposedPitch;
}

//--------------------------- convertToMSDuration() ----------------------------
/*
		This function converts a beat duration into a length in milliseconds. 

		beatLength = the length in beats (i.e. 1, 1.5, etc) to convert

		return the converted value as milliseconds
*/
Track.prototype.convertToMSDuration = function (beatLength) {
		return beatLength * (60000 / GetTimingInfo().tempo);
}

//------------------------- convertToBeatDuration() ----------------------------
/*
		This function converts a millisecond length into a beat duration

		ms = the millisecond value to convert

		return the converted value as a beat length (i.e. 1, 1.5, etc)
*/
Track.prototype.convertToBeatDuration = function (ms) {
		 return ms / (60000 / GetTimingInfo().tempo);
}
//------------------------------- pushTimingInfo() -----------------------------
/*
		This function handles the triggering of all events, based on the track's
		mode type (note or cc). The block is only executed if the track is not
		bypassed, and if in envelope mode, it has not reached the end of the
		sequence.

		Beats are scheduled to play, only if the beat falls within the current
		process buffer. This function is called once every process buffer. The
		function is called from the Sequencer function of the same name, which is
		called each ProcessMIDI() call.

		Any special actions, such as step retriggers, or CC glides, are calculated
		in this function as well.

		At the end of the function, resolveNextStep() is called, to determine
		which step should be played next.

		musicInfo = the host timing into object
		beatToSchedule = the beat that should be scheduled, based on the tracks rate
		division = the number of divisions per beat
		divLength = the length in ms of a division
*/
Track.prototype.pushTimingInfo = function (musicInfo, beatToSchedule, division,
																										divLength) {
	 	//if the track is bypassed, or envelope mode has reached it's end, do not
	 	//proceed
	 	if (this.hasFinished) {
  			return;
	 	}

		var lookAheadEnd = musicInfo.blockEndBeat;

		// when cycling, find the beats that wrap around the last buffer
		if (musicInfo.cycling && lookAheadEnd >= musicInfo.rightCycleBeat) {
				if (lookAheadEnd >= musicInfo.rightCycleBeat) {
						var cycleBeats = musicInfo.rightCycleBeat - musicInfo.leftCycleBeat;
				    var cycleEnd = lookAheadEnd - cycleBeats;
			  }
		}

		//calculate swing ..........................................................
		var swingOffset = 0;      //amount of offset to apply to the Note events
		var isSwinging = false;   //flag to check if the current step is swinging
		var divisionPercent = 0;
		var swingPercent = 0;

		//only calculate swing if it is active
		if (this.swing > 50) {
				divisionPercent = 1 / division;
				swingPercent = (this.swing * 2) / 100;
				//calculate swing offset
				swingOffset = (divisionPercent * swingPercent) - divisionPercent;

				//calculate if the current step is swinging
				//1/4 note
				if (division === 1 ) {
						if ((beatToSchedule / divisionPercent) % 2 === 1) {
								isSwinging = false;
						} else {
								isSwinging = true;
						}
				}
				//1/8, 1/16, 1/32
				else if (division === 2 || division === 4 || division === 8) {
						if ((beatToSchedule / divisionPercent) % 2 === 0) {
								isSwinging = false;
						} else {
								isSwinging = true;
						}
				}
				//1/12
				else if (division === 3) {
						var mod = Math.round(beatToSchedule / divisionPercent) % 6;
						if (mod === 1 || mod === 3 || mod === 5) {
								isSwinging = false
						} else {
								isSwinging = true;
						}
				}
				//1/24
				else if (division === 6) {
						var mod = Math.round(beatToSchedule / divisionPercent) % 6;
						if (mod === 1 || mod === 3 || mod === 5) {
								isSwinging = true
						} else {
								isSwinging = false;
						}
				}
		}

		// loop through the beats that fall within this buffer .....................
    while ((beatToSchedule >= musicInfo.blockStartBeat
    && beatToSchedule < lookAheadEnd)
    // including beats that wrap around the cycle point
    || (musicInfo.cycling && beatToSchedule < cycleEnd)) {
    			// adjust for cycle
				if (musicInfo.cycling && beatToSchedule >= musicInfo.rightCycleBeat) {
						beatToSchedule -= cycleBeats;
				}

				var stepIndex;

				//if retrigger is "Follow Global Position"	 and direction is Forwards
				//calculate beat based on timeline position
				if (this.retriggerType === 2 && this.direction === 0) {
						this.currentStep = ((beatToSchedule * division) - division) 
																												% this.currentLength;
						//added to compensate for trigger beat
						if (this.currentStep < 0) {
								this.currentStep = this.currentLength - this.currentStep;
						}

						//calculate step index
						stepIndex = (this.currentStep ) % this.currentLength;
						stepIndex = (stepIndex + this.currentOffset - 1)%this.steps.length;
				}
				//all other modes: use current step
				else {
						stepIndex = this.currentStep;
				}

				//get next step so you know where you are going
	 			var nextStepIndex = stepIndex + 1;

	 			//keep in range
	 			if (nextStepIndex >= this.steps.length) {
	 					nextStepIndex = 0;
	 			}

	 			//if LEDs are shown in the UI, update the current LED position
	 			if (this.usesLED && this.isActive ) {
	 					this.updateLED(stepIndex, this.trackID);
	 			}

	 			//store this step and next step
	 			var thisStep = this.steps[stepIndex];
	 			var nextStep = this.steps[nextStepIndex];
				var currentNote;

				//if pitchByTrack, do not apply transpose value to pitch
				if (this.pitchByTrack) {
						currentNote = this.convertOctave(this.trackOctave)+this.trackPitch;
				} 
				//pitch by step; apply transpose value to pitch
				else {
						var octave = this.convertOctave(thisStep.octave);
					 	currentNote = this.calculatePitch(octave + thisStep.pitch);
				}

	 			//check if the current note is valid MIDI note, if it is out of range
	 			//do not play
	 			var isValidNote = true;
	 			if (this.mode === "note") {
	 					if (currentNote > 127 || currentNote < 0) {
	 					  		isValidNote = false;
	 					}
	 			}

	 			//check probability of note playing, and if the note is in range, play
	 			//the step. (CC steps will always have probability and valid note by
	 			//default, and will always play)
	 			if (this.isActive 
	 			&& thisStep.shouldPlay() 
	 			&& isValidNote 
	 			&& thisStep.isActive) {
	 					//note mode ........................................................
	 					switch (this.mode) {
	 					case "note":
	 							var gateLength;
	 							//get length of step either from the step or from the global
	 							//step length control
	 							if(this.gateLengthByStep) {
	 									gateLength = thisStep.gateLength;
	 							} else {
	 									gateLength = this.trackGateLength;
	 							}
	 							gateLength /= 100;

	 							//get retrigger amount. if step should be retriggered, calculate
	 							//the retrigger timing .........................................
	 							var retrigNum = thisStep.retrigger;
	 							if (this.usesRetrigger
	 							&& this.retriggerActive
	 							&& retrigNum > 1) {
	 									//calculate the start beat
	 									var startBeat = beatToSchedule;
	 									var end = beatToSchedule + (1 / division);

	 									if (isSwinging) {
	 											startBeat += swingOffset;
	 									}

	 									var beatLength = end - startBeat;
	 									var endVelocity = thisStep.retriggerEndVelocity;
	 									var retrigLength = beatLength / retrigNum;
	 									var currentVelocity;

	 									if (this.velocityByStep) {
	 											currentVelocity = thisStep.velocity;
	 									} else {
	 											currentVelocity = this.trackVelocity;
	 									}

	 									//calculate the velocity ramp for retriggers
	 									var high;
	 									var low;
	 									var direction;  //0 = down 1 = up
	 									if (currentVelocity > endVelocity) {
	 											high = currentVelocity;
	 											low = endVelocity;
	 											direction = 0;
	 									} else {
	 											high = endVelocity;
	 											low = currentVelocity;
	 											direction = 1;
	 									}
	 									velocityIncrement = (high - low) / (retrigNum - 1);

										//schedule all of the retriggers ...........................
	 									for (var i = 0; i < retrigNum; i++) {
	 											//create note event and assign all needed values
	 											var note = new NoteOn();
	 											note.pitch = currentNote 
	 																		+ (thisStep.retriggerTransposition  * i);

	 											if (note.pitch > 127 || note.pitch < 0) {
	 													continue;
	 											}
	 											
	 											note.articulationID = thisStep.articulationID;
	 											note.velocity = currentVelocity;

	 											//apply velocity ramp depending on direction
	 											if (direction === 0) {
	 													note.velocity -= (velocityIncrement * i);
	 											} else {
	 													note.velocity += (velocityIncrement * i);
	 											}
	 											note.velocity = MIDI.normalizeData(note.velocity);

	 											//schedule the beat
	 											var retrigStart = startBeat + (i * retrigLength);

	 											//calculate when the noteOff should occur
	 											var endBeat = startBeat +  (((retrigLength * (i)))
	 											                  + (retrigLength * gateLength));

												if (i === retrigNum - 1 && !isSwinging) {
														endBeat += swingOffset * gateLength;
												}

												var lengthInMS =
															 this.convertToMSDuration(endBeat - retrigStart);
												if (lengthInMS < 4) {
														endBeat = retrigStart +
																								this.convertToBeatDuration(4);
												}

	 											//in case the end beat is outside of the cycling area,
	 											//do not send the note, to prevent hung notes
	 											if (musicInfo.cycling
	 											&& endBeat > musicInfo.rightCycleBeat) {
														continue;
	 											}

												//send NoteOn
	 											note.sendAtBeat(retrigStart);

	 											//create noteOff event and assign all needed values
					 							var noteOff = new NoteOff();
	 											noteOff.pitch = note.pitch;
	 											noteOff.articulationID = thisStep.articulationID;
			 									noteOff.sendAtBeat(endBeat);
	 									}
	 							}
	 							//non-retriggered notes ........................................
	 							else {
	 									//create the noteOn event and assign all needed values
	 									var note = new NoteOn();
	 									note.pitch = currentNote;
	 									note.articulationID = thisStep.articulationID;

	 									if (this.velocityByStep) {
	 											note.velocity = thisStep.velocity;
	 									} else {
	 											note.velocity = this.trackVelocity;
	 									}

	 									//calculate the start beat
	 									var startBeat = beatToSchedule;

	 									if (isSwinging) {
	 											startBeat += swingOffset;
	 									}

										//calculate end beat
										var endBeat = beatToSchedule + (1 / division);

										if (!isSwinging) {
												endBeat += swingOffset;
										}

										//calculate length of beat and scale it by gate length
										var leng = endBeat - startBeat;
										leng *= gateLength;
										//store actual ending beat
										endBeat = startBeat + leng;

										var lengthInMS =
															   this.convertToMSDuration(endBeat - startBeat);
										if (lengthInMS < 4) {
												endBeat = startBeat + this.convertToBeatDuration(4);
										}

										//in case the end beat is outside of the cycling area,
										//don't send the note, to avoid a hung note
	 									if (musicInfo.cycling
	 									&& endBeat > musicInfo.rightCycleBeat) {
	 											break;
	 									}

	 									//send noteOn
	 									note.sendAtBeat(startBeat);

										//create noteOff event and assign all needed values
			 							var noteOff = new NoteOff();
	 									noteOff.pitch = note.pitch;
	 									noteOff.articulationID = thisStep.articulationID;
	 									noteOff.sendAtBeat(endBeat);
	 							}

	 							break;
	 					//CC mode ..........................................................
			 			case "cc":
								var startBeat = beatToSchedule;

								if (isSwinging) {
	 									startBeat += swingOffset;
	 							}
	 							
	 							//aftertouch
	 							if (this.ccNumber === 128) {
	 									if(this.previousStep && !this.previousStep.shouldGlide) {
	 											var cp = new ChannelPressure();
	 											cp.value = this.scaleCCValue(this.previousStep.value);
	 											cp.sendAtBeat(startBeat - .005);
	 									}
	 									
	 									var cp = new ChannelPressure();
	 									cp.value = this.scaleCCValue(thisStep.value);
	 									cp.sendAtBeat(startBeat);
	 							}
	 							//pitchbend
	 							else if (this.ccNumber === 129) {
	 									if (this.previousStep && !this.previousStep.shouldGlide) {
	 											var pb = new PitchBend();
	 											pb.value = this.scaleCCValue(this.previousStep.value);
	 											pb.value = this.scaleValue(pb.value, 
	 																								0, 
	 																								127, 
	 																								-8192, 
	 																								8191);
	 											pb.sendAtBeat(startBeat - .005);
	 									}
	 									
	 									var pb = new PitchBend();
	 									pb.value = this.scaleCCValue(thisStep.value);
	 									pb.value = this.scaleValue(pb.value, 0, 127, -8192, 8191);
	 									pb.sendAtBeat(startBeat);
	 							} 
	 							//regular CC
	 							else {
	 									//to preserve stepped modulation when recording to MIDI 
	 									//track, resend the previous value right before the new 
	 									//value so that there is no interpolated line connecting 
	 									//the 2 values
			 							if (this.previousStep && !this.previousStep.shouldGlide) {
			 									var cc = new ControlChange();
			 									cc.number = this.ccNumber;
			 									cc.value = this.scaleCCValue(this.previousStep.value);
			 									cc.sendAtBeat(startBeat - .005);
			 							}

	 									//create CC event and assign all values
	 									var cc = new ControlChange();
	 									cc.number = this.ccNumber;
			 							//scale the current value to the range that is defined by
	 									//scale low and scale high controls
	 									cc.value = this.scaleCCValue(thisStep.value);
	 									//send initial value
	 									cc.sendAtBeat(startBeat);
	 							}

	 				 			//if the current step is set to glide
	 							if (thisStep.shouldGlide) {
	 									var curve;

	 									//get glide curve either from step or track slider
	 									if (this.glideCurveByStep) {
	 											curve = this.scaleGlideCurve(thisStep.glideCurve);
	 									} else {
	 											curve = this.scaleGlideCurve(this.trackGlideCurve);
	 									}

	 									//get start and end values
	 									var startValue = this.scaleCCValue(thisStep.value);
	 									var endValue;

	 									//if endGlide slider is active, use the value from the end
	 									//glide slider, otherwise, use the value from the next
	 									//step in the sequence
	 									if (this.usesEndGlide) {
	 											endValue = this.scaleCCValue(thisStep.endGlide);
	 									} else {
	 											endValue = this.scaleCCValue(nextStep.value);
	 									}

	 									//create an array of CC values, which are scaled to
	 									//increment in a curve, set by glideCurve
	 									this.curveArray = this.getCurve(startValue,
	 																								endValue,
	 																								curve);

	 									//if the array is filled
	 									if (this.curveArray.length > 0) {
												//calculate end beat
												var endBeat = beatToSchedule + (1 / division);

												if (!isSwinging) {
														endBeat += swingOffset;
												}

												//calculate the timing intervals between CC sends
	 										 	var interval = (endBeat - startBeat)
	 										 															/ this.curveArray.length;
												var counter = 0;

	 											//schedule all CC events
	 											for (var i = 0 ; i < this.curveArray.length; i++) {
	 													//aftertouch
	 													if (this.ccNumber === 128) {
	 															var cp = new ChannelPressure();
	 															cp.value = this.curveArray[i];
	 															cp.sendAtBeat(startBeat + counter);
	 															counter += interval;
	 													}
	 													//pitchbend
	 													else if (this.ccNumber === 129) {
	 															var pb = new PitchBend();
	 															pb.value = this.scaleValue(this.curveArray[i], 
	 																												0, 
	 																												127, 
	 																												-8192, 
	 																												8191);
	 															pb.sendAtBeat(startBeat + counter);
	 															counter += interval;
	 													}
	 													else {
																var cc = new ControlChange();
																cc.number = this.ccNumber;
																cc.value = this.curveArray[i];
																cc.sendAtBeat(startBeat + counter);
																counter += interval;
														}
												}
	 									}
	 							}

	 							break;

	 					default:
								Trace("error in Track.pushTimingInfo()");
	 					}

	 					//store the previous step
	 					this.previousStep = thisStep;

	 			} else {
	 					//not a valid note, do nothing
	 			}

	 			//for all modes, except "Follow Global Position" calculate the next
	 			//step in the sequence
	 			this.resolveNextStep();

				// advance to next beat
			  beatToSchedule += 0.001;
			  beatToSchedule = Math.ceil(beatToSchedule * division) / division;
		}
}

//--------------------------------- advanceStep() ------------------------------
/*
		This function advances the currentStep either forwards of backwards by one,
		and performs checks to keep the step within the valid range that is defined
		by Start Step and Cycle Length

		startStep = the first step in the cycle
		endStep = the last step in the cycle
		direction = 0 (forwards) or 1 (backwards)
*/
Track.prototype.advanceStep = function (startStep, endStep, direction) {
	 	if (direction === 0) {
	 		 	//move forward in the sequence
	 			this.currentStep++;

	 			//if the step is beyond the max step, wrap around to 0
	 			if (this.currentStep > this.steps.length - 1) {
	 					this.currentStep = 0;
	 			}
	 	} else {
	 			//move backwards in the sequence
	 			this.currentStep--;

				//if the step is below the min range, wrap to max step
				if (this.currentStep < 0) {
						this.currentStep = this.steps.length - 1;
				}
	 	}

	 	//if the step is outside of the range of active steps, adjust the
	 	//step to fall within the range
	 	if (startStep < endStep) {
	 			//forwards
	 			if (direction === 0) {
	 					if (this.currentStep < startStep || this.currentStep > endStep) {
	 							this.currentStep = startStep;
	 					}
	 			}
	 			//backwards
	 			else {
	 					if (this.currentStep < startStep || this.currentStep > endStep) {
	 							this.currentStep = endStep;
	 					}
	 			}
	 	} else if (startStep > endStep) {
	 			//forwards
	 			if (direction === 0) {
	 				 	if (this.currentStep > endStep && this.currentStep < startStep) {
	 							this.currentStep = startStep;
	 					}
	 			}
	 			//backwards
	 			else {
	 				 	if (this.currentStep > endStep && this.currentStep < startStep) {
	 							this.currentStep = endStep;
	 					}
	 			}
	 	} else if (startStep === endStep) {
	 			//forwards
	 			if (direction === 0) {
	 					this.currentStep = startStep;
	 			}
	 			//backwards
	 			else {
	 				 	this.currentStep = endStep;
	 			}
		}
}

//------------------------------- resolveNextStep() ----------------------------
/*
		This function determines the next step that should be played in the sequence
		based on the track Direction control.
*/
Track.prototype.resolveNextStep = function () {
		//get the range of active steps
		var startStep = this.currentOffset - 1;
		var endStep = ((this.currentOffset - 1) + (this.currentLength - 1))
																													% this.steps.length;

		//calculate the next step, based on the current direction
	 	switch (this.direction) {
	 	//forwards .................................................................
	 	case 0:
				this.advanceStep(startStep, endStep, 0);
	 			break;
	 	//backwards ................................................................
	 	case 1:
				this.advanceStep(startStep, endStep, 1);
	 			break;
	 	//forwards <-> backwards ...................................................
	 	case 2:
	 			//fall through to case 3, as f <-> b and b <-> f use the same logic
	 	//backwards <-> forwards ...................................................
	 	case 3:
	 			//when reaching the start or end step of the range, switch directions
				//switch from forward to backward
				if (this.currentDirection === 0 && this.currentStep ===  endStep) {
						this.currentDirection = 1;
						this.currentStep = endStep + 1;
				}
				//switch from backwards to forwards
				else if (this.currentDirection === 1 && this.currentStep === startStep){
						this.currentDirection = 0;
				   	this.currentStep = startStep - 1;
				}

				//advance step depending on direction
				if (this.currentDirection === 0) {
						this.advanceStep(startStep, endStep, 0);
	 			} else {
						this.advanceStep(startStep, endStep, 1);
	 			}

	 			break;
	 	//random ...................................................................
	 	case 4:
	 			//randomly pick a step in the range of active steps
	 			var randomStep = Math.floor(Math.random() * this.currentLength);
				this.currentStep = (randomStep + this.currentOffset - 1)
																												% this.steps.length;
	 			break;
	 	//switch ...................................................................
	 	case 5:
				//see if the direction should change, depending on the switch
				//probability percentage
				if (Math.ceil(Math.random() * 100) <= this.switchProbability) {
						if (this.currentDirection === 0) {
								this.currentDirection = 1;
						} else {
								this.currentDirection = 0;
						}
				}

				if (this.currentDirection === 0) {
						this.advanceStep(startStep, endStep, 0);
				} else {
						this.advanceStep(startStep, endStep, 1);
				}

	 			break;
	 	//envelope forwards ........................................................
	 	case 6:
	 			this.currentStep++;

	 			if (this.currentStep >= this.steps.length) {
	 					this.currentStep = 0;
	 			}

	 			var end = ((this.currentOffset - 1) + (this.currentLength))
	 																												% this.steps.length

	 			if (this.currentStep === end ) {
	 					this.hasFinished = true;
	 			}

	 			break;
	 	//envelope backwards .......................................................
	 	case 7:
	 			this.currentStep--;

	 			var end = (this.currentOffset-2)%this.steps.length;

	 			if(this.currentStep === end) {
	 					this.hasFinished = true;
	 			}

	 			if(this.currentStep < 0) {
	 					this.currentStep = this.steps.length-1;
	 			}

	 			break;
	 	default:
	 			break;
	 	}
}

//-------------------------- updateTranspositionSlider() -----------------------
/*
		This function sets the value of the "Semitone Offset From C3" slider, if 
		the Transposition mode is set to "Follow Keyboard". 
		
		pitch is the incoming pitch, from which the offset is calculated 
*/
Track.prototype.updateTranspositionSlider = function (pitch) {
		if (this.playbackMode === 1) {
				var offsetValue = pitch - 60;
				SetParameter(this.transpositionSliderIndex, offsetValue);
		}
}

//----------------------------------- pushMIDI() -------------------------------
/*
		 This function receives incoming MIDI events and updates the state of the
		 track playback accordingly.

		 When NoteOn messages are received the sequence will be restarted (according
		 to the Retrigger type), and the beat at which the NoteOn event was received
		 will be stored. The current pitch is stored, so that the sequence can be
		 transposed.

		 When NoteOff messages are received remove the Note from the list of active
		 notes.

		 When sustain pedal message is received, track the state of the sustain
		 pedal.

		 Set the sequence to play if notes are active, or if sustain pedal is
		 pressed.

		 event = the incoming MIDI event
		 hostInfo = the timing info from the host
*/
Track.prototype.pushMIDI = function (event, hostInfo) {
		//Handle NoteOn events .....................................................
		if (event instanceof NoteOn) {
				//Retrigger === "On New Note")
				if (this.retriggerType === 0) {
						this.resetStep();
				}
				//Retrigger === "One New Phrase" and a new phrase is starting
				else if (this.retriggerType === 1
				&& this.activeNotes.length === 0
				&& !this.isSustaining) {
						this.resetStep();
				}
				//Retrigger === "Follow Global Position"
				else if (this.retriggerType === 2) {
						//forward or switch
						if (this.direction === 0 || this.direction === 5) {
								//do nothing
						}
						//always retrigger envelope modes
						else if (this.direction === 6 || this.direction === 7) {
								this.resetStep();
						}
				}

				//track the pitch for transposing the sequence
				if (this.playbackMode === 1) {
						this.currentPitchOffset = event.pitch;
				}
				
				//set playing to true
				this.isPlaying = true;

				//add to list of active notes
				this.activeNotes.push(event.pitch);			
				
				//set the transposition slider to the current offset
				this.updateTranspositionSlider(event.pitch);	
		}
		//Handle NoteOff events ....................................................
		else if (event instanceof NoteOff) {
				//remove the note from list of active notes
				for (var i = 0; i < this.activeNotes.length; i++) {
						if (event.pitch === this.activeNotes[i]) {
								this.activeNotes.splice(i,1);

								//set current pitch offset to the last note that was triggered
								if (this.playbackMode === 1 && this.activeNotes.length > 0) {
										this.currentPitchOffset =
																this.activeNotes[this.activeNotes.length - 1];
								}
								break;
						}
				}
		}
		//Handle Sustain Pedal .....................................................
		else if (event instanceof ControlChange && event.number === 64) {
				this.isSustaining = (event.value > 0) ? true : false;
		}

		//if sustain type is "always"
		if (this.sustainType === 1) {
				//set to playing
				this.isPlaying = true;
		}
		//if sustain type is "while notes are active" check if there are held notes
		//or if the sustain pedal is pressed. if neither are true, stop playing
		else if (this.activeNotes.length < 1 && !this.isSustaining) {
				this.isPlaying = false;
		}
}

//---------------------------------- updateLED() -------------------------------
/*
		This function updates the step LEDs for a track, to show the currently
		active step. It also sets all other steps to be inactive.

		currentStep = the current step (0 through steps.length-1)
		track = the trackID of the track to update the LEDs for
*/
Track.prototype.updateLED = function (currentStep, track) {
		var controlsOffset = this.numberOfControls - this.numberOfRandomControls 
															- this.numberOfEditingControls;

		for (var i = 0; i < track; i++) {
				if (i < this.sequencer.tracks.length - 1) {
						controlsOffset += this.sequencer.tracks[i].numberOfControls;
				}
		}

		var step = (controlsOffset - this.steps.length + currentStep);
		var startIndex = controlsOffset - this.steps.length ;

		for (var i = startIndex; i < controlsOffset; i++) {
				if(step === i) {
	    				SetParameter(i, 1);
				} else {
						SetParameter(i, 0);
		 		}
		}
}

//---------------------------------- resetStep() -------------------------------
/*
		This function resets the currentStep to either the first or last step in the
		sequence.

		If the mode supports changing direction, reset the direction

		If the mode is envelope mode, reset the hasFinished flag, for tracking if
		the sequence has played through
*/
Track.prototype.resetStep = function () {
		switch (this.direction){
		//forwards
		case 0:
				this.currentStep = this.currentOffset - 1;
				break;
		//backwards
		case 1:
				this.currentStep = (this.currentOffset - 1 + this.currentLength - 1)
																											 % (this.steps.length);
				break;
		//forwards <-> backwards
		case 2:
				this.currentStep = this.currentOffset - 1;
				this.currentDirection = 0;
				break;
		//backwards <-> forwards
		case 3:
				this.currentStep = (this.currentOffset - 1 + this.currentLength - 1)
																											 % (this.steps.length);
				this.currentDirection = 1;
				break;
		//random
		case 4:
				//do nothing, as there is no reset for random
				break;
		//switch
		case 5:
				this.currentStep = this.currentOffset - 1;
				break;
		//envelope : forwards
		case 6:
				this.currentStep = this.currentOffset - 1;
				this.hasFinished = false;
				break;
		//envelope : backwards
		case 7:
				this.currentStep = (this.currentOffset - 1 + this.currentLength - 1)
																											 % (this.steps.length);
				this.hasFinished = false;
				break;
		default:
				break;
		}
}

//^^^^^^^^^^^^^^^^^^^^^^^^^ Sequencer Object & Functions ^^^^^^^^^^^^^^^^^^^^^^^

//_________________________________ Sequencer() ________________________________
/*
		Constructor for the Sequencer object. The constructor does input error
		handling, which supplies default values, if the user does not enter valid
		values.

		If the arguments are valid the object will be initialized with the specified
		number of tracks, with the specified number of steps, and the specified
		mode. The user can modify tracks individually, using the various optional
		functions that are supplied.

		numTracks = the number of tracks to instantiate
		numSteps = the max number of steps in all tracks
		mode = the mode for all tracks : "note" or "cc"
*/
function Sequencer (numTracks, numSteps, mode) {
		var trackCount;             //private variable for initializing track count
		var stepCount; 							//private variable for initializing step count
		var initialMode;						//private variable for initializing mode type
		this.paramArray;            //stores a reference to the PluginParameters
																//array

		//handle invalid input .....................................................
		if (arguments.length !== 3) {
				Trace("Error creating Sequencer object. Please specify the number of "
				+ "tracks, the number of steps, and the sequencer mode. Defaulting to "
				+ "1 track, 16 steps, in \"note\" mode.");
				trackCount = 1;
				stepCount = 16;
				initialMode = "note";
		} else {
				if (numTracks < 1) {
						Trace("Error creating Sequencer object. The number of tracks must "
						+ "be at least 1. Defaulting to 1 track.");
						trackCount = 1;
				} else {
						trackCount = numTracks;
				}

				if (numSteps < 1) {
						Trace("Error creating Sequencer object. The number of steps must "
						+ "be at least 1. Defaulting to 16 steps.");
						stepCount = 16;
				} else {
						stepCount = numSteps;
				}

				var lowerCaseMode = mode.toLowerCase();
				if (lowerCaseMode !== "note" && lowerCaseMode !== "cc") {
						Trace("Error creating Sequencer object. The sequencer mode must be "
						+ "either \"note\" or \"cc\". Defaulting to \"note\" mode.");
						initialMode = "note";
				} else {
						initialMode = lowerCaseMode;
				}
		}

		this.tracks = [];               //array of track objects
		this.wasPlaying = false;        //flag to track if the sequencer changed
																	//from playing to note playing
		this.hostInfo;									//the timing info from the host (updated
																	//every ProcessMIDI() block
		this.active = false;	           //if the sequencer is running or not
		this.isSustaining = false;			//if the sustain pedal is currently pressed
		this.activeNotes = [];					//array of active notes that the user is
																	//holding down
		this.updateFunctions = [];      //array of all functions used in updating
																	//values, when changing UI controls
		//instantiate the specified number of tracks, with specified number of
		//steps, and mode type
		for (var i = 0; i < trackCount; i++) {
				this.tracks.push(new Track(stepCount, initialMode, i, this));
		}
}

//---------------------------- pushParameterChanged() --------------------------
/*
		This function gets called whenever a Scripter UI element is changed. The
		data from ParameterChanged() gets passed to this function, which calculates
		which function to call, to update the corresponding Step or Track parameter
		values.

		The functions are defined in the createUI function, and are stored in an
		array. The param index will access the function at that index.

		All functions are called with the (value, track, step) configuration so that
		any function can be called with a single, generic function call. If the
		function does not require step data, the value will simply be ignored.

		The track object is sent, and the step index is sent.

		param = the index of the scripter parameter that has changes
		value = the value of the parameter
*/
Sequencer.prototype.pushParameterChanged = function (param, value) {
		var previousControl = 0;      //number of controls the previous track has
		var trackIndex = 0;						//index of the track that is being updated
		var stepIndex = 0;						//index of the step that is being updated

		//get the index for the related track
		for (var i = 0; i < this.tracks.length; i++) {
				var numControls = this.tracks[i].numberOfControls;
				var total = (numControls + previousControl);
				
				//if the param index falls within this range of controls, this is the
				//index of the track
				if (param < total) {
						trackIndex = i;
						break;
				}

				//update the amount of controls that have already been examined
				previousControl += numControls;
		}

		//number of track level controls for this track
		var numGlobalControls = this.tracks[trackIndex].numberOfGlobalControls;
		//number of step level controls (total) for this track
		var numStepControls = this.tracks[trackIndex].numberOfStepControls;
		//the number of controls per step
		var numPerStep = numStepControls/this.tracks[trackIndex].steps.length;

		//if the parameter falls outside the track count, it is a step control
		if (param > numGlobalControls) {
				//get the step index
				stepIndex = (param - numGlobalControls) - previousControl;

				//calculate the step index, based on number of step controls
				if (stepIndex % numPerStep > 0 ) {
						stepIndex = (stepIndex - stepIndex % numPerStep) / numPerStep
				} else {
						stepIndex /= numPerStep;
				}
		}
		
		//if the param index falls within the range of the array of functions
		if (param < this.updateFunctions.length) {
				//every UI control has a function for updating it, stored in an array
				//of functions: updateFunctions. Send the calculated track object and
				//step index info to the function, with the new value
				this.updateFunctions[param](value,
																	this.tracks[trackIndex],
																	stepIndex,
																	param);
		}
}

//----------------------------------- createUI() -------------------------------
/*
		This function creates the UI for the sequencer, based on the user's
		configuration (which must be set, prior to calling this method). All
		functions for updating values are defined as variables in this method,
		and stored in an array that has a 1:1 relationship with the parameter index.

		paramArray = the PluginParameters array variable. This array must be passed
								into this function for the UI to be created.
*/
Sequencer.prototype.createUI = function (paramArray) {
		//keep a reference to the paramArray, so that it can be accessed elsewhere
		this.paramArray = paramArray;

		//note names
		var notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", 
									"B"];

		//generic track functions ..................................................
		//update state (bypassed/active)
		var trackStateUpdate = function (value, track, step) {
				track.isActive = (value === 0) ? false : true;
		}

		//update Rate
		var trackRateUpdate = function (value, track, step) {
				//if 1/24
				if (value === 4) {
						track.division = 6;
				}
				//if 1/32
				else if (value === 5) {
						track.division = 8;
				}
				//all other rates
				else {
						track.division = value + 1;
				}
		}

		//update Direction
		var trackDirectionUpdate = function (value, track, step) {
				//set direction
				track.direction = value;
				//reset hasFinished flag so that envelope modes are reset when switching
				//modes
				track.hasFinished = false;
		}

		//update Switch Probability
		var trackSwitchUpdate = function (value, track, step) {
				track.switchProbability = value;
		}

		//update Retrigger mode
		var trackRetriggerUpdate = function (value, track, step) {
				track.retriggerType = value;
		}

		//update Sustain mode
		var trackSustainUpdate = function (value, track, step) {
				track.sustainType = value;
				//if going from "always" to "while notes are active" and there are
				//no active notes and sustain is not being pressed, then
				//stop the playback
				if (value === 0
				&& track.activeNotes.length === 0
				&& track.isSustaining === false) {
						track.isPlaying = false;
				}
		}

		//update Swing
		var trackSwingUpdate = function (value, track, step) {
				track.swing = value;
		}

		//update Start Step
		var trackStartStepUpdate = function (value, track, step) {
				track.currentOffset = value;
		}

		//update Cycle Length
		var trackCycleLengthUpdate = function(value, track, step) {
				track.currentLength = value;
		}

		//note track functions .....................................................
		var trackPlaybackUpdate = function (value, track, step) {
				track.playbackMode = value;
				if (value === 0) {
						SetParameter(track.transpositionSliderIndex, 0);
				} else if (value === 1) {
						//do nothing
				} else {
						track.currentPitchOffset = 
																	GetParameter(track.transpositionSliderIndex);
				}
		}
		
		var trackSemiOffsetUpdate = function (value, track, step) {
				if (track.playbackMode === 2) {
						track.currentPitchOffset = value; 
				}
		}

		var trackVelocityUpdate = function (value, track, step) {
				track.trackVelocity = value;
		}

		var trackRetriggerBypassUpdate = function (value, track, step) {
				track.retriggerActive = value;
		}

		var trackGateLengthUpdate = function (value, track, step) {
				track.trackGateLength = value;
		}

		var trackPitchUpdate = function (value, track, step) {
				track.trackPitch = value;
		}

		var trackOctaveUpdate = function (value, track, step) {
				track.trackOctave = value;
		}

		//CC track functions .......................................................
		//update CC target
		var trackCCUpdate = function (value, track, step) {
				track.ccNumber = value;
		}

		//update Scale High
		var trackScaleHighUpdate = function (value, track, step) {
				track.scaleHighAmount = value;
		}

		//update Scale Low
		var trackScaleLowUpdate = function (value, track, step) {
				track.scaleLowAmount = value;
		}

		//update track Glide Curve
		var trackGlideCurveUpdate = function (value, track, step) {
				track.trackGlideCurve = value;
		}

		//note step functions ......................................................
		//These functions are for updating step variables, for note mode.
		//the new value, track index, and step index are passed to these functions
		//update pitch
		var pitchUpdate = function (value, track, step) {
				track.steps[step].pitch = value;
		}

		var octaveUpdate = function (value, track, step) {
				track.steps[step].octave = value;
		}

		//update articulation ID
		var articulationUpdate = function (value, track, step) {
				track.steps[step].articulationID = value;
		}

		//update step length per step
		var lengthUpdate = function (value, track, step) {
				track.steps[step].gateLength = value;
		}

		//update velocity per step
		var velocityUpdate = function (value, track, step) {
				track.steps[step].velocity = value;
		}

		//update probability
		var probabilityUpdate = function (value, track, step) {
				track.steps[step].probability = value;
		}

		//update retrigger
		var retriggerUpdate = function (value, track, step) {
				track.steps[step].retrigger = value;
		}
		
		var retriggerTransUpdate = function (value, track, step) {
				track.steps[step].retriggerTransposition = value;
		}

		//update retrigger end velocity
		var retriggerEndVelocityUpdate = function (value, track, step) {
				track.steps[step].retriggerEndVelocity = value;
		}

		var gateUpdate = function (value, track, step) {
				track.steps[step].isActive = value;
		}

		//CC  step functions .......................................................
		//these functions update step variables, for CC mode
		//update CC value
		var valueUpdate = function (value, track, step) {
				track.steps[step].value = value;
		}

		//update glide button
		var glideUpdate = function (value, track, step) {
				track.steps[step].shouldGlide = value;
		}

		//update glide curve per step
		var glideCurveUpdate = function (value, track, step) {
				track.steps[step].glideCurve = value;
		}

		//update end glide
		var endGlideUpdate = function (value, track, step) {
				track.steps[step].endGlide = value;
		}

		var emptyLED = function (value, track, step) {
				//do nothing
		}

		//random functions  ........................................................
		//triggers randomization of selected parameters
		var trackRandomize = function (value, track, step, index) {
				if (value === 1) {
						track.randomize();
						SetParameter(index, 0); //set random button to off
				}
		}

		//set randomization amount
		var trackRandomAmount = function (value, track, step) {
				track.randomAmount = value;
		}

		//enable randomization for global elements ------------------
		var trackRandomRate = function (value, track, step) {
				track.enableRandomRate = value;
		}

		var trackRandomDirection = function (value, track, step) {
				track.enableRandomDirection = value;
		}

		var trackRandomStartStep = function (value, track, step) {
				track.enableRandomStartStep = value;
		}

		var trackRandomCycleLength = function (value, track, step) {
				track.enableRandomCycleLength = value;
		}

		var trackRandomSwing = function (value, track, step) {
				track.enableRandomSwing = value;
		}

		//enable randomization for CC elements ----------------------
		var trackRandomScaling = function (value, track, step) {
				track.enableRandomScaling = value;
		}

		var stepRandomValues = function (value, track, step) {
				track.enableRandomValues = value;
		}

		var stepRandomGlides = function (value, track, step) {
				track.enableRandomGlide = value;
		}

		var stepRandomGlideCurves = function (value, track, step) {
				track.enableRandomGlideCurve = value;
		}

		var stepRandomGlideTo = function (value, track, step) {
				track.enableRandomGlideTo = value;
		}

		//enable randomization for note elements --------------------
		var stepRandomPitch = function (value, track, step) {
				track.enableRandomPitch = value;
		}

		var stepRandomOctave = function (value, track, step) {
				track.enableRandomOctave = value;
		}

		var stepRandomArticulation = function (value, track, step) {
				track.enableRandomArticulationID = value;
		}

		var stepRandomGateLength = function (value, track, step) {
				track.enableRandomGateLength = value;
		}

		var stepRandomProbability = function (value, track, step) {
				track.enableRandomProbability = value;
		}

		var stepRandomVelocity = function (value, track, step) {
				track.enableRandomVelocity = value;
		}

		var stepRandomRetrigger = function (value, track, step) {
				track.enableRandomRetrigger = value;
		}
		
		var stepRandomRetrigTrans = function (value, track, step) {
				track.enableRandomRetrigTrans = value;
		}

		var stepRandomRetriggerVelocity = function (value, track, step) {
				track.enableRandomRetriggerVelocity = value;
		}

		var stepRandomGate = function (value, track, step) {
				track.enableRandomGate = value;
		}
		
		//track editing tools functions ............................................
		// (set all steps to specified value)
		//note ---------------------
		var editPitch = function (value, track, step) {
				track.editPitch = value;
		}
		
		var setPitch = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.pitchIndexes.length; i++) {
								SetParameter(track.pitchIndexes[i], track.editPitch);
						}
						SetParameter(index, 0);
				}
		}

		var editOctave = function (value, track, step) {
				track.editOctave = value;
		}

		var setOctave = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.octaveIndexes.length; i++) {
								SetParameter(track.octaveIndexes[i], track.editOctave);
						}
						SetParameter(index, 0);
				}
		}
		
		var editArtID = function (value, track, step) {
				track.editArtID = value;
		}
		
		var setArtID = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.articulationIndexes.length; i++) {
								SetParameter(track.articulationIndexes[i], track.editArtID);
						} 
						SetParameter(index, 0);
				}
		}
		
		var editProbability = function (value, track, step) {
				track.editProbability = value;
		}
		
		var setProbability = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.probabilityIndexes.length; i++) {
								SetParameter(track.probabilityIndexes[i], 
														 track.editProbability);
						} 
						SetParameter(index, 0);
				}
		}

		var editVelocity = function (value, track, step) {
				track.editVelocity = value;
		}
		
		var setVelocity = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.velocityIndexes.length; i++) {
								SetParameter(track.velocityIndexes[i], track.editVelocity);
						} 
						SetParameter(index, 0);
				}
		}

		var editRetrigger = function (value, track, step) {
				track.editRetrigger = value;
		}

		var setRetrigger = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.retriggerIndexes.length; i++) {
								SetParameter(track.retriggerIndexes[i], track.editRetrigger);
						} 
						SetParameter(index, 0);
				}
		}

		var editRetrigTrans = function (value, track, step) {
				track.editRetrigTrans = value;
		}
		
		var setRetrigTrans = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.retrigTransIndexes.length; i++) {
								SetParameter(track.retrigTransIndexes[i], 
															track.editRetrigTrans);
						} 
						SetParameter(index, 0);
				}
		}

		var editRetrigEndVel = function (value, track, step) {
				track.editRetrigEndVel = value;
		}
		
		var setRetrigEndVel = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.retrigTransIndexes.length; i++) {
								SetParameter(track.retriggerVelocityIndexes[i], 
																											track.editRetrigEndVel);
						} 
						SetParameter(index, 0);
				}
		}

		var editGate = function (value, track, step) {
				track.editGate = value;
		}

		var setGate = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.gateIndexes.length; i++) {
								SetParameter(track.gateIndexes[i], track.editGate);
						} 
						SetParameter(index, 0);
				}
		}

		//cc ----------------------
		var editValue = function (value, track, step) {
				track.editValue = value;
		}
		
		var setValue = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.valueIndexes.length; i++) {
								SetParameter(track.valueIndexes[i], track.editValue);
						}
						SetParameter(index, 0);
				}
		}

		var editGlideTo = function  (value, track, step) {
				track.editGlideTo = value;
		}
		
		var setGlideTo = function (value, track, step, index) {
				if (value === 1) {
						for (var i = 0; i < track.glideToIndexes.length; i++) {
								SetParameter(track.glideToIndexes[i], track.editGlideTo);
						}	
						SetParameter(index, 0);
				}
		}
		
		var editGlideCurve = function (value, track, step) {
				track.editGlideCurve = value;
		}
		
		var setGlideCurve = function (value, track, step, index) {
				if(value === 1) {
						for (var i = 0; i < track.glideCurveIndexes.length; i++) {
								SetParameter(track.glideCurveIndexes[i], track.editGlideCurve);
						}
						SetParameter(index, 0);
				}
		}
		
		var editGlide = function (value, track, step) {
				track.editGlide = value;
		}
		
		var setGlide = function (value, track, step, index) {
				if(value === 1) {
						for (var i = 0; i < track.glideIndexes.length; i++) {
								SetParameter(track.glideIndexes[i], track.editGlide);
						}
						SetParameter(index, 0);
				}
		}
		
		var editGateLength = function (value, track, step) {
				track.editGateLength = value;
		}
		
		var setGateLength = function (value, track, step, index) {
				if(value === 1) {
						for (var i = 0; i < track.gateLengthIndexes.length; i++) {
								SetParameter(track.gateLengthIndexes[i], track.editGateLength);
						}
						SetParameter(index, 0);
				}
		}
		
		//Create UI ................................................................
		//add controls for each track, depending on the user defined optional
		//configurations. Add all related functions to the updateFunctions array,
		//while adding the UI control to the PluginParameters array
		for (var i = 0; i < this.tracks.length; i++) {
				var trackNumber = i + 1           //the user facing track number
				var mode = this.tracks[i].mode;   //the mode of this track

				var namePrefix = "";              //used to prefix both track and step
																				//numbers to the controls. if only
																				//one track is instantiated, no
																				//track numbers will be prefixed

				//if multiple tracks are being created, append the track # to all names
				if (this.tracks.length > 1) {
						namePrefix += "T"
						namePrefix += trackNumber + " : ";
				}

				//create track controls ________________________________________________
				//max # of steps for this track
				var maxLength = this.tracks[i].steps.length;

				//State ................................................................
				paramArray.push({
						name:namePrefix + "State",
						type:"menu",
						valueStrings:["Bypassed", "Active"],
						defaultValue:1,
						numberOfSteps:2
				});
				//function for updating track state
				this.updateFunctions.push(trackStateUpdate);

				//Rate .................................................................
				paramArray.push({
						name:namePrefix + "Rate",
						type:"menu",
						valueStrings:this.getDivisionLengths(),
						defaultValue:3,
						numberOfSteps:this.getDivisionLengths().length - 1
				});
				this.updateFunctions.push(trackRateUpdate);
				this.tracks[i].rateIndex = paramArray.length - 1;

				paramArray.push({
						name:namePrefix + "Direction",
						type:"menu",
						valueStrings:["Forwards", "Backwards", "Forwards <-> Backwards",
												"Backwards <-> Forwards", "Random", "Switch",
												"Envelope : Forwards", "Envelope : Backwards",],
						defaultValue:0,
						numberOfSteps:3
				});
				this.updateFunctions.push(trackDirectionUpdate);
				this.tracks[i].directionIndex = paramArray.length - 1;

				//Switch Probability ...................................................
				paramArray.push({
						name:namePrefix + "Switch Probability",
						type:"linear",
						minValue:0,
						maxValue:100,
						unit:"%",
						numberOfSteps:100,
						defaultValue:0
				});
				this.updateFunctions.push(trackSwitchUpdate);
				this.tracks[i].switchProbabilityIndex = paramArray.length - 1;

				//Retrigger ............................................................
				paramArray.push({
						name:namePrefix + "Retrigger",
						type:"menu",
						valueStrings:["On New Note",
												 "On New Phrase",
												 "Follow Global Position"],
						defaultValue:0,
						numberOfSteps:2
				});
				this.updateFunctions.push(trackRetriggerUpdate);

				//Sustain ..............................................................
				paramArray.push({
						name:namePrefix + "Sustain",
						type:"menu",
						valueStrings:["While Notes Are Active", "Always"],
						numberOfSteps:1,
						defaultValue:0
				});
				this.updateFunctions.push(trackSustainUpdate);

				//Swing ................................................................
				paramArray.push({
						name:namePrefix + "Swing",
						type:"linear",
						minValue:50,
						maxValue:99,
						numberOfSteps:49,
						defaultValue:50,
						unit:"%"
				});
				this.updateFunctions.push(trackSwingUpdate);
				this.tracks[i].swingIndex = paramArray.length - 1;

				//Start Step ...........................................................
				paramArray.push({
						name: namePrefix + "Start Step",
						type:"linear",
						minValue:1,
						maxValue: maxLength,
						defaultValue:1,
						numberOfSteps:maxLength - 1,
				});
				this.updateFunctions.push(trackStartStepUpdate);
				this.tracks[i].startStepIndex = paramArray.length - 1;

				//Cycle Length .........................................................
				paramArray.push({
						name: namePrefix + "Cycle Length",
						type:"linear",
						minValue:1,
						maxValue: maxLength,
						defaultValue:maxLength,
						numberOfSteps:maxLength - 1,
				});
				this.updateFunctions.push(trackCycleLengthUpdate);
				this.tracks[i].cycleLengthIndex = paramArray.length - 1;
				//keep count of how many global (track) controls there are
				this.tracks[i].numberOfGlobalControls += 9;

				//if track is CC _______________________________________________________
				if (mode === "cc") {
						var ccOptions = [];
						for (cc in MIDI._ccNames) {
								ccOptions.push(cc + " - " + MIDI._ccNames[cc]);
						}
						ccOptions.push("Aftertouch");
						ccOptions.push("Pitch Bend");

						//CC Select ........................................................
						paramArray.push({
								name:namePrefix + "CC",
								type:"menu",
								valueStrings:ccOptions,
								defaultValue:1	,
								numberOfSteps:ccOptions.length - 1
						});
						this.updateFunctions.push(trackCCUpdate);

						//Scale High .......................................................
						paramArray.push({
								name:namePrefix + "Scale High",
								type:"linear",
								minValue:0,
								maxValue:127,
								numberOfSteps:127,
								defaultValue:127
						});
						this.updateFunctions.push(trackScaleHighUpdate);
						this.tracks[i].scaleHighIndex = paramArray.length - 1;

						//Scale Low ........................................................
						paramArray.push({
								name:namePrefix + "Scale Low",
								type:"linear",
								minValue:0,
								maxValue:127,
								numberOfSteps:127,
								defaultValue:0
						});
						this.updateFunctions.push(trackScaleLowUpdate);
						this.tracks[i].scaleLowIndex = paramArray.length - 1;
						//keep track of how many global (track) controls there are
						this.tracks[i].numberOfGlobalControls += 3;

						//Track Glide ......................................................
						if (this.tracks[i].glideCurveByStep === false) {
								paramArray.push({
										name:namePrefix + "Track Glide Curve",
										type:"linear",
										minValue:-1,
										maxValue:1,
										numberOfSteps:400,
										defaultValue:0
								});
								this.updateFunctions.push(trackGlideCurveUpdate);
								this.tracks[i].numberOfGlobalControls++;
								this.tracks[i].glideCurveIndexes.push(paramArray.length - 1);
						}
				}
				//if track is note _____________________________________________________
				else {
						//Playback Mode ....................................................
						if (this.tracks[i].pitchByTrack) {
								paramArray.push({
										name:namePrefix + "Pitch",
										type:"menu",
										valueStrings:notes,
										defaultValue:0,
										numberOfSteps:notes.length - 1
								});
								this.updateFunctions.push(trackPitchUpdate);
								this.tracks[i].numberOfGlobalControls++;

								paramArray.push({
										name:namePrefix + "Octave",
										type:"linear",
										minValue:-2,
										maxValue:8,
										numberOfSteps:10,
										defaultValue:3
								});
								this.updateFunctions.push(trackOctaveUpdate);
								this.tracks[i].numberOfGlobalControls++;

						} else {
								paramArray.push({
										name:namePrefix + "Transposition",
										type:"menu",
										valueStrings:["Off", "Follow Keyboard", "Follow Slider"],
										numberOfSteps:1,
										defaultValue:1
								});
								this.updateFunctions.push(trackPlaybackUpdate);
								this.tracks[i].numberOfGlobalControls++;
								
								paramArray.push({
										name:namePrefix + "Semitone Offset From C3",
										type:"linear",
										minValue:-60,
										maxValue:67,
										numberOfSteps:127,
										defaultValue:0,
										disableAutomation:true,
										readOnly:true
								});
								this.updateFunctions.push(trackSemiOffsetUpdate);
								this.tracks[i].numberOfGlobalControls++;
								this.tracks[i].transpositionSliderIndex = paramArray.length - 1;
						}

						//Track Velocity ...................................................
						if (this.tracks[i].velocityByStep === false) {
								paramArray.push({
										name:namePrefix + "Track Velocity",
										type:"linear",
										minValue:1,
										maxValue:127,
										numberOfSteps:126,
										defaultValue:96
								});
								this.updateFunctions.push(trackVelocityUpdate);
								this.tracks[i].velocityIndexes.push(paramArray.length - 1);
								this.tracks[i].numberOfGlobalControls++;
						}

						//Step Retriggers On/Off ...........................................
						if (this.tracks[i].usesRetrigger) {
								paramArray.push({
										name:namePrefix + "Step Retriggers",
										type:"menu",
										valueStrings:["Bypassed","Enabled"],
										defaultValue:1,
										numberOfSteps:1
								});
								this.updateFunctions.push(trackRetriggerBypassUpdate);
								this.tracks[i].numberOfGlobalControls++;
						}

						//Track Step Length ................................................
						if(this.tracks[i].gateLengthByStep === false) {
								paramArray.push({
										name:namePrefix + "Gate Length",
										type:"linear",
										minValue:1,
										maxValue:100,
										numberOfSteps:99,
										defaultValue:100,
										unit:"%"
								});
								this.updateFunctions.push(trackGateLengthUpdate);
								this.tracks[i].numberOfGlobalControls++;
								this.tracks[i].gateLengthIndexes.push(paramArray.length - 1);
						}
				}

				//Create Step Controls__________________________________________________
				for (var j = 0; j < this.tracks[i].steps.length; j++) {
						var stepNumber = j + 1;
						var stepPrefix = "S" + stepNumber + " : "

						//NOTE -------------------------------------------------------------
						if (mode === "note") {
								//pitch ........................................................
								if (!this.tracks[i].pitchByTrack) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Pitch",
												type:"menu",
												valueStrings:notes,
												defaultValue:0,
												numberOfSteps:notes.length-1
										});
										this.updateFunctions.push(pitchUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].pitchIndexes.push(paramArray.length - 1);

										paramArray.push({
												name:namePrefix + stepPrefix + "Octave",
												type:"linear",
												minValue:-2,
												maxValue:8,
												numberOfSteps:10,
												defaultValue:3
										});
										this.updateFunctions.push(octaveUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].octaveIndexes.push(paramArray.length - 1);
								}

								//articulation ID ..............................................
								if (this.tracks[i].usesArticulationID) {
										var minArt = this.tracks[i].minArticulation;
										var maxArt = this.tracks[i].maxArticulation;
										paramArray.push({
												name:namePrefix + stepPrefix + "Articulation ID",
												type:"linear",
												minValue:minArt,
												maxValue:maxArt,
												numberOfSteps:maxArt - minArt,
												defaultValue:minArt,
										});
										this.updateFunctions.push(articulationUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].articulationIndexes.push(
																											paramArray.length - 1);
								}

								//step length by step ..........................................
								if (this.tracks[i].gateLengthByStep) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Gate Length",
												type:"linear",
												minValue:1,
												maxValue:100,
												numberOfSteps:99,
												defaultValue:100,
												unit:"%"
										});
										this.updateFunctions.push(lengthUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].gateLengthIndexes.push(paramArray.length-1);
								}

								//probability ..................................................
								if (this.tracks[i].usesProbability) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Probability",
												type:"linear",
												minValue:0,
												maxValue:100,
												numberOfSteps:100,
												unit:"%",
												defaultValue:100
										});
										this.updateFunctions.push(probabilityUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].probabilityIndexes.push(
																											paramArray.length - 1);
								}

								//velocity by step .............................................
								if(this.tracks[i].velocityByStep) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Velocity",
												type:"linear",
												minValue:1,
												maxValue:127,
												numberOfSteps:126,
												defaultValue:96
										});

										this.updateFunctions.push(velocityUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].velocityIndexes.push(paramArray.length - 1);
								}

								//retrigger and retrigger end velocity .........................
								if (this.tracks[i].usesRetrigger) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Retrigger",
												type:"linear",
												minValue:1,
												maxValue:4,
												numberOfSteps:3,
												defaultValue:1
										});
										this.tracks[i].retriggerIndexes.push(paramArray.length - 1);

										paramArray.push({
												name:namePrefix+stepPrefix + "Retrigger Transposition",
												type:"linear",
												minValue:-12,
												maxValue:12,
												numberOfSteps:24,
												defaultValue:0,
												unit:"semi",
										});
										this.tracks[i].retrigTransIndexes.push(paramArray.length-1);

										paramArray.push({
												name:namePrefix + stepPrefix + "Retrigger End Velocity",
												type:"linear",
												minValue:1,
												maxValue:127,
												numberOfSteps:126,
												defaultValue:96
										});
										this.tracks[i].retriggerVelocityIndexes.push(
																											paramArray.length - 1);

										this.updateFunctions.push(retriggerUpdate);
										this.updateFunctions.push(retriggerTransUpdate);
										this.updateFunctions.push(retriggerEndVelocityUpdate);
										this.tracks[i].numberOfStepControls += 3;
								}

							  paramArray.push({
							  			name:namePrefix + stepPrefix + "Gate",
							  			type:"checkbox",
							  			defaultValue:1
							  });
							  this.updateFunctions.push(gateUpdate);
							  this.tracks[i].numberOfStepControls++;
							  this.tracks[i].gateIndexes.push(paramArray.length - 1);

						} 
						//CC ---------------------------------------------------------------
						else {
								//CC value .....................................................
								paramArray.push({
										name:namePrefix + stepPrefix + "Value",
										type:"linear",
										minValue:0,
										maxValue:127,
										numberOfSteps:127,
										defaultValue:0
								});
								this.updateFunctions.push(valueUpdate);
								this.tracks[i].numberOfStepControls += 1;
								this.tracks[i].valueIndexes.push(paramArray.length - 1);

								//glide per step ...............................................
								if (this.tracks[i].usesEndGlide) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Glide To",
												type:"linear",
												minValue:0,
												maxValue:127,
												numberOfSteps:127,
												defaultValue:0
										});
										this.updateFunctions.push(endGlideUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].glideToIndexes.push(paramArray.length - 1);
								}

								//glide curve by step ..........................................
								if (this.tracks[i].glideCurveByStep) {
										paramArray.push({
												name:namePrefix + stepPrefix + "Glide Curve",
												type:"linear",
												minValue:-1,
												maxValue:1,
												numberOfSteps:400,
												defaultValue:0
										});
										this.updateFunctions.push(glideCurveUpdate);
										this.tracks[i].numberOfStepControls++;
										this.tracks[i].glideCurveIndexes.push(paramArray.length-1);
								}

								//glide ........................................................
								paramArray.push({
										name:namePrefix + stepPrefix + "Glide",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(glideUpdate);
								this.tracks[i].numberOfStepControls += 1;
								this.tracks[i].glideIndexes.push(paramArray.length - 1);
						}
				}
				//keep track of number of controls
				this.tracks[i].numberOfControls = this.tracks[i].numberOfGlobalControls
																				+ this.tracks[i].numberOfStepControls;

				//if optional mode to view step position by LED ------------------------
				if (this.tracks[i].usesLED) {
						//create an LED for each step
						for (var j = 0; j < this.tracks[i].steps.length; j++) {
								var stepName = "";
								stepName += "S" + (j + 1) + " LED";

								paramArray.push({
										name:namePrefix + stepName,
										type:"checkbox",
										defaultValue:0,
										disableAutomation:true,
								});

								this.tracks[i].numberOfControls++;
								//push an empty function, so that there is always a 1:1 relation
								//between UI params, and the function array
								this.updateFunctions.push(emptyLED);
						}
				}

				//create controls for randomizing --------------------------------------
				if (this.tracks[i].enableRandom) {
						//global options ...................................................
						paramArray.push({
								name:namePrefix + "Randomize Rate",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomRate);

						paramArray.push({
								name:namePrefix + "Randomize Direction",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomDirection);

						paramArray.push({
								name:namePrefix + "Randomize Start Step",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomStartStep);

						paramArray.push({
								name:namePrefix + "Randomize Cycle Length",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomCycleLength);

						paramArray.push({
								name:namePrefix + "Randomize Swing",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomSwing);

						this.tracks[i].numberOfControls += 7;
						this.tracks[i].numberOfRandomControls += 7;

						//mode specific parameters -----------------------------------------
						//CC ...............................................................
						if (mode === "cc") {
								paramArray.push({
										name:namePrefix + "Randomize Scaling Values",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(trackRandomScaling);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;

								paramArray.push({
										name:namePrefix + "Randomize CC Values",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomValues);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;

								if (this.tracks[i].usesEndGlide) {
										paramArray.push({
												name:namePrefix + "Randomize Glide To",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomGlideTo);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}
								
								paramArray.push({
										name:namePrefix + "Randomize Glide Curve",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomGlideCurves);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;								
								
								paramArray.push({
										name:namePrefix + "Randomize Glide Enabled",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomGlides);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;
						}
						//note .............................................................
						else if (mode === "note") {

								if (! this.tracks[i].pitchByTrack) {
										paramArray.push({
												name:namePrefix + "Randomize Pitch",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomPitch);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;

										paramArray.push({
												name:namePrefix + "Randomize Octave",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomOctave);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}

								if (this.tracks[i].usesArticulationID) {
										paramArray.push({
												name:namePrefix + "Randomize Articulation ID",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomArticulation);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}

								paramArray.push({
										name:namePrefix + "Randomize Gate Length",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomGateLength);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;

								if (this.tracks[i].usesProbability) {
										paramArray.push({
												name:namePrefix + "Randomize Probability",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomProbability);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}

								paramArray.push({
										name:namePrefix + "Randomize Velocity",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomVelocity);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;

								if (this.tracks[i].usesRetrigger) {
										paramArray.push({
												name:namePrefix + "Randomize Retrigger",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomRetrigger);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;

										paramArray.push({
												name:namePrefix + "Randomize Retrigger Transposition",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomRetrigTrans);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;

										paramArray.push({
												name:namePrefix + "Randomize Retrigger End Velocity",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(stepRandomRetriggerVelocity);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}

								paramArray.push({
										name:namePrefix + "Randomize Gate",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(stepRandomGate);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfRandomControls++;
						}
						
						//for setting amount of randomization ------------------------------
						paramArray.push({
								name:namePrefix + "Randomize Amount",
								type:"linear",
								minValue:1,
								maxValue:100,
								unit:"%",
								defaultValue:100,
								numberOfSteps:99
						});
						this.updateFunctions.push(trackRandomAmount);
						
						//for triggering the randomization
						paramArray.push({
								name:namePrefix + "RANDOMIZE!",
								type:"checkbox",
								defaultValue:0,
						});
						this.updateFunctions.push(trackRandomize);
				}
				//create controls for editing tools ____________________________________
				//These controls allow you to set all steps to the same value.
				if (this.tracks[i].editingTools) {
						//mode specific parameters 
						//CC ---------------------------------------------------------------
						if (mode === "cc") {
								//values .......................................................
								paramArray.push({
										name:namePrefix + "Value (all)",
										type:"linear",
										minValue:0,
										maxValue:127,
										numberOfSteps:127,
										defaultValue:0
								});
								this.updateFunctions.push(editValue);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;

								paramArray.push({
										name:namePrefix + "Set All : Value",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(setValue);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;

								//glide to .....................................................
								if (this.tracks[i].usesEndGlide) {
										paramArray.push({
												name:namePrefix + "Glide To (all)",
												type:"linear",
												minValue:0,
												maxValue:127,
												numberOfSteps:127,
												defaultValue:0
										});
										this.updateFunctions.push(editGlideTo);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
										
										paramArray.push({
												name:namePrefix + "Set All : Glide To",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setGlideTo);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfRandomControls++;
								}

								//glide curve ..................................................
								if (this.tracks[i].glideCurveByStep) {
										paramArray.push({
												name:namePrefix + "Glide Curve (all)",
												type:"linear",
												minValue:-1,
												maxValue:1,
												numberOfSteps:400,
												defaultValue:0
										});								
										this.updateFunctions.push(editGlideCurve);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								
										paramArray.push({
												name:namePrefix + "Set All : Glide Curve",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setGlideCurve);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}

								//glide on/off .................................................
								paramArray.push({
										name:namePrefix + "Glide (all)",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(editGlide);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;

								paramArray.push({
										name:namePrefix + "Set All : Glide",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(setGlide);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;
						}
						//note -------------------------------------------------------------
						else if (mode === "note") {
								//pitch by step ................................................
								if (!this.tracks[i].pitchByTrack) {
										paramArray.push({
												name:namePrefix + "Pitch (all)",
												type:"menu",
												valueStrings:notes,
												defaultValue:0,
												numberOfSteps:notes.length-1
										});
										this.updateFunctions.push(editPitch);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
										
										paramArray.push({
												name:namePrefix + "Set All : Pitch",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setPitch);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;

										paramArray.push({
												name:namePrefix + "Octave (all)",
												type:"linear",
												minValue:-2,
												maxValue:8,
												numberOfSteps:10,
												defaultValue:3,
										});
										this.updateFunctions.push(editOctave);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;

										paramArray.push({
												name:namePrefix + "Set All : Octave",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setOctave);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}

								//articulation id ..............................................
								if (this.tracks[i].usesArticulationID) {
										var minArt = this.tracks[i].minArticulation;
										var maxArt = this.tracks[i].maxArticulation;

										paramArray.push({
												name:namePrefix + "Articulation ID (all)",
												type:"linear",
												minValue:minArt,
												maxValue:maxArt,
												numberOfSteps:maxArt - minArt,
												defaultValue:minArt,
										});
										this.updateFunctions.push(editArtID);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;

										paramArray.push({
												name:namePrefix + "Set All : Articulation ID",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setArtID);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}

								//gate length by step ..........................................
								if (this.tracks[i].gateLengthByStep) {
										paramArray.push({
												name:namePrefix + "Gate Length (all)",
												type:"linear",
												minValue:1,
												maxValue:100,
												numberOfSteps:99,
												defaultValue:100,
												unit:"%"
										});
										this.updateFunctions.push(editGateLength);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
										
										paramArray.push({
												name:namePrefix + "Set All : Gate Length",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setGateLength);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}

								//probability ..................................................
								if (this.tracks[i].usesProbability) {
										paramArray.push({
												name:namePrefix + "Probability (all)",
												type:"linear",
												minValue:0,
												maxValue:100,
												numberOfSteps:100,
												unit:"%",
												defaultValue:100
										});
										this.updateFunctions.push(editProbability);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;

										paramArray.push({
												name:namePrefix + "Set All : Probability",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setProbability);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}						

								//velocity by step .............................................
								if (this.tracks[i].velocityByStep) {
										paramArray.push({
												name:namePrefix + "Velocity (all)",
												type:"linear",
												minValue:1,
												maxValue:127,
												numberOfSteps:126,
												defaultValue:96
										});			
										this.updateFunctions.push(editVelocity);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;					
								
										paramArray.push({
												name:namePrefix + "Set All : Velocity",
												type:"checkbox",
												defaultValue:0,
										});		
										this.updateFunctions.push(setVelocity);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;								
								}
								
								//retrigger ....................................................
								if (this.tracks[i].usesRetrigger) {
										paramArray.push({
												name:namePrefix + "Retrigger (all)",
												type:"linear",
												minValue:1,
												maxValue:4,
												numberOfSteps:3,
												defaultValue:1
										});										
										this.updateFunctions.push(editRetrigger);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;		

										paramArray.push({
												name:namePrefix + "Set All : Retrigger",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setRetrigger);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;		

										paramArray.push({
												name:namePrefix + "Retrigger Transposition (all)",
												type:"linear",
												minValue:-12,
												maxValue:12,
												numberOfSteps:24,
												defaultValue:0,
												unit:"semi",
										});
										this.updateFunctions.push(editRetrigTrans);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;		
										
										paramArray.push({
												name:namePrefix + "Set All : Retrigger Transposition",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setRetrigTrans);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;		

										paramArray.push({
												name:namePrefix + "Retrigger End Velocity (all)",
												type:"linear",
												minValue:1,
												maxValue:127,
												numberOfSteps:126,
												defaultValue:96
										});
										this.updateFunctions.push(editRetrigEndVel);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;		
										
										paramArray.push({
												name:namePrefix + "Set All : Retrigger End Velocity",
												type:"checkbox",
												defaultValue:0,
										});
										this.updateFunctions.push(setRetrigEndVel);
										this.tracks[i].numberOfControls++;
										this.tracks[i].numberOfEditingControls++;
								}
								
								//gate on/off ..................................................
								paramArray.push({
							  			name:namePrefix + "Gate (all)",
							  			type:"checkbox",
							  			numberOfSteps:1,
							  			defaultValue:1
							  });
							  
								this.updateFunctions.push(editGate);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;	
								
								paramArray.push({
										name:namePrefix + "Set All : Gate",
										type:"checkbox",
										defaultValue:0,
								});
								this.updateFunctions.push(setGate);
								this.tracks[i].numberOfControls++;
								this.tracks[i].numberOfEditingControls++;	
						}
				}
		}
}

//---------------------------------- addTrack() --------------------------------
/*
		This function adds a new track to the Sequencer object, with the specified
		max number of steps, and mode. This function includes input error handling.

		This function must be called prior to creating the UI controls for the
		sequencer.

		numSteps = the desired max number of steps. must be >= 1
		mode = the desired mode of the track: 'note' or 'cc'
*/
Sequencer.prototype.addTrack = function (numSteps, mode) {
		var lowerCaseMode = mode.toLowerCase();

		if (numSteps < 1) {
				Trace("Error in addTrack(): " + numSteps + " is not a valid "
				+ "number of steps. Please enter a number of 1 or greater");
		} else if (lowerCaseMode !== "note" && lowerCaseMode !== "cc") {
				Trace("Error in mode(): " + mode + " is not a valid mode. Please input "
				+ "'note' or 'cc'");
		} else {
				//number of steps, mode, ID for this track, reference to sequencer obj
				this.tracks.push(new Track(numSteps, mode, this.tracks.length, this));
		}
}

//------------------------------ maxNumberOfSteps() ----------------------------
/*
		Overloaded function for setting the max number of steps for a track(s). If a
		single track is not specified, all instantiated tracks will have their max
		number of steps updated. This function includes input error detection.

		This function must be called prior to creating the UI controls for the
		sequencer.

		numSteps = the desired max number of steps. must be >= 1
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, numSteps is applied to all tracks.
*/
Sequencer.prototype.maxNumberOfSteps = function (numSteps, track) {
		//error check numSteps
		if (numSteps < 1) {
				Trace("Error in maxNumberOfSteps(): " + numSteps + " is not a valid "
				+ "number of steps. Please enter a number of 1 or greater");
		}
		//if track parameter is supplied (only update the specified track)
		else if (track !== undefined) {
				var trackIndex = track - 1;

				//error check track
				if (trackIndex >= 0 && trackIndex <= this.tracks.length - 1) {
						//add more steps
						if (this.tracks[trackIndex].steps.length < numSteps) {
								while (this.tracks[trackIndex].steps.length < numSteps) {
										this.tracks[trackIndex].steps.push(new Step(0));
								}
						}
						//remove excess steps
						else if (this.tracks[trackIndex].steps.length > numSteps) {
								var numToRemove =
															this.tracks[trackIndex].steps.length - numSteps;
								this.tracks[trackIndex].steps.splice(numSteps, numToRemove);
						}

						//update currentLength to match new max number of steps
						this.tracks[trackIndex].currentLength =
																				this.tracks[trackIndex].steps.length;

				} else {
						Trace("Error in maxNumberOfSteps(): track " + track + " is not a "
						+ "valid track. Please input a number between 1 and "
						+ this.tracks.length);
				}
		}
		//update all tracks
		else {
				for (var i = 0; i < this.tracks.length; i++) {
						//add more steps
						if (this.tracks[i].steps.length < numSteps) {
								while (this.tracks[i].steps.length < numSteps) {
										this.tracks[i].steps.push(new Step(0));
								}
						}
						//remove excess steps
						else if (this.tracks[i].steps.length > numSteps) {
								var numToRemove = this.tracks[i].steps.length - numSteps;
								this.tracks[i].steps.splice(numSteps, numToRemove);
						}

						//update currentLength to match new max number of steps
						this.tracks[i].currentLength = this.tracks[i].steps.length;
				}
		}
}

//------------------------------------ mode() ----------------------------------
/*
		Overloaded function for setting the mode of a sequencer track(s).If a single
		track is not specified, all instantiated tracks will receive the specified
		mode ('note'/'cc'). This function includes input error detection.

		This function must be called prior to creating the UI controls for the
		sequencer.

		mode = the desired sequencer mode: 'note' or 'cc'
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, mode is applied to all tracks.
*/
Sequencer.prototype.mode = function (mode, track) {
		var lowerCaseMode = mode.toLowerCase();

		//error check mode
		if (lowerCaseMode !== "note" && lowerCaseMode !== "cc") {
				Trace("Error in mode(): " + mode + " is not a valid mode. Please input "
				+ "'note' or 'cc'");
		}
		//if track parameter is supplied (only update the specified track)
		else if (track !== undefined) {
				//error check track
				if (track - 1 >= 0 && track - 1 <= this.tracks.length - 1) {
						this.tracks[track - 1].mode = lowerCaseMode;
				} else {
						Trace("Error in mode(): track " + track + " is not a valid track. "
						+ "Please input a number between 1 and " + this.tracks.length);
				}
		}
		//apply to all tracks
		else {
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].mode = lowerCaseMode;
				}
		}
}

//---------------------------- displayTrackError() -----------------------------
/*
		This function displays an error message in the console. It specifies the
		name of the function in which the error occurred, and the track that is
		invalid. The valid track options will be displayed.

		functionName = the name of the function displaying an error
		track = the invalid track parameter
*/
Sequencer.prototype.displayTrackError = function (functionName, track) {
		if (this.tracks.length === 1) {
				Trace("Error in " + functionName + ": track " + track
				+ " is not a valid track. There is only 1 track instantiated.");
		} else {
				Trace("Error in " + functionName + ": track " + track
				+ " is not a valid track. Please input a number between 1 and "
				+ this.tracks.length);
		}
}

//---------------------------- displayStateError() -----------------------------
/*
		This function displays an error message in the console. It specifies the
		name of the function in which the error occurred, and the state that is
		invalid. The valid state options will be displayed.

		functionName = the name of the function displaying an error
		state = the invalid state parameter
*/
Sequencer.prototype.displayStateError = function (functionName, state) {
		Trace("Error in " + functionName + ": the argument: " + state
		+ " must be either true or false");
}

//--------------------------- displayUnknownError() ----------------------------
/*
		This function displays an error message in the console. It specifies the
		name of the function in which the unknown error occurred.

		functionName = the name of the function with the unknown error
*/
Sequencer.prototype.displayUnknownError = function (functionName) {
		Trace("An unknown error has occurred in " + functionName);
}

//-------------------------------- getAction() ---------------------------------
/*
		This function analyzes the state and track values that are passed into the
		optional sequencer state functions and returns an action to perform:
		all = update all tracks
		single = update only the specified track
		stateError = state argument is invalid: display error
		trackError = track argument is invalid: display error

		state = true or false
		track = the number of a specific track

		return = the action to perform
*/
Sequencer.prototype.getAction = function (state, track) {
		var action;

		if (state !== true && state !== false) {
				action = "stateError";
		} else if (track !== undefined) {
				if (track - 1 >= 0 && track - 1 <= this.tracks.length - 1) {
						action = "single";
				} else {
						action = "trackError";
				}
		} else {
				action = "all";
		}

		return action;
}

//--------------------------- setArticulationRange() ---------------------------
/*
		Overloaded function for setting the range of articulation IDs, when
		articulation IDs are enabled. Instead of displaying the full range of 0-255,
		define the range of active IDs (for example, 1-6). This function includes
		input error handling. The maxID must always be greater than the minID

		This function must be called prior to creating the UI controls for the
		sequencer.

		minID = the minimum artID value
		maxID = the maximum artID value
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.setArticulationRange = function (minID, maxID, track) {
		var inRange = function (value) {
				if (value >= 0 && value <= 255) {
						return true;
				} else {
						return false;
				}
		}
		var minRange = 0;
		var maxRange = 255;

		if (arguments.length < 2) {
				Trace("error in setArticulationRange: you must enter a min and max "
				      + "value. Defaulting to a range of 0 - 255.");
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].minArticulation = minRange;
						this.tracks[i].maxArticulation = maxRange;
				}
		} else if (arguments.length === 2 || arguments.length === 3) {
				if (inRange(minID)) {
						minRange = minID;
				} else {
						Trace("error in setArticulationRange: min value must be 0 - 255. "
						+ "Defaulting to 0");
				}

				if (inRange(maxID)){
						if (maxID > minID) {
								maxRange = maxID;
						} else {
								Trace("error in setArticulationRange: max value must be greater"
								+ " than the min value. Defaulting to 255.");
						}
				} else {
						Trace("error in setArticulationRange: max value must be 0 - 255. "
						+ "Defaulting to 255");
				}
		}

		if (arguments.length === 3) {
				if (track-1 >= 0 && track - 1 <= this.tracks.length - 1){
						this.tracks[track-1].minArticulation = minRange;
						this.tracks[track-1].maxArticulation = maxRange;
				} else {
						this.displayTrackError("setArticulationRange()", track);

						for (var i = 0; i < this.tracks.length; i++) {
								this.tracks[i].minArticulation = minRange;
								this.tracks[i].maxArticulation = maxRange;
						}
				}
		} else {
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].minArticulation = minRange;
						this.tracks[i].maxArticulation = maxRange;
				}
		}
}

//-------------------------------- pitchByTrack() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of defining
		the note value to sequence by track, instead of by step.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if pitch should be set by track instead of step: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.pitchByTrack = function (state, track) {
		var functionName = "pitchByTrack()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].pitchByTrack = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].pitchByTrack = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//-------------------------------- enableRandom() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of displaying
		controls for randomizing the various sequencer values.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if random controls should be displayed: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.enableRandom = function (state, track) {
		var functionName = "enableRandom()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].enableRandom = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].enableRandom = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//------------------------------ velocityByStep() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		note velocities by step, instead of by track. If a single track is not
		specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if note velocities should be controlled per step: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.velocityByStep = function (state, track) {
		var functionName = "velocityByStep()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].velocityByStep = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].velocityByStep = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//----------------------------- glideCurveByStep() -----------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		CC glide curves by step, instead of by track. If a single track is not
		specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if CC glide curves should be controlled per step: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.glideCurveByStep = function (state, track) {
		var functionName = "glideCurveByStep()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].glideCurveByStep = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].glideCurveByStep = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//------------------------------- usesRetrigger() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of allowing
		for note steps to be retriggered. If a single track is not specified, all
		instantiated tracks will receive the specified state (true/false).
		The getAction() function does input error detection, and determines what
		action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if note steps should have retrigger controls: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.usesRetrigger = function (state, track) {
		var functionName = "usesRetrigger()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].usesRetrigger = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].usesRetrigger = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//----------------------------- gateLengthByStep() -----------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		note step lengths per step, instead of by track. If a single track is not
		specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if note step lengths should be controlled per step: true or false
		track = optional value: the number of the individual track to modify. If no
						track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.gateLengthByStep = function (state, track) {
		var functionName = "gateLengthByStep()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].gateLengthByStep = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].gateLengthByStep = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//------------------------------ usesProbability() -----------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		the probability of a note step being triggered. If a single track is not
		specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if probability controls for note steps should be used: true or
		        false
		track = optional value: the number of the individual track to modify. If no
					  track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.usesProbability = function (state, track) {
		var functionName = "usesProbability()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].usesProbability = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].usesProbability = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//---------------------------- usesArticulationID() ---------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		the articulation ID of a note, per step. If a single track is not
		specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if articulation ID controls for note steps should be used: true or
		        false
		track = optional value: the number of the individual track to modify. If no
					  track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.usesArticulationID = function (state, track) {
		var functionName = "usesArticulationID()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].usesArticulationID = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].usesArticulationID = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//-------------------------------- usesEndGlide() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of controlling
		the end value of a CC glide, instead of gliding to the value of the next
		step. If a single track is not specified, all instantiated tracks will
		receive the specified state (true/false). The getAction() function does
		input error detection, and determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if an end value control should be used on CC steps: true or
		        false
		track = optional value: the number of the individual track to modify. If no
					  track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.usesEndGlide = function (state, track) {
		var functionName = "usesEndGlide()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].usesEndGlide = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].usesEndGlide = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//----------------------------------- usesLED() --------------------------------
/*
		Overloaded function for setting the optional sequencer state of displaying
		LEDs to visualize the current step of the track sequence. If a single track
		is not specified, all instantiated tracks will receive the specified state
		(true/false). The getAction() function does input error detection, and
		determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if LEDs should be displayed: true or false
		track = optional value: the number of the individual track to modify. If no
					  track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.usesLED = function (state, track) {
		var functionName = "usesLED()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].usesLED = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].usesLED = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//-------------------------------- editingTools() ------------------------------
/*
		Overloaded function for setting the optional sequencer state of displaying
		controls that allow you to set all steps to the same value, at once. This is
		useful to clear out values, if randomizing does not provide useful results.
		If a single track is not specified, all instantiated tracks will receive the
		specified state (true/false). The getAction() function does input error 
		detection, and determines what action should be performed.

		This function must be called prior to creating the UI controls for the
		sequencer.

		state = if editing tools should be displayed: true or false
		track = optional value: the number of the individual track to modify. If no
					  track value is supplied, state is applied to all tracks.
*/
Sequencer.prototype.editingTools = function (state, track) {
		var functionName = "editingTools()";
		var action = this.getAction(state, track);

		switch (action) {
		case "single":
				this.tracks[track - 1].editingTools = state;
				break;
		case "all":
				for (var i = 0; i < this.tracks.length; i++) {
						this.tracks[i].editingTools = state;
				}
				break;
		case "trackError":
				this.displayTrackError(functionName, track);
				break;
		case "stateError":
				this.displayStateError(functionName, state);
				break;
		default:
				this.displayUnknownError(functionName);
				break;
		}
}

//------------------------------------ reset() ---------------------------------
/*
		This function sends NoteOff events for every pitch in the sequence. This
		function is called in the Scripter function: reset()
*/
Sequencer.prototype.reset = function () {
		for (var i = 0; i < this.tracks.length; i++) {
				this.tracks[i].activeNotes = [];
				this.tracks[i].isPlaying = false;
				MIDI.allNotesOff();		
		}
}

//------------------------------ getDivisionLengths() --------------------------
/*
		This function returns an array of strings, which are the names of the
		divisions to be used in the "Rate" menu.
*/
Sequencer.prototype.getDivisionLengths = function () {
		return [ "1/4",
						"1/8",
						"1/12",
						"1/16",
						"1/24",
						"1/32"];
}

//----------------------------------- pushMIDI() -------------------------------
/*
		This function receives MIDI events from the Scripter function HandleMIDI()
		and passes the events to the various sequencer tracks.

		The function also decides what types of MIDI events will be passed through
		to the software instrument:
			- if any track is in 'note' mode, incoming note events will not be passed
				through
			- if any track is in 'note' mode, incoming sustain pedal events will not
				be passed through
			- if all tracks are in 'CC' mode, all event types will be passed through
*/
Sequencer.prototype.pushMIDI = function (event) {
		var usesNoteMode = false;

		for (var i = 0; i < this.tracks.length; i++) {
				//push events to tracks for individual handling
				this.tracks[i].pushMIDI(event, this.hostInfo);

				if (this.tracks[i].mode === "note") {
						usesNoteMode = true;
				}
		}

		//if any track uses Note mode
		if (usesNoteMode) {
				//only send non-note and non-sustain pedal events
				if ( ! (event instanceof Note
			  || (event instanceof ControlChange && event.number === 64))) {
						event.send();
				}
		}
		//if only CC modes are used, send all event types
		else {
				event.send();
		}
}

//-------------------------------- pushTimingInfo() ----------------------------
/*
		This function receives timing info from the Scripter function ProcessMIDI()
		and passes the timing info to each track, to calculate playback.

		To avoid recalculating the same data for each track, if tracks share the
		same division, certain data is calculated before sending the timing info
		to the tracks, such as the division in beats, the division number, the
		length of a division in ms, and the beat that the track was triggered on.
		If these values are shared across tracks, the values are stored in arrays,
		instead of recalculating the data for each track.
*/
Sequencer.prototype.pushTimingInfo = function (info) {
		//if the transport goes from playing to stopped
		if ((!info.playing) && this.wasPlaying) {

				this.wasPlaying = false;
				this.reset(); //send NoteOff events for all active notes

		} else if (info.playing) {
				this.hostInfo = info; //update this.hostInfo with current info
				this.wasPlaying = true;

				var beatToSchedule;

				//arrays to store values that have already been calculated for the
				//same variable, across tracks
				var divisionBeats = [];
				var divLengths = [];

				for (var i = 0; i < this.tracks.length; i++) {
						if (this.tracks[i].isPlaying) {
								var currentDiv = this.tracks[i].division;

								//if any value has not been calculated for a previous
								//track division, calculate the values
								if ( ! (divisionBeats[currentDiv]
								&& divLengths[currentDiv]
								&& trigBeat[currentDiv] )) {
										//calculate all values
										var beat =
											  Math.ceil(info.blockStartBeat * currentDiv)
											  		/ currentDiv;

										var quarterNote = 60000 / info.tempo;
										var divisionLength = quarterNote / currentDiv;

										//store values to be used by other tracks
										divLengths[currentDiv] = divisionLength;
										divisionBeats[currentDiv] = beat;
								}

								//send values to the track, to calculate playback
								this.tracks[i].pushTimingInfo(info,
																						divisionBeats[currentDiv],
																						currentDiv,
																						divLengths[currentDiv]);
						}//if
				}//for
		}//else if info.playing
}

//******************************************************************************
//                                                                             *
//                              Scripter Functions                             *
//                                                                             *
//******************************************************************************

//-------------------------------- HandleMIDI() --------------------------------
/*
		called for each incoming MIDI event

		event = the incoming MIDI event
*/
function HandleMIDI (event) {

		/* Handle all MIDI events in the sequencer.

		If any track is in 'note' mode, Note events and Sustain Pedal events will
		not be passed through, but all others events will. If all tracks are in 'cc'
		mode, the Note and Sustain Pedal events will be passed through.

		This function must be called for the sequencer to work! */
		SEQ.pushMIDI(event);
}

//_______________________________ ProcessMIDI() ________________________________
/*
		called every process block
*/
function ProcessMIDI() {

		//Push the current timing info for this process block to the sequencer.
		//This function must be called for the sequencer to work!
	 	SEQ.pushTimingInfo(GetTimingInfo());

}

//_____________________________ ParameterChanged() _____________________________
/*
		called when a UI control is changed

		param : index of the incoming parameter
		value : current value of the incoming parameter
*/
function ParameterChanged (param, value) {

		//Push the parameter info to the sequencer, which will update the
		//appropriate values

		//This function must be called for the sequencer to work!
		SEQ.pushParameterChanged(param, value);
}

//__________________________________ Reset() ___________________________________
/*
		called when transport state goes from stopped to playing, or when Scripter
		bypass state changes.
*/
function Reset() {

		//Reset the sequencer
		//This function must be called for the sequencer to work!
		SEQ.reset();
}

//******************************************************************************
//                                                                             *
//                               Global Variables                              *
//                                                                             *
//******************************************************************************

//Global Scripter variables
var NeedsTimingInfo = true;               //needed for GetTimingInfo()

var ResetParameterDefaults = true;				//flag that tells Scripter whether
																				//or not to reset UI controls to
																				//their default values when
																				//re-running the script

//create a Sequencer object
/*
		This constructor sets the number of tracks the sequencer holds, and
		initializes them all to have the same mode and and max number of steps.
		To change modes for individual tracks, use the mode() function, explained
		next.
*/
var SEQ = new Sequencer(1,         //number of tracks
											 16,       //max number of steps
											 "note"); 	//'note' or 'cc' mode

//adding more tracks -----------------------------------------------------------
//example code, is commented out. Uncommenting this code will result in 2 more
//tracks being added to the sequencer: A Note sequencer with 16 steps, and a 
//CC sequencer with 4 steps.

//SEQ.addTrack(16, "note");     //adds a track with the specified max number of
															//steps using the specified mode
//SEQ.addTrack(4, "cc");



//_________________________optional configurations______________________________
/*
		The following functions are overloaded.If you only provide a single argument
		ALL tracks in the sequencer will be updated. If you supply an additional
		argument, which is a track number, then only that track will be configured
		in the specified state. example:

				SEQ.velocityByStep(true); = ALL tracks will have velocity controls
																			 per step

		If an additional value, which is the track number, is supplied, only that
		track will be configured in the specified state. example:

				SEQ.velocityByStep(true, 2); = ONLY track 2 will have velocity
																					controls per step. All other tracks
																					will use a track level slider.

		Uses these functions to customize the behavior of each sequencer track.

		Note: if you call a note function on a cc track, and vice versa, there are
		      no ill effects. They will simply not affect the track. Additionally,
		      false values are the default state. You do not need to set any option
		      to false, unless you have previous set the option to true.

*/


//sequencer mode selection ----------------------------------------------------

//example code is commented out

//SEQ.mode("cc");   						//sets ALL tracks to CC mode
//SEQ.mode("note", 2);  				//sets only track 2 to note mode

//SEQ.maxNumberOfSteps(4);			//sets the max number of steps for all tracks
//SEQ.maxNumberOfSteps(4,2);		//sets the max number of steps for the specified
														  //track


//'note' mode configurations 	--------------------------------------------------
SEQ.velocityByStep(true);			  //if true, each step has a velocity slider
															//if false, there is a track-level slider
															//that controls the velocity of all track
															//steps.

SEQ.usesRetrigger(true);        //if true, each step has a retrigger slider
															//that retriggers the note n number of times
															//in that beat. A track level control is
															//also added to dynamically bypass all
															//retrigger values.
															//if false, no retrigger controls will be
															//available.

SEQ.gateLengthByStep(true);     //if true, each step has a gate length
															//slider. if false, there is a track-level
															//slider that controls the gate length
															//of all track steps

SEQ.usesProbability(true);      //if true, each step has a probability
															//slider that sets the chance of the step
															//being triggered. if false, no probability
															//controls will be available.

SEQ.usesArticulationID(false);  //if true, each step has an articulationID
															//slider that sets the articulationID of
															//the step. if false, no articulationID
															//controls will be available.

SEQ.pitchByTrack(false);        //if true, pitch will be set by track. if false
															//pitch will be set by step. this mode is good
															//for sequencing a single drum voice per track.


SEQ.editingTools(true);       //if true, controls will be added for setting all
														  //steps controls to the same value. This is
															//helps speed up the process of setting all
															//steps the the same value. If false, no controls
															//are displayed.

//example code is commented out. uncomment this code if you want to set a range
//or articulation IDs to use

//SEQ.setArticulationRange(1,3, 1); //sets the min and max range for a tracks
																	//articulation ID (if enabled).
																	//min, max, optional track#

//'cc' mode configurations -----------------------------------------------------
SEQ.glideCurveByStep(true);   //if true, each step will have a control
														//to set the curve of it's glide. if false,
														//there is a track-level slider that controls
														//the glide curve of all track steps.

SEQ.usesEndGlide(true);				//if true, each step will have a control that
													 	//is the ending value of the glide. if false,
														//the step will glide into the value of the
														//next step.



//LED mode ---------------------------------------------------------------------
SEQ.usesLED(true);          //if true, LEDs will be added to the UI that
													//update to indicate the currently active step
													//in the sequence. if false, no LEDs will be
													//displayed.

SEQ.enableRandom(true);			//if true. controls for randomizing track values
													//will be displayed. if false, not randomization
													//controls will be displayed.

//******************************************************************************
//                                                                             *
//                                 GUI Creation                                *
//                                                                             *
//******************************************************************************


var PluginParameters = [];  //the empty array of UI controls

//pass the empty PluginParameters array to the sequencer, which will dynamically
//create the UI for the sequencer, based on the optional configurations that
//were set above. Note: all optional configurations must be set before calling
//the createUI() function
SEQ.createUI(PluginParameters);


//-----------------------------------------------------------------------------
// Simple Arpeggiator
//-----------------------------------------------------------------------------
/*	
		Held notes are tracked in a global array in the HandleMIDI() callback.
		Notes are chosen and played back during the ProcessMIDI() callback.
*/

var NeedsTimingInfo = true;
var activeNotes = [];
var startTime =  new Date().getTime();
var stepCounter = 0;

function HandleMIDI(event) {
	if (event instanceof NoteOn) {
		// add note to array
		activeNotes.push(event);
	} 	
	else if (event instanceof NoteOff) {
		// remove note from array
		for (i=0; i < activeNotes.length; i++) {
			if (activeNotes[i].pitch == event.pitch) {
				activeNotes.splice(i, 1);
				break;
			}
		}
	}
	// pass non-note events through
	else event.send();
	
	// sort array of active notes
	activeNotes.sort(sortByPitchAscending);
}

//-----------------------------------------------------------------------------
function sortByPitchAscending(a,b) {
	if (a.pitch < b.pitch) return -1;
	if (a.pitch > b.pitch) return 1;
	return 0;
}

//-----------------------------------------------------------------------------
var wasPlaying = false;

function ProcessMIDI() {
	// Get timing information from the host application
	var musicInfo = GetTimingInfo();
	
	// clear activeNotes[] when the transport stops and send any remaining note off events
	if (wasPlaying && !musicInfo.playing){
		for(i=0;i<activeNotes.length;i++) {
			var off = new NoteOff(activeNotes[i]);
			off.send();
		}
	}
	
	wasPlaying = musicInfo.playing;
	
	if (activeNotes.length != 0) {	
		
		// get parameters
		var division = GetParameter("Beat Division");
		var noteOrder = GetParameter("Note Order");
		var noteLength = (GetParameter("Note Length") / 100) * (1 / division);
		var randomLength = Math.random() * ((GetParameter("Random Length") / 100) * (1 / division));
		var randomDelay = Math.random() * ((GetParameter("Random Delay") / 100) * (1 / division));
		var randomOctave = Math.floor(Math.random() * GetParameter("Random Octave")) * 12;
		
		//if playing, use the host timing info
		if (musicInfo.playing) {	
			
			// calculate beat to schedule
			var lookAheadEnd = musicInfo.blockEndBeat;
			var nextBeat = Math.ceil(musicInfo.blockStartBeat * division) / division;
			
			// when cycling, find the beats that wrap around the last buffer
			if (musicInfo.cycling && lookAheadEnd >= musicInfo.rightCycleBeat) {
				if (lookAheadEnd >= musicInfo.rightCycleBeat) {
					var cycleBeats = musicInfo.rightCycleBeat - musicInfo.leftCycleBeat;
					var cycleEnd = lookAheadEnd - cycleBeats;
				}
			}

			// loop through the beats that fall within this buffer
			while ((nextBeat >= musicInfo.blockStartBeat && nextBeat < lookAheadEnd)
			// including beats that wrap around the cycle point
			|| (musicInfo.cycling && nextBeat < cycleEnd)) {
				// adjust for cycle
				if (musicInfo.cycling && nextBeat >= musicInfo.rightCycleBeat)
					nextBeat -= cycleBeats;
					
				// calculate step
				var step = Math.floor(nextBeat / (1 / division) - division);
				var chosenNote = chooseNote(noteOrder, step);
				
				// send events
				var noteOn = new NoteOn(chosenNote);
				noteOn.pitch = MIDI.normalizeData(noteOn.pitch + randomOctave);
				noteOn.sendAtBeat(nextBeat + randomDelay);
				var noteOff = new NoteOff(noteOn);
				noteOff.sendAtBeat(nextBeat + randomDelay + noteLength + randomLength)

				// advance to next beat
				nextBeat += 0.001;
				nextBeat = Math.ceil(nextBeat * division) / division;
			}
		}
		//if not playing, calculate the timing info based on tempo
		else {    
			var tempo = musicInfo.tempo;    
			stepLength = 60000/tempo/division;
			randomLength = Math.random() * ((GetParameter("Random Length") / 100) * stepLength);
			randomDelay = Math.random() * ((GetParameter("Random Delay") / 100) * stepLength);

			//if the time of a step length has elapsed
			if(new Date().getTime() - startTime > stepLength) {
				
				//reset startTime
				startTime = new Date().getTime();
				
				//increment the step
				stepCounter += 1;
        
				var chosenNote = chooseNote(noteOrder, stepCounter);
				
				noteLength = stepLength * (GetParameter("Note Length")/100);
				
				// send events
				var noteOn = new NoteOn(chosenNote);
				noteOn.pitch = MIDI.normalizeData(noteOn.pitch + randomOctave);
				noteOn.sendAfterMilliseconds(randomDelay);
				var noteOff = new NoteOff(noteOn);
				noteOff.sendAfterMilliseconds(noteLength + randomLength + randomDelay);
			}
		}
	}
}

//-----------------------------------------------------------------------------
var noteOrders = ["up", "down", "random"];

function chooseNote(noteOrder, step) {
	var order = noteOrders[noteOrder];
	var length = activeNotes.length
	if (order == "up") return activeNotes[step % length];
	if (order == "down") return activeNotes[Math.abs(step % length - (length - 1))];
	if (order == "random") return activeNotes[Math.floor(Math.random() * length)];
	else return 0;
}

//-----------------------------------------------------------------------------
var PluginParameters = 
[
		{name:"Beat Division", type:"linear",
		minValue:1, maxValue:16, numberOfSteps:15, defaultValue:4},
	
		{name:"Note Order", type:"menu", valueStrings:noteOrders,
		minValue:0, maxValue:2, numberOfSteps: 3, defaultValue:0},
 
		{name:"Note Length", unit:"%", type:"linear",
		minValue:1, maxValue:200, defaultValue:100.0, numberOfSteps:199},

		{name:"Random Length", unit:"%", type:"linear",
		minValue:0, maxValue:200, numberOfSteps: 200, defaultValue:0},

		{name:"Random Delay", unit:"%", type:"linear",
		minValue:0, maxValue:200, numberOfSteps:200, defaultValue:0},

		{name:"Random Octave", type:"linear",
		minValue:1, maxValue:4, defaultValue:1, numberOfSteps:3}
];





/*
		Stutter v2.pst
		
		This is an enhanced version of the Stutter.pst which allows far more control
		in creating and performing stutters. Instead of a single pair of controls,
		you are given multiple groups of these controls, so that you can jump to
		different "preset" stutters. 
		
		"Select Group With" defines how stutter groups are selected:
			- "Group Select" Slider will use the group # that is currently selected
			   with the slider of the same name. This allows you to dynamically sweep
			   through the groups in real time, "performing" your stutters with a 
			   single parameter. A value of 0 will apply no stutter, and is in effect
			   a bypass. All incoming notes will be stuttered.
			   
			- Articulation ID selects the group # by the corresponding Articulation ID
			  (the Articulation ID can be set per note, via the Event editor. To view
			  Articulation ID data in the Event editor, go to View>Articulation ID)
			  When this mode is selected, there are 2 ways that stutters can function,
			  and this is set by the "Articulation ID Stutters" menu:
			  		- "Only The Notes With An ID" will only apply the stutter effect to
			  		  notes that have an articulation ID (that isn't 0). This means you
			  		  could stutter a single drum hit in a pattern, by only setting the
			  		  articulation ID for that specific hit. All other notes that are 
			  		  playing will not be effected.
			  		- "All Incoming Notes" will apply the stutter effect to all notes. 
			  		  The last used stutter group, will be applied to all incoming notes.
			  		  The default group is 1, so if no articulation ID is supplied,
			  		  the settings from group 1 will be used. 
			  		  
			- Random will randomly choose a group for every incoming MIDI note
			  		  
		- Velocity Mode controls how the stuttered notes velocities are set:
			- "As Played" will apply the same incoming velocity to all stutters
			- "Follow  Slider" will set all stutter velocities to the value that is
				defined with the "Stutter Velocity" slider.
			- "Ramp From Original To Slider" will increment the velocity, in equal 
			  steps, from the incoming velocity, to the value that is set with the
			  "Stutter Velocity" slider. Use this to add more dynamics to your 
			  stutters.
			- "Ramp From Slider To Original" works the same way, but in an inverted 
			   manner, offering a different type of sound. 
			   
		- RANDOMIZE! button will apply random values to all stutter groups, allowing
		  you to quickly experiment with different settings.			  	
		  
		- Add Group will dynamically add a new group of Stutter and Repeats controls 
		  to the UI, and update the Group Select range.
		 
		- Remove Group will dynamically remove the last group from the UI and update
	    the Group Select range. If you remove a group, it's settings are lost and
	    will not be restored when adding a new group.
			   
		And finally, this PST is fully scalable. Change the value of 
		NUMBER_OF_STUTTERS to any positive number, and rerun the script. You will 
		now have that number of groups to select from. 
		
*/

//user customizable variable ----------------------------------------------------
var NUMBER_OF_STUTTERS = 4;     //number of Stutter groups to create. Change this 
                                //value to add/remove groups
                                
ResetParameterDefaults = false; //flag that tells Scripter whether of not it 
                                //should reset the UI controls to their default
                                //values, when rerunning the script.
                                
var FILTER_ID = true;           //when set to true, the incoming Art ID will be 
                                //thrown away, so that it is not passed on to the
                                //next plugin or instrument. if false, the ID
                                //will remain in the NoteOn event. Try adding 
                                //another instance of Stutter v2 after this one,
                                //with Select Stutter With, set to Articulation 
                                //ID, for cumlitive, crazy stutters!
               

//global variables --------------------------------------------------------------
var MODE = 0;                   //0 = Slider
                                //1 = Articulation ID
                                //2 = Random 


var SLIDER_VALUE = 1;           //Stutter group to use when in slider mode

var ART_ID_MODE = 1;            //0 = Only The Notes With An ID
                                //1 = All Incoming Notes
														
var STUTTER_VELOCITY = 96;      //Value of the Stutter Velocity slider

var VELOCITY_MODE = 0;          //0 = As Played
                                //1 = Follow Slider
                                //2 = Ramp From Original To Slider
                                //3 = Ramp From Slider To Original

var PREVIOUS_GROUP_INDEX = 1;   //The last stutter group that was used

var NUM_GLOBAL_CONTROLS = 9;

var PARAMS_TO_ADD = [];
var PARAMS_TO_REMOVE = [];
var GROUP_NUMBER = NUMBER_OF_STUTTERS;

NeedsTimingInfo = true;         //Need to access GetTimingInfo()


//------------------------------ HandleMIDI() -----------------------------------
/*
		HandleMIDI() is called for every incoming MIDI event.
		
		If the event is a NoteOn event, perform the appropriate stutter, as is
		set by the various modes and settings.
*/
function HandleMIDI(event) {
	
		if (event instanceof NoteOn) {
		
				//array to hold stutter and repeats values
				var groupValues;
		
				var artID = event.articulationID;
				
				if(FILTER_ID) {
						event.articulationID = 0;
				}
				
				event.send();
		
				switch(MODE) {
				//slider ---------------------------------------------------------------
				case 0:
						if (SLIDER_VALUE > 0) {
								groupValues = getGroupValues(SLIDER_VALUE);
								stutter(event, groupValues[0], groupValues[1]);
						}
						break;
				//articulation ID ------------------------------------------------------
				case 1:
						//Only The Notes With An Assigned ID
						if (ART_ID_MODE === 0) {
								//only stutter notes that have Articulation IDs
								if (artID > 0 
								&& artID <= GROUP_NUMBER) {
										groupValues = getGroupValues(artID);
										stutter(event, groupValues[0], groupValues[1]);
								} 
								//show alert if incoming Art ID is greater than # of groups
								else if (artID > GROUP_NUMBER) {
										Trace("Error: Articulation ID <" + artID 
											+  "> is great than number of Stutter Groups");
								} else {
										//do nothing for events without a valid articulationID
								}
						} 
						//all incoming notes
						else {
								//if a note has an articulation ID update the PREVIOUS variables
								if (artID > 0
								&& artID <= GROUP_NUMBER) {
										groupValues = getGroupValues(artID);
										PREVIOUS_GROUP_INDEX = artID;
								} 
								//show alert if incoming Art ID is great than # of groups
								else if (artID > GROUP_NUMBER) {
										Trace("Error: Articulation ID < " + artID 
											+  " > is great than number of Stutter Groups");
								} 
								//if no ID is specified
								else {
										//use the last articulation ID
										groupValues = getGroupValues(PREVIOUS_GROUP_INDEX);
								}
						
								if (groupValues) {
										stutter(event, groupValues[0], groupValues[1]);
								}
						}
						
						break;
				//random ---------------------------------------------------------------
				case 2:
							var randomGroup = 
											Math.round(Math.random() * (GROUP_NUMBER - 1)) + 1;
							var groupValues = getGroupValues(randomGroup);
							stutter(event,groupValues[0], groupValues[1]);
							break;
				default:
						Trace("Error in HandleMIDI()");
				}
		} 
		//all other events
		else {
				event.send();
		}
}

//----------------------------- ParameterChanged() ------------------------------
/*
		ParameterChanged() is called whenever a UI element is changed.

		If the control is a global control, update the corresponding global variable.
		If RANDOMIZE! is pressed, cycle through all group values, and randomize them.
		All group values are updated in the getGroupValues() call, in HandleMIDI().
		If Add or Remove Group is pressed, schedule the addition or removal of 
		controls for a group, which will be handled in the Idle() function.		
*/
function ParameterChanged(param, value) {
		//if param is a global control, update the related global variables
		if (param <= NUM_GLOBAL_CONTROLS - 1) {
				switch (param) {
						case 1:
								MODE = value;
								break;
						case 2:
								SLIDER_VALUE = value;
								break;
						case 3:
								ART_ID_MODE = value;
								break;
						case 4:
								VELOCITY_MODE = value;
								break;
						case 5:
								STUTTER_VELOCITY = value;
								break;
						//RANDOMIZE! button
						case 6:
								//when enabled
								if (value === 1) {
										for(var i = 6; i < PluginParameters.length; i++) {
												var min = PluginParameters[i].minValue;
												var max = PluginParameters[i].maxValue;
												var randomValue = 
																Math.round(Math.random() * (max - min) + min);
												SetParameter(i, randomValue);
										}
										//set button to disabled
										SetParameter(6, 0);
								}
								break;
						//Add Group
						case 7:
								if (value === 1) {
								
										GROUP_NUMBER++;
								
										PARAMS_TO_ADD.push({
												name:"------ Group " + GROUP_NUMBER + " ------",
												type:"text"
										}, {
												name:"(" + GROUP_NUMBER + ") Stutter",
												type:"linear",
												minValue:1,
												maxValue:12,
												numberOfSteps:11,
												defaultValue:getDefaultStutterForGroup(GROUP_NUMBER)
										}, {
												name:"(" + GROUP_NUMBER + ") Repeats",
												type:"linear",
												minValue:1,
												maxValue:12,
												numberOfSteps:11,
												defaultValue:getDefaultRepeatsForGroup(GROUP_NUMBER),
										});
										
										SetParameter(7, 0);
								}
								break;
						//Remove Group
						case 8:
								if (value === 1) {
										PARAMS_TO_REMOVE.push(PluginParameters.length - 1);
										PARAMS_TO_REMOVE.push(PluginParameters.length - 2);
										PARAMS_TO_REMOVE.push(PluginParameters.length - 3);
										SetParameter(8, 0);
								}
								break;
						default:
								Trace("Error In ParameterChanged()");	
				}
		} 
}

//--------------------------------- Idle () -------------------------------------
/*
		Idle() is a built-in Scripter function that gets called several times per 
		second. Use this function to update UI elements, without affecting the 
		performance of the script.
		
*/
function Idle () {

		if (PARAMS_TO_ADD.length > 0) {
		
				for (var i = 0; i < PARAMS_TO_ADD.length; i++) {
						PluginParameters.push(PARAMS_TO_ADD[i]);		
				}
					
				PARAMS_TO_ADD = [];
				PluginParameters[2].maxValue = GROUP_NUMBER;
				PluginParameters[2].numberOfSteps = GROUP_NUMBER;
				UpdatePluginParameters();
		}
		
		if (PARAMS_TO_REMOVE.length > 0) {
				var removeCanceled = false;
		
				for (var i = 0; i < PARAMS_TO_REMOVE.length; i++) {
						
						var paramIndex = PARAMS_TO_REMOVE[i];
						
						if (GROUP_NUMBER === 1) {
								PARAMS_TO_REMOVE = [];
								removeCanceled = true;
								
								Trace("Remove Group failed: There must be at least one group.");
								break;
						}

						var removedItem = PluginParameters.splice(paramIndex, 1);
				}
				
				if (! removeCanceled) {
						GROUP_NUMBER--;
				}
				
				PARAMS_TO_REMOVE = [];
				PluginParameters[2].maxValue = GROUP_NUMBER;
				PluginParameters[2].numberOfSteps = GROUP_NUMBER;
				UpdatePluginParameters();
		}		
}


//------------------------------ getGroupValues() -------------------------------
/*
		This function retrieves the Stutter and Repeats values for a specified 
		group #. The values are returned in an array, with index 0 = stutter and 
		index 1 = repeats
		
		groupNumber is the stutter group you are retrieving the values for. This
		should be a value between 1 and NUMBER_OF_STUTTERS
		
		returns an array with the stutter and repeats values for the specifed group
*/
function getGroupValues (groupNumber) {
		var valueArray = [];
		
		//index of the last global control
		var baseOffset = NUM_GLOBAL_CONTROLS - 1;
		
		//the base group offset
		var groupOffset = (groupNumber - 1) * 3;
		
		//the index of the repeats control
		var index = (baseOffset + groupOffset) + 2;
		
		valueArray[0] = GetParameter(index);
		valueArray[1] = GetParameter(index + 1);

		return valueArray; 
}

//---------------------------------- stutter() ----------------------------------
/*
		This function takes an incoming NoteOn event and performs a sutter effect,
		based on the specified stutter and repeats settings. The velocity of the 
		stuttered notes depend on the global velocity mode, and stutter velocity
		values. 
		
		noteOn = the incoming NoteOn event that should be stuttered
		stutter = the stutter value 
		repeats = the repeats value
*/
function stutter (noteOn, stutter, repeats) {
		var info = GetTimingInfo();
		var secondsPerBeat = 1 / (info.tempo / 60);
		var rollBase = secondsPerBeat * 1000;
		var rollTime = rollBase / Math.pow(2, stutter) * repeats;
	
		var originalVelocity = noteOn.velocity;
	
		if (stutter > 0) {
				for (i=0; i < stutter; i++) {
						var rollIteration = rollTime + (rollTime * i);
						var rollTotal = rollIteration;
						
						switch(VELOCITY_MODE) {
								case 0:
										//use original velocity
										break;
								case 1:
										noteOn.velocity = STUTTER_VELOCITY;
										break;
								case 2:
										var range = originalVelocity - STUTTER_VELOCITY;
										var step = range/stutter;
										noteOn.velocity = originalVelocity - (step * (i+1));
										break;
								case 3:
										var range = originalVelocity - STUTTER_VELOCITY;
										var step = range/stutter;
										noteOn.velocity = STUTTER_VELOCITY + (step * (i+1));
										break;
								default:
										Trace("Error in stutter velocity");
						}
						
						noteOn.velocity = MIDI.normalizeData(noteOn.velocity);
						
						//if global variable is set to filter out incoming Articulation IDs
						if(FILTER_ID) {
								//remove the articulation ID, so that it is not passed along to
								//the next plugin or instrument
								noteOn.articulationID = 0;
						}
						
						noteOn.sendAfterMilliseconds(rollTotal);
						var noteOff = new NoteOff(noteOn);
						noteOff.sendAfterMilliseconds(rollTotal + 1);
				}		
		}
}

//---------------------------- create the UI controls ---------------------------

//create global controls --------------------------------------------------------
var PluginParameters = [{
		name:"------ Global Controls ------",
		type:"text"
}, {
		name:"Select Group With",
		type:"menu",
		valueStrings:[
				"\"Group Select\" Slider", 
				"Articulation ID", 
				"Random"
		],
		numberOfSteps:2,
		defaultValue:0
}, {		
		name:"Group Select",
		type:"linear",
		minValue:0,
		maxValue:NUMBER_OF_STUTTERS,
		numberOfSteps:NUMBER_OF_STUTTERS,
		defaultValue:1
}, {
		name:"Articulation ID Stutters",
		type:"menu",
		valueStrings:[
				"Only The Notes With An ID", 
				"All Incoming Notes"
		],
		numberOfSteps:1,
		defaultValue:0,
		
}, {
		name:"Velocity Mode",
		type:"menu",
		valueStrings:[
				"As Played", 
				"Follow Slider", 
				"Ramp From Original To Slider", 
				"Ramp From Slider To Original"],
		numberOfSteps:3,
		defaultValue:0
		
}, {
		name:"Stutter Velocity",
		type:"linear",
		minValue:1,
		maxValue:127,
		numberOfSteps:126,
		defaultValue:96
}, {
		name:"RANDOMIZE!",
		type:"checkbox",
		defaultValue:0
}, {
		name:"Add Group",
		type:"checkbox",
		defaultValue:0,
		disableAutomation:true
}, {
		name:"Remove Group",
		type:"checkbox",
		defaultValue:0,
		disableAutomation:true
}];

function getDefaultStutterForGroup (group) {
	return	(group * 2) % 12;
}

function getDefaultRepeatsForGroup (group) {
	return	group % 12;
}

//create stutter group controls -------------------------------------------------
for (var i = 0; i < NUMBER_OF_STUTTERS; i++) {

		PluginParameters.push({
				name:"----- Group " + (i + 1) + " ------",
				type:"text"
		}, {
				name:"(" + (i + 1) + ") Stutter", 
				type:"linear",
  				minValue:1, 
  				maxValue:12, 
  				numberOfSteps:11, 
  				defaultValue:getDefaultStutterForGroup(i+1)
		}, {
				name:"(" + (i + 1) + ") Repeats", 
				type:"linear",
  				minValue:1, 
  				maxValue:12, 
  				numberOfSteps:11, 
  				defaultValue:getDefaultRepeatsForGroup(i+1)
		});
}

// Stutter []

NeedsTimingInfo = true;

function HandleMIDI(e) {
	e.send();
	var info = GetTimingInfo();
	var roll = GetParameter("Stutter");
	var rollFactor = GetParameter("Repeats");
	var secondsPerBeat = 1 / (info.tempo / 60);
	var rollBase = secondsPerBeat * 1000;
	var rollTime = rollBase / Math.pow(2, roll) * rollFactor;
	
	if (e instanceof NoteOn)
		// drumroll please...
		if (roll > 0) {
			for (i=0; i < roll; i++) {
				var rollIteration = rollTime + (rollTime * i);
				var rollTotal = rollIteration;
				e.sendAfterMilliseconds(rollTotal);
				var noteOff = new NoteOff(e);
				noteOff.sendAfterMilliseconds(rollTotal + 1);
		}
	}
}

var PluginParameters = [
  {name:"Stutter", type:"linear",
  minValue:0, maxValue:12, numberOfSteps:12, defaultValue:3},
  
  {name:"Repeats", type:"linear",
  minValue:1, maxValue:12, numberOfSteps:11, defaultValue:1}];



  //1 - Simple Pass Through

/* 
    The HandleMIDI() function lets you process MIDI events that the plug-in 
    receives. 

    HandleMIDI is called each time a MIDI event is received by the plug-in and is 
    required to process incoming MIDI events. If you do not implement the 
    HandleMIDI function, events pass through the plug-in unaffected.
    HandleMIDI is called with one argument which is a JavaScript object that 
    represents the incoming MIDI event. HandleMIDI and JavaScript Event object 
    use is shown in the examples.
*/
function HandleMIDI(event) {
    event.send(); //Pass MIDI events through the plug-in.
}


//2 - Trace Events

/* 
    The HandleMIDI() function lets you process MIDI events that the plug-in 
    receives. 

    HandleMIDI is called each time a MIDI event is received by the plug-in and is 
    required to process incoming MIDI events. If you do not implement the 
    HandleMIDI function, events pass through the plug-in unaffected.
    HandleMIDI is called with one argument which is a JavaScript object that 
    represents the incoming MIDI event. HandleMIDI and JavaScript Event object 
    use is shown in the examples.
*/

function HandleMIDI(event) {

    event.trace();//Log events to the plug-in console and do not send them 
                  //anywhere.
}

//3 - Transpose and Delay

/* 
    The HandleMIDI() function lets you process MIDI events that the plug-in 
    receives. 

    HandleMIDI is called each time a MIDI event is received by the plug-in and is 
    required to process incoming MIDI events. If you do not implement the 
    HandleMIDI function, events pass through the plug-in unaffected.
    HandleMIDI is called with one argument which is a JavaScript object that 
    represents the incoming MIDI event. HandleMIDI and JavaScript Event object 
    use is shown in the examples.
*/

//Repeat notes up one octave with 100ms delay and pass all other events through.
function HandleMIDI(event) {
    event.send(); // send original event

    // if it's a note 
    if (event instanceof Note) { 
    
        event.pitch += 12; // transpose up one octave 
        event.sendAfterMilliseconds(100); // send after delay
    }
}

//4 - ProcessMIDI Callback

/*
    The ProcessMIDI() function lets you perform periodic (generally 
    timing-related) tasks. This can be used when scripting a sequencer, 
    arpeggiator, or other tempo-driven MIDI effect. ProcessMIDI is generally not 
    required for applications that do not make use of musical timing information 
    from the host. ProcessMIDI is called once per “process block,” which is 
    determined by the host’s audio settings (sample rate and buffer size).
    
    ProcessMIDI is called with no arguments.  

    This function will often be used in combination with the "JavaScript 
    TimingInfo object" to make use of timing information from the host 
    application.  The use of ProcessMIDI and the TimingInfo object is shown in 
    the example.
    
    Note: To enable the GetTimingInfo feature, you need to add 
          NeedsTimingInfo = true; at the global script level (outside of any 
          functions).
*/    
    
// Define NeedsTimingInfo as true at the global scope to enable GetHostInfo()
NeedsTimingInfo = true;
function ProcessMIDI() {

    var info = GetTimingInfo(); // get a TimingInfo object from the host

    //if the transport is running
    if (info.playing) { 
 
        Trace(info.tempo); // print the tempo in the plugin console
    }
}

//4 - ProcessMIDI Callback

/*
    The ProcessMIDI() function lets you perform periodic (generally 
    timing-related) tasks. This can be used when scripting a sequencer, 
    arpeggiator, or other tempo-driven MIDI effect. ProcessMIDI is generally not 
    required for applications that do not make use of musical timing information 
    from the host. ProcessMIDI is called once per “process block,” which is 
    determined by the host’s audio settings (sample rate and buffer size).
    
    ProcessMIDI is called with no arguments.  

    This function will often be used in combination with the "JavaScript 
    TimingInfo object" to make use of timing information from the host 
    application.  The use of ProcessMIDI and the TimingInfo object is shown in 
    the example.
    
    Note: To enable the GetTimingInfo feature, you need to add 
          NeedsTimingInfo = true; at the global script level (outside of any 
          functions).
*/    
    
// Define NeedsTimingInfo as true at the global scope to enable GetHostInfo()
NeedsTimingInfo = true;
function ProcessMIDI() {

    var info = GetTimingInfo(); // get a TimingInfo object from the host

    //if the transport is running
    if (info.playing) { 
 
        Trace(info.tempo); // print the tempo in the plugin console
    }
}


//6 - ParameterChanged Callback

/*
    The ParameterChanged() function lets you perform tasks triggered by changes 
    to plug-in parameters. ParameterChanged is called each time one of the 
    plug-in’s parameters is set to a new value. ParameterChanged is also called 
    once for each parameter when you load a plug-in setting.
    
    ParameterChanged is called with two arguments, first the parameter index (an
    integer number starting from 0), then the parameter value (a number).
*/

//Print parameter changes to the plug-in console. This example also creates a 
//slider in the plug-in window and assigns the ParameterChanged function to it.

//create a slider (default range 0.0 - 1.0)
var PluginParameters = [{name:"Slider", type:"lin", minValue:0 ,maxValue:1 ,numberOfSteps:100, defaultValue:0}]; 
function ParameterChanged(param, value) {

    // if it's the slider you just created
    if (param == 0) {
    
        Trace(value); // print the value to the console
    }
}

//7 - Event Creation

/*
    JavaScript Event Object
    
    When the "HandleMIDI function" is called, an Event object represents one MIDI
    event and implements the following methods you can call in your script:
    
    Event.send() //send the event
    
    Event.sendAfterMilliseconds(number ms) //send the event after the specified 
                                           //value has elapsed(can be an integer 
                                           //or floating point number)
                                           
    Event.sendAtBeat(number beat) //as above, but uses the beat value as a delay 
                                  //in beats from the current position
                                  
    Event.trace() //print the event to the plug-in console
    
    Event.toString() //returns a String representation of the event
    
    Event.channel(number) //sets MIDI channel 1 to 16. Note: Event.channel is an
                          //event property, rather than a method
                          
                          
    The Event object is not instantiated directly, but is a prototype for the 
    following event-specific object types. All of the following types inherit the
    methods described above and the channel property. The event types and their
    properties are passed to HandleMIDI as follows:
    
    NoteOn.pitch(integer number) //pitch from 1-127
    
    NoteOn.velocity(integer number) //velocity from 0-127. A velocity value of 0
                                    //is interpreted as a note off event, not a
                                    //note on.
                                    
    NoteOff.pitch(integer number) //pitch from 0-127
    
    NoteOff.velocity(integer number) //velocity from 0-127
    
    PolyPressure.pitch(integer number) //pitch from 1-127. Polyphonic aftertouch
                                       //is uncommon on synthesizers
                                       
    PolyPressure.value(integer number) //pressure value from 0-127
    
    ControlChange.number(integer number) //controller number from 0-127
    
    ControlChange.value(integer number) //controller value from 0-127
                                        //tip: use MIDI.controllerName(number) to
                                        //look up the name of the controller
                                        
    ProgramChange.number(integer number) //Program change number from 0-127
    
    ChannelPressure.value(integer number) //aftertouch value from 0-127
    
    PitchBend.value(integer number) //14-bit pitch bend value from -8192 to 8191
                                    //a value of 0 is center
*/

//Replace every MIDI event with a modulation control change message
//Tip: you can use the JavaScript "new" keyword to generate a new instance of an
//Event object of any type.

function HandleMIDI() {
  
    var cc = new ControlChange; //make a new control change message
    cc.number = 1; //set it to controller 1 (modulation)
    cc.value = 100; //set the value
    cc.send(); //send the event
    cc.trace(); //print the event to the console
}



//8 - Event Modification

/*
    JavaScript Event Object
    
    When the "HandleMIDI function" is called, an Event object represents one MIDI
    event and implements the following methods you can call in your script:
    
    Event.send() //send the event
    
    Event.sendAFterMilliseconds(number ms) //send the event after the specified 
                                           //value has elapsed(can be an integer 
                                           //or floating point number)
                                           
    Event.sendAtBeat(number beat) //as above, but uses the beat value as a delay 
                                  //in beats from the current position
                                  
    Event.trace() //print the event to the plug-in console
    
    Event.toString() //returns a String representation of the event
    
    Event.channel(number) //sets MIDI channel 1 to 16. Note: Event.channel is an
                          //event property, rather than a method
                          
                          
    The Event object is not instantiated directly, but is a prototype for the 
    following event-specific object types. All of the following types inherit the
    methods described above and the channel property. The event types and their
    properties are passed to HandleMIDI as follows:
    
    NoteOn.pitch(integer number) //pitch from 1-127
    
    NoteOn.velocity(integer number) //velocity from 0-127. A velocity value of 0
                                    //is interpreted as a note off event, not a
                                    //note on.
                                    
    NoteOff.pitch(integer number) //pitch from 0-127
    
    NoteOff.velocity(integer number) //velocity from 0-127
    
    PolyPressure.pitch(integer number) //pitch from 1-127. Polyphonic aftertouch
                                       //is uncommon on synthesizers
                                       
    PolyPressure.value(integer number) //pressure value from 0-127
    
    ControlChange.number(integer number) //controller number from 0-127
    
    ControlChange.value(integer number) //controller value from 0-127
                                        //tip: use MIDI.controllerName(number) to
                                        //look up the name of the controller
                                        
    ProgramChange.number(integer number) //Program change number from 0-127
    
    ChannelPressure.value(integer number) //aftertouch value from 0-127
    
    PitchBend.value(integer number) //14-bit pitch bend value from -8192 to 8191
                                    //a value of 0 is center
*/

//Replace every MIDI event received with a C3 notes on/off
//Tip: you can use the JavaScript "new" keyboard to generate a new instance of an
//Event object of any type.

NeedsTimingInfo = true; //needed for .sendAfterBeats() to work

function HandleMIDI(event) {
  
    var on = new NoteOn; //make a new note on
    on.pitch = 60; //set it's pitch to C3
    on.send(); //send the note
    
    var off = new NoteOff(on); //make a note off using the note on to initialize 
                               //it's pitch value (to C3)
    off.sendAfterBeats(1); //send a note off one beat later                       
}


//9 - NeedsTimingInfo and GetTimingInfo

/*
    JavaScript TimingInfo object
    
    The TimingInfo object contains timing information that describes the state of
    the host transport and the current musical tempo and meter. A TimingInfo
    object can be retrieved by calling GetTimingInfo()
    
    TimingInfo properties:
    
    TimingInfo.playing //uses boolean logic where "true" means the host
                       //transport is running.
    
    TimingInfo.blockStartBeat //a floating point number indicates the beat
                              //position at the start of the process block
                              
    TimingInfo.blockEndBeat //a floating point number indicates the beat position
                            //at the end of the process block
                            
    TimingInfo.blockSize //a floating point number indicates the length of the
                         //process block in beats
                         
    TimingInfo.tempo //a floating point number indicates the host tempo
    
    TimingInfo.meterNumerator //an integer indicates the host meter numerator
    
    TimingInfo.meterDenominator //an integer number indicates the host meter
                                //denominator
                                
    TimingInfo.cycling //uses boolean logic where "true" means the host transport
                       //is cycling
                       
    TimingInfo.leftCycleBeat //a floating point number indicates the beat position
                             //at the start of the cycle range
                             
    TimingInfo.rightCycleBeat //a floating point number indicates the beat
                              //position at the end of the cycle range
                              
    *note: The length of a beat is determined by the host application time
           signature and tempo.                    
*/

//print the beat position while the transport is running

var NeedsTimingInfo = true; //needed for GetTimingInfo() to work

function ProcessMIDI() {

    var info = GetTimingInfo(); //get the timing info from the host
	
    	//if the transport is playing
	  if (info.playing)
		    Trace(info.blockStartBeat); //print the beat position
}

//11 - Slider Creation

/*
    Create JavaScriptMIDI controls
    
    The JavaScriptMIDI Script Editor lets you use a simple shorthand to add
    standard controllers such as sliders and menus for automated or real time
    control of your plug-ins. The only mandatory property to define a new 
    parameter is a name, which will default to a basic slider. In addition, you 
    can add the following properties to change the type and behavior of controls.
    
    Optional properties:
    
    type: 
        //type one of the following strings as the value:
        "lin" //creates a linear fader
        "log" //creates a logarithmic fader
        "menu" //creates a menu
        "valueStrings" //the menu type requires an additional property that is
                         //an array of strings to show in the menu
    
    defaultValue: //type an integer or floating point number to set a default
                  //value. If not value is typed the default is 0.0
    
    minValue: //type an integer or floating point number to set a minimum value.
              //if no value is typed, the default is 0.0
              
    maxValue: //type an integer or floating point number to set a maximum value.
              //if no value is typed, the default is 1.0
*/

//Define MIDI plug-in controls

//This results in a slider named "Parameter x" with a default range of 0 to 1.
//It is set to the mid-point of 0.5
var PluginParameters = [{name:"Parameter x", type:"lin", defaultValue:0.5}];


//12 - Slider Ranges

/*
    Create JavaScriptMIDI controls
    
    The JavaScriptMIDI Script Editor lets you use a simple shorthand to add
    standard controllers such as sliders and menus for automated or real time
    control of your plug-ins. The only mandatory property to define a new 
    parameter is a name, which will default to a basic slider. In addition, you 
    can add the following properties to change the type and behavior of controls.
    
    Optional properties:
    
    type: 
        //type one of the following strings as the value:
        "lin" //creates a linear fader
        "log" //creates a logarithmic fader
        "menu" //creates a menu
        "valueStrings" //the menu type requires an additional property that is
                         //an array of strings to show in the menu
    
    defaultValue: //type an integer or floating point number to set a default
                  //value. If not value is typed the default is 0.0
    
    minValue: //type an integer or floating point number to set a minimum value.
              //if no value is typed, the default is 0.0
              
    maxValue: //type an integer or floating point number to set a maximum value.
              //if no value is typed, the default is 1.0
*/

//Define MIDI plug-in controls

//This results in a linear slider type, with five possible positions(steps), and
//a range from 0 to 5.
var PluginParameters = [{name:"Octaves", defaultValue:3, minValue:0, maxValue:5,
                        numberOfSteps:5,type:"lin"}];


//13 - Menu Creation

/*
    Create JavaScriptMIDI controls
    
    The JavaScriptMIDI Script Editor lets you use a simple shorthand to add
    standard controllers such as sliders and menus for automated or real time
    control of your plug-ins. The only mandatory property to define a new 
    parameter is a name, which will default to a basic slider. In addition, you 
    can add the following properties to change the type and behavior of controls.
    
    Optional properties:
    
    type: 
        //type one of the following strings as the value:
        "lin" //creates a linear fader
        "log" //creates a logarithmic fader
        "menu" //creates a menu
        "valueStrings" //the menu type requires an additional property that is
                         //an array of strings to show in the menu
    
    defaultValue: //type an integer or floating point number to set a default
                  //value. If not value is typed the default is 0.0
    
    minValue: //type an integer or floating point number to set a minimum value.
              //if no value is typed, the default is 0.0
              
    maxValue: //type an integer or floating point number to set a maximum value.
              //if no value is typed, the default is 1.0
*/

//Define MIDI plug-in controls

//This creates a menu named "Range" with the options: "Low", "Mid", and "High"
var PluginParameters = [{name:"Range", type:"menu", 
                        valueStrings:["Low", "Medium", "High"],
                        defaultValue:0, numberOfSteps:3}];



//14 - Convert Events with Parameter

/*
    Retrieve plug-in parameter values
    
    Call GetParameter() with the parameter name to return a value (number object)
    with the parameter's current value. GetParameter() is typically used inside
    the "HandleMIDI function" or "ProcessMIDI function"
*/

//This example converts modulation events into note events and provides a slider
//to determine note lengths

NeedsTimingInfo = true;

//create a slider (default range 0 - 100)
var PluginParameters = [{	name:"Note Length", type:"lin", minValue:0, maxValue: 100, 
												numberOfSteps:100, defaultValue:0, unit:"%"}]; 
												                                              
function HandleMIDI(event) {

    //if event is MIDI cc1 (modwheel)
    if(event instanceof ControlChange && event.number == 1) {
    
        var note = new NoteOn; //create a NoteOn object
        
        //since modwheel's range is 0-127, and pitch range is 1-127
        //convert a modwheel value of 0 to 1
        if(event.value == 0)
            event.value = 1;
            
        note.pitch = event.value; //use cc value as note pitch
        note.velocity = 100; //use velocity 100
        note.send(); //send note on
        
        var off = new NoteOff(note); //create a NoteOff object that inherits the
                                     //NoteOn's pitch and velocity
        
        //retrieve the parameter value of the slider you created (add 0.1 to
        //guarantee note on and off are not simultaneous
        var delayInBeats = GetParameter("Note Length")/100 + 0.1; 
                                                 
        off.sendAfterBeats(delayInBeats); //send note off after the length in
                                          //beats is set via the slider
    }
}
                        






